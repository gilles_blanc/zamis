import 'dart:async';

import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionInitPresenceActivite.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/model/EntActivite.dart';
import 'package:zamis/model/EntPresenceActivite.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/ActiviteViewModel.dart';
import 'package:zamis/widget/Base/BaseStatefullWidget.dart';

class ElemActivite extends StatefulWidget {

  late EntActivite _activite;


  ElemActivite(EntActivite activite){
    _activite=activite;


  }

  @override
  _DetailElemActivitePageState createState() => _DetailElemActivitePageState();
}

class _DetailElemActivitePageState extends BaseStatefullWidget<ActiviteViewModel,ElemActivite>  {

  late Timer _timer;
  String _timerMessage="";

  void _startTimer(ActiviteViewModel vm) {

    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
        //print("Delay heure = "+vm.param.ouverture_heure.toString());
        setState(() {
          _timerMessage=widget._activite.getInformationOpen(vm.param);
        });
    });
  }

  void _stopTimer() {
    if(_timer != null) _timer.cancel();
  }

  @override
  void onInit(ActiviteViewModel vm) {


    _startTimer(vm);
  }


  @override
  void onChange(ActiviteViewModel vm, BuildContext context) {


  }

  void _refreshCurrentUserPresence(ActiviteViewModel vm){
    print("ON CHANGE dans ElemActivite A");
    if(vm.currentUser != null) {
      print("ON CHANGE dans ElemActivite B");
      vm.action(ActionInitPresenceActivite(
          idActivite: widget._activite.id, idUSer: vm.currentUser!.id));
    }
    else{
      print("USER NULL dans ElemActivite !!!!");
    }
  }

  @override
  void onDispose(Store<AppState> store) {
    _stopTimer();
  }

  @override
  ActiviteViewModel modelBuilder(Store<AppState> store) {
    return (ActiviteViewModel.create(store));
  }

  _initPresence() {
    /*_subListPresenceActivite=PresenceActiviteService.getInstance().getObsListPrensenceActivites(widget.activite).listen((lp) {
      if(mounted) {
        setState(() {
          _listePresence = lp;
          _analysePresence();
        });
      }
      });
*/
  }

  /*Widget _getFixedRow(Widget w,int len,Color col){
    Widget ret=Container(
        width: MediaQuery.of(context).size.width-len,
        //color:col,
        /*child : Align(
          alignment: Alignment.centerLeft,
          child: te,
        )*/
      child : w

    );

    return(ret);
  }*/

  Widget _getButtonInfo(ActiviteViewModel vm){
    Widget w= IconButton(
        icon: Icon(Icons.info_outline),
        tooltip: 'Détail',
        onPressed: (widget._activite.isOpen(vm.param) || (vm.isAdmin())) ? (){
          _detailActivite(vm);
        } : null);


    return(w);

  }

  Widget _getBodyActivite(ActiviteViewModel vm) {

    double textSize = 15;
    EntPresenceActivite? pa = vm.getActivite(widget._activite.id);

    List<Widget> lw = [];

    if(pa != null){
      lw.add(Util.getIconBadge(
          pa.listIdPresent.length,
          Icon(Icons.accessibility),
          getPrimary(),
          getSecondary(),
          ((widget._activite.isOpen(vm.param) == true) || (vm.isAdmin() == true)) ? () {
            _togglePresence(vm);
          } : null,
          40,
          pa.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_YES
      ));

      lw.add(_getInfoBody(vm));
      lw.add(_getButtonInfo(vm));
    }
    else{
      _refreshCurrentUserPresence(vm);
    }






    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: lw,
    );


    return (r);
  }

  Widget _getInfoBody(ActiviteViewModel vm){

    int len1=220;
    int len2=250;

    String titre=widget._activite.getTitre();
    Text t=Text(titre ,
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold ,
              color:((widget._activite.isOpen(vm.param) == true) || (vm.isAdmin() == true)) ? getPrimary() : Colors.black12));

      List<Widget> lw = [];
      lw.add(t);
      Text st=Text(_timerMessage ,
            style: TextStyle(
                fontSize: 12,
                color: getPrimary()));
      lw.add(st);
      Column col = Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: lw
      );

    Expanded ex =Expanded(
      child: col,
    );

    /*Container cont=Container(
        color:Colors.limeAccent,
        child:col
    );*/
    return(ex);
  }

  void _togglePresence(ActiviteViewModel vm){
   /* if((_myPresence != null) && (widget.mode == Constantes.MODE_ELEM_ACTIVITE_SAISIE)) {
      if(_myPresence!.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_YES) _myPresence!.codePresence =Constantes.CODE_PRESENCE_ACTIVITE_NO;
      else _myPresence!.codePresence =Constantes.CODE_PRESENCE_ACTIVITE_YES;

      sendNotif();
      PresenceActiviteService.getInstance().majPresenceActivite(_myPresence!);

    }*/
    vm.toggleActivite(widget._activite.id);
  }

  void sendNotif() {
   /* int typeNotif=Constantes.TYPE_NOTIFICATION_RETRAIT_PARTICIPATION_MATCH;
    int nb = _nbPresent;
    bool flagRetrait =true;
    if(_myPresence!.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_YES) flagRetrait=false;

    if(flagRetrait == false) nb++;
    else nb--;
    if(nb < 0) nb=0;

    switch(widget.activite.typeActivite) {
      case Constantes.TYPE_ACTIVITE_NEXT_MATCH:
      case Constantes.TYPE_ACTIVITE_MATCH:
        if(flagRetrait == true) typeNotif=Constantes.TYPE_NOTIFICATION_RETRAIT_PARTICIPATION_MATCH;
        else typeNotif=Constantes.TYPE_NOTIFICATION_PARTICIPATION_MATCH;
        break;
      case Constantes.TYPE_ACTIVITE_NEXT_TRAINING:
        if(flagRetrait == true) typeNotif=Constantes.TYPE_NOTIFICATION_RETRAIT_PARTICIPATION_ENTRAINEMENT;
        else typeNotif=Constantes.TYPE_NOTIFICATION_PARTICIPATION_ENTRAINEMENT;
        break;
    }

    UserService.getInstance().getObsUsers().first.then((lu){
      EntNotification notif = EntNotification(typeNotif, widget.currentUser!,lu,nb);
      NotificationService.getInstance().sendNotificationFCM(notif);
    });

*/
  }

  void _detailActivite(ActiviteViewModel vm){
    vm.goDetail(widget._activite);
  }

  void _deleteActivite(){
    /*switch(widget.activite.typeActivite){
          case Constantes.TYPE_ACTIVITE_NEXT_MATCH:
          case Constantes.TYPE_ACTIVITE_MATCH:
            String nom= equipe != null ? equipe!.libelle : "Inconnue";
          Util.confirme(context,"Confirmez-vous la suppression du match contre " + nom +" ?").then((ret) {
            if (ret == true) {
              MatchService.getInstance().deleteMatch(
                  widget.activite as EntMatch);
            }
          });
          break;
    }*/
  }

  void _editActivite(){
   /* switch(widget.activite.typeActivite){
      case Constantes.TYPE_ACTIVITE_NEXT_MATCH:
      case Constantes.TYPE_ACTIVITE_MATCH:
      Navigator.of(context).pushNamed('/EditMatch',arguments: widget.activite as EntMatch);
      break;
    }*/

  }


  Widget _getLogo(){
    Widget w=Container();

    /*if(widget.activite.isMatch() == true){
      if (equipe != null) {
        Widget l=Util.getLogoEquipe(equipe!);
        w=Padding(
          padding: EdgeInsets.only(right:10),
          child : l
        );
      }
    }*/


    return(w);
  }



  Widget _getButtonAdmin(){
    Widget w=Container();

      Row r=Row(
        children: [
          IconButton(
              icon: Icon(Icons.delete),
              tooltip: 'Suppression',
              onPressed: _deleteActivite),
          IconButton(
              icon: Icon(Icons.edit),
              tooltip: 'Modification',
              onPressed: _editActivite)
        ],
      );

      w=Align(
          alignment: Alignment.centerRight,
          child : r
      );



    return(w);

  }

  edit() {

  }



  @override
  Widget getBody(BuildContext context, ActiviteViewModel vm) {
    Widget w;
    Container c = Container(
        margin: EdgeInsets.only(top: 5, bottom: 5, right: 10, left: 10),
        padding: EdgeInsets.only(left: 5, bottom: 5),
        width: MediaQuery.of(context).size.width - 100,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: Theme.of(context).colorScheme.primary,
            width: 2,
          ),
          borderRadius: BorderRadius.circular(12),
        ),
        child:Column(
          //crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            _getHeaderActivite(vm),
            _getBodyActivite(vm),
          ],
        )
    );

    GestureDetector gd = GestureDetector(
        onTap: () {
          _togglePresence(vm);
        },
        onLongPress: () {
          _detailActivite(vm);
        },
        child: c);

    if((widget._activite.isOpen(vm.param) == true) || (vm.isAdmin())) w=gd;
    else w=c;

    return (w);
  }

  Widget _getHeaderActivite(ActiviteViewModel vm) {

    Icon? ico = null;
    EntPresenceActivite? pa = vm.getActivite(widget._activite.id);
    if(pa != null){
      if(pa.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_UNKNOWN) {
        //ico = Icon(Icons.thumb_up,color:Colors.green,size:20);
      }
      if(pa.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_YES) {
        ico = Icon(Icons.thumb_up,color:Colors.green,size:20);
      }
      if(pa.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_NO) {
        ico = Icon(Icons.thumb_down,color:Colors.red,size:20);
      }
    }

    Row r =Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        _getDate(vm),
        ico != null ? ico : Text("")
      ],
    );

    Padding p=Padding(
        padding:EdgeInsets.only(top:5,bottom:5,left:10,right:10),
        child:r
    );

    return(p);

  }


  Widget _getDate(ActiviteViewModel vm) {
    double titreSize = 15;

   Text t=Text(widget._activite.getDateLong(), style: TextStyle(fontSize: titreSize));

    return (t);
  }





}
