import 'package:flutter/material.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/widget/Avatar/Avatar.dart';

class JoueurActivite extends StatelessWidget {
  bool me=false;
  late EntUser user;
  late String currentId;

  JoueurActivite(EntUser u,String currentId) {
    this.user=u;
    this.currentId=currentId;
    this.me=(user.id == this.currentId);
  }

  @override
  Widget build(BuildContext context) {
    if(user == null) return(Container());
    Container c = Container(
      margin: EdgeInsets.only(top: 5, bottom: 5, right: 10, left: 10),
      padding: EdgeInsets.all(3),
      //color: Colors.orangeAccent,
      decoration: BoxDecoration(
        border: Border.all(
          color: Theme.of(context).colorScheme.primary,
          width: 1,//widget.me ? 5 : 2,
        ),
        borderRadius: BorderRadius.circular(12),
        color: me ? Theme.of(context).colorScheme.secondary : Colors.white
      ),
      child: Row(
        children: <Widget>[
          Padding(
            padding:EdgeInsets.only(right:3),
            child:Avatar(user)
          ),
          Text(user.getNomFormat(),style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          Text(" " + user.getPrenomFormat(),style: TextStyle(fontSize: 20))
        ],
       ),
    );
    return (c);
  }

}
