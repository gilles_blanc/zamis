import 'dart:async';
import 'package:collection/src/iterable_extensions.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/model/EntActivite.dart';
import 'package:zamis/model/EntPresenceActivite.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/ActiviteViewModel.dart';
import 'package:zamis/widget/Base/BasePageStatefull.dart';

import 'JoueurActivite.dart';


class DetailActivite extends StatefulWidget {


  DetailActivite() {

  }

  @override
  _DetailActivitePageState createState() => _DetailActivitePageState();
}

class _DetailActivitePageState extends BasePageStatefull<ActiviteViewModel,DetailActivite>  {

  EntActivite? _currentActivite=null;
  //ParamActivite? paramActivite=null;
  //int mode=1;
  //int myMode=-1;
  //List<EntUser> listeUser=[];
  /*List<EntPresenceActivite> listePresent=[];
  List<EntPresenceActivite> listeAbsent=[];
  List<EntPresenceActivite> listeNoResponse=[];
*/
  //EntEquipe? equipe=null;
  //List<EntPresenceActivite> _listePresence=[];
  TabController? _tabController;

  @override
  ActiviteViewModel modelBuilder(Store<AppState> store) {
    return (ActiviteViewModel.create(store));
  }

  @override
  void onInit(ActiviteViewModel vm) {
    _tabController = TabController(length: 3, vsync: this);

    Future.delayed(
      const Duration(milliseconds: 50),
          () {
        _initDetail(vm);
      },
    );
  }

  void changeTab(int index){
    setState(() {
      _tabController!.index=index;
    });

  }

  /*_initAllUser() {
    UserService.getInstance().getObsUsers().first.then((l) {

      setState(() {
        listeUser = l;
      });
      _initPresence();
    });
  }*/

  void _initDetail(ActiviteViewModel vm){
    setState(() {
      _currentActivite = ModalRoute.of(context)!.settings.arguments as EntActivite;
    });
  }
/*


  _initPresence() {
    _subListPresenceActivite=PresenceActiviteService.getInstance().getObsListPrensenceActivites(paramActivite!.activite).listen((lp) {
      setState(() {
        _listePresence = lp;
        _analysePresence();
      });
    });
  }

  String _getLibelle(){
    String ret="";
    if(paramActivite!.activite.isMatch() == true){
      if (equipe != null) ret=equipe!.libelle;
    }
    if(paramActivite!.activite.isTraining() == true){
      ret="Entrainement";
    }


    return(ret);
  }

  void _analysePresence(){
    listePresent.clear();
    listeAbsent.clear();
    listeNoResponse.clear();

    _listePresence.forEach((element) {
      if(element.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_YES) listePresent.add(element);
      if(element.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_NO) listeAbsent.add(element);
      if(element.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_UNKNOWN) listeNoResponse.add(element);

      if(paramActivite!.user != null){
        if(element.idUser == paramActivite!.user.id){
          myMode=element.codePresence;
        }
      }
    });

    setState(() {


    });
  }*/

  /*Widget _getInfoBody(){
    Widget w=Container();
    if(paramActivite!.activite.isMatch() == true){
      /*Text te=Text(equipe!.libelle ,

          style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold));


      Text ts=Text("scsc" ,
          style: TextStyle(fontSize: 15));
      Text tsep=Text("-" ,
          style: TextStyle(fontSize: 15));
      List<Widget> lr = [];
      if((paramActivite!.activite as EntMatch).flagDomicile == true){
        Container c=Container(
            width: MediaQuery.of(context).size.width-200,
            color:Colors.red,
            child : Align(
              alignment: Alignment.centerLeft,
              child:
              Expanded(child: te)


            )


        );

        lr.add(ts);
        lr.add(tsep);
        lr.add(c);
      }
      else{
        Container c=Container(
            width: MediaQuery.of(context).size.width-200,
            //color:Colors.red,
            child : Align(
              alignment: Alignment.centerRight,
              child: te,
            )

        );
        lr.add(c);
        lr.add(tsep);
        lr.add(ts);
      }
      w=Row(
        children:lr,
      );*/
      w=Text("Contre " + equipe!.libelle ,
          style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold));
    }
    if(paramActivite!.activite.isTraining() == true){
      /*w=Text("Entrainement" ,
          style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold));*/
    }
    return(w);
  }*/

  /*
  Widget _getLogo(){
    Widget w=Container();

    if(paramActivite!.activite.isMatch() == true){
      if (equipe != null) w=Util.getLogoEquipe(equipe!);
    }


    return(w);
  }*/

  /*
  _back(){
    Navigator.of(context).pop();
  }*/

  @override
  Widget getAppBar(BuildContext context, ActiviteViewModel vm) {
    return AppBar(
        //toolbarHeight: 35,
        automaticallyImplyLeading: true,
        title: _getHeader()
    );
  }

  Widget _getHeader() {

    Column col=Column(
      children: <Widget>[
        _getFirstLine(),
        _getSecondLine(),
      ],
    );

    Container cont = Container(
        child: col
    );
    return(cont);

  }

  Widget _getFirstLine() {
    double titreSize = 15;

    if(_currentActivite == null) return(Container());
    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text("Présences : ", style: TextStyle(fontSize: titreSize)),
        Text(_currentActivite!.getTitre(), style: TextStyle(fontSize: titreSize)),
      ],
    );
    return (r);
  }

  Widget _getSecondLine() {
    double titreSize = 12;

    if(_currentActivite == null) return(Container());
    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(_currentActivite!.getDateLong(), style: TextStyle(fontSize: titreSize))
      ],
    );
    return (r);
  }

  @override
  Widget getBody(BuildContext context, ActiviteViewModel vm) {
    if(_currentActivite == null) return(Container());

    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _getBodyDetail(vm),
          ],
        ));
  }

  Widget _getBodyDetail(ActiviteViewModel vm) {
    EntPresenceActivite? pa = vm.getActivite(_currentActivite!.id);
    if(pa == null) return(Container());

    Column col = Column(
      children: <Widget>[
        _getTabBar(pa),
        _getTabView(pa,vm)
      ],
    );
    return (col);
  }

  Widget _getTabBar(EntPresenceActivite pa){

    String str="";
    Widget tb=TabBar(
        indicatorColor: Theme.of(context).colorScheme.primary,
        labelColor: Colors.grey,
        controller: _tabController,

        tabs: _getListTab(pa)
    );
    Padding p = Padding(
        padding:EdgeInsets.only(top:1),
        child:tb
    );
    return(p);
  }

  List<Tab> _getListTab(EntPresenceActivite pa){
    double sizeIcon=30;
    double heightTab=80;
    List<Tab> lRet = [];
    lRet.add(Tab(
        height: heightTab,
        text : _getTitle(pa.listIdPresent,"Présent"),
        icon: Util.getIconBadge(
            pa.listIdPresent.length,
            Icon(Icons.thumb_up),
            Colors.green,
            Theme.of(context).colorScheme.secondary,
                (){
              changeTab(0);
            },
            sizeIcon,
            pa.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_YES)
    )
    );

    lRet.add(Tab(
        height: heightTab,
        text:_getTitle(pa.listIdAbsent,"Absent"),
        icon: Util.getIconBadge(
            pa.listIdAbsent.length,
            Icon(Icons.thumb_down),
            Colors.red,
            Theme.of(context).colorScheme.secondary,
                (){
              changeTab(1);
            },
            sizeIcon,
            pa.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_NO)
    ));

    lRet.add(Tab(
        height: heightTab,
        text:"Indécis",
        icon: Util.getIconBadge(
            pa.listIdIndecis.length,
            Icon(Icons.group),
            Colors.grey,
            Theme.of(context).colorScheme.secondary,
                (){
              changeTab(2);
            },
            sizeIcon,
            pa.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_UNKNOWN)
    ));

    return(lRet);
  }

  Widget _getTabView(EntPresenceActivite pa,ActiviteViewModel vm){
    TabBarView tbv=TabBarView(
      controller: _tabController,
      children: <Widget>[
        getBlocUser(pa.listIdPresent,vm),
        getBlocUser(pa.listIdAbsent,vm),
        getBlocUser(pa.listIdIndecis,vm)
      ],
    );

    Container cont = Container(
        margin: const EdgeInsets.all(10.0),
        //padding: const EdgeInsets.all(5.0),
        height:MediaQuery.of(context).size.height-240,
        //color:Colors.yellow,
        child: tbv);

    Column col = Column(
      children: [
        _getSousTitre(pa),
        cont
      ],
    );
    return(cont);
  }

  Widget _getSousTitre(EntPresenceActivite pa) {
    String title="";

    switch (_tabController!.index) {
      case 0: // Présents
        title =_getTitle(pa.listIdPresent,"Présent");
        break;
      case 1: // Absents
        title =_getTitle(pa.listIdPresent,"Absent");
        break;
      case 2: // Sans Réponse
        title ="?";
        break;
    }
    Text t = Text(title,style:TextStyle(color: Theme.of(context).colorScheme.secondary,fontSize: 15 ,fontWeight:FontWeight.bold));
    Container cont = Container(
        margin: const EdgeInsets.all(10.0),
        //padding: const EdgeInsets.all(5.0),
        //height:MediaQuery.of(context).size.height-220,
        //color:Colors.yellow,
        child: Align(
            alignment: Alignment.centerLeft,
            child:t
        ));
    return(cont);
  }

  String _getTitle(List<String> l,String base){
    String nb="";
    String title = base;
    if(l.length > 1) title=title + "s";

    return(title);
  }

  Widget getBlocUser(List<String> lid,ActiviteViewModel vm){

    List<JoueurActivite> lja=[];

    EntUser? u;
    lid.forEach((id) {
      u=vm.listeUser.firstWhereOrNull((u) => u.id == id);
      if(u != null) lja.add(JoueurActivite(u!,vm.currentUser!.id));
    });

    lja.sort((j1,j2){
      int ret=j1.user.nom.toUpperCase().compareTo(j2.user.nom.toUpperCase());
      return(ret);
    });

    ListView lv = ListView(
      children: lja,
    );

    Container c = Container(
        //margin: EdgeInsets.all(10),
        height: MediaQuery.of(context).size.height-250,
        //padding: EdgeInsets.all(5),
        child: lv
    );


    return (c);


  }
}
