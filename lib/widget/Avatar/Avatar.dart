import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/AvatarViewModel.dart';
import 'package:zamis/widget/Base/BaseStatelessWidget.dart';

class Avatar extends StatelessWidget {
  late int modeAction;
  late EntUser currentUser;

  Avatar(EntUser user,[int mode = Constantes.ACTION_AVATAR_NONE]) {
    this.currentUser=user;
    modeAction = mode;
  }

  bool flagWait=false;

  actionAvatar(BuildContext context) async {
    switch (modeAction) {
      case Constantes.ACTION_AVATAR_NONE:
        break;
      case Constantes.ACTION_AVATAR_EDIT:
        Navigator.pushNamed(context, "/AvatarEdit",arguments: currentUser);
        break;
      case Constantes.ACTION_AVATAR_ADMIN:
        Navigator.pushNamed(context, "/JoueurEdit",arguments: currentUser);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {

    if(currentUser == null) {
      return (Container());
    }

    List<Widget> lw = [];
    Widget w;
    CircleAvatar ca;

    if (currentUser.urlAvatar != "") {
      CachedNetworkImageProvider p =
      CachedNetworkImageProvider(currentUser.urlAvatar);

      ca = new CircleAvatar(
          foregroundColor: Colors.white,
          backgroundColor: Theme
              .of(context)
              .colorScheme
              .secondary,
          backgroundImage: p);
    }
    else {
      ca = new CircleAvatar(
          foregroundColor: Colors.white,
          backgroundColor: Theme
              .of(context)
              .colorScheme
              .secondary,
          child: (Text(currentUser.getInitiales())));
    }

    if ((modeAction > 0) && (currentUser != null)) {
      w = new GestureDetector(
          onTap: () {
            actionAvatar(context);
          },
          child: ca);
    }
    else {
      w = ca;
    }

    lw.add(w);
    if(flagWait == true) {
      CircularProgressIndicator ci = CircularProgressIndicator(
          backgroundColor: Colors.white,
          valueColor: new AlwaysStoppedAnimation<Color>(
              Theme
                  .of(context)
                  .colorScheme
                  .primary));
      lw.add(ci);
    }
    Stack s = Stack(
      children: lw,
    );

    return (s);

  }

}
