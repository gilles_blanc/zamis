import 'dart:typed_data';
import 'dart:ui';
import 'package:crop/crop.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:redux/redux.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/AvatarViewModel.dart';
import 'package:http/http.dart';
import 'package:image/image.dart' as customImage;
import 'package:zamis/widget/Base/BasePageStatefull.dart';

class AvatarEdit extends StatefulWidget {

  AvatarEdit() {

  }

  @override
  _AvatarEditPageState createState() => _AvatarEditPageState();
}

class _AvatarEditPageState extends BasePageStatefull<AvatarViewModel,AvatarEdit> {

  XFile? image=null;
  Uint8List? buffer=null;
  final controller = CropController();
  EntUser? _user;

  void _initAvatar(AvatarViewModel vm){
    _user = (ModalRoute.of(context)!.settings.arguments as EntUser).clone();
      if (_user!.urlAvatar != "") {
        Uri uri =  Uri.parse(_user!.urlAvatar);
        get(uri).then((response) {
          setState(() {
            buffer=response.bodyBytes;
          });
        },onError: (error){
          print("error get :"+_user!.urlAvatar);
        });
      }
  }

  void pickImage() async {
    ImagePicker _picker = ImagePicker();
        _picker.pickImage(
          source: ImageSource.gallery,
          //maxWidth: Constantes.MAX_SIZE_PHOTO_AVATAR,
          //maxHeight: Constantes.MAX_SIZE_PHOTO_AVATAR,
          imageQuality: 100,
        ).then((img) {
          if(img != null){
            image=img;
            img.readAsBytes().then((tab) {
              setState(() {
                buffer=tab;
              });
            });
          }
        });

  }

  void pickPhoto() async {
    ImagePicker _picker = ImagePicker();
    _picker.pickImage(
      source: ImageSource.camera,
      //maxWidth: Constantes.MAX_SIZE_PHOTO_AVATAR,
      //maxHeight: Constantes.MAX_SIZE_PHOTO_AVATAR,
      imageQuality: 100,
    ).then((img) {
      if(img != null){
        image=img;
        img.readAsBytes().then((tab) {
          setState(() {
            buffer=tab;
          });
        });
      }
    });

  }

  List<Widget> _getCmdAppBar(AvatarViewModel vm){
    double heightIcon=30;

    List<Widget> lw = [];
    if(buffer != null){
      lw.add(IconButton(
          iconSize: heightIcon,
          icon: Icon(
            Icons.check,
            color: Colors.white,
          ),
          tooltip: 'valide',
          onPressed: () {
            majPhoto(vm);
          }));

    }
    else{
      lw.add(Container());
    }
    return(lw);
  }

  void majPhoto(AvatarViewModel vm) async {
    //var pixelRatio = MediaQuery.of(context).devicePixelRatio;

    var newPhoto = await controller.crop(pixelRatio: 1);

    dynamic bd = await newPhoto.toByteData(format: ImageByteFormat.png);


    customImage.Image? img=customImage.decodeImage(bd.buffer.asUint8List());
    customImage.Image tb= customImage.copyResize(img!,width: Constantes.MAX_SIZE_AVATAR);
    vm.maj(_user!,customImage.encodePng(tb) as Uint8List);
    vm.pop();
  }

  Widget _getCmdImage(){

    Widget w;
    if(buffer != null){
      Slider s=Slider(
        min: 1,
        max: 10.0,
        value: controller.scale,
        onChanged: (value) {
          setState(() {
            controller.scale = value;
          });
        },
      );

      w=Container(
        child:s
      );
    }
    else{
      w=Container();
    }
    return(w);
  }

  Widget _getCmdPick(){
    double heightIcon=30;

    Widget w;
      w = Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          kIsWeb == false ? ElevatedButton(
            onPressed: () {
              pickPhoto();
            },
            child: Icon(Icons.camera_alt),
          ) : Container(),
          ElevatedButton(
            onPressed: () {
              pickImage();
            },
            child: Icon(Icons.image),
          )
        ],
      );


    return(w);
  }

  Widget _getCropImage(AvatarViewModel vm) {
    BoxShape shape = BoxShape.circle;
    double radiusX = (4*getW() / 5)-10;
    double radiusY = (4*getH() / 5)-10;
    double radius=radiusX <radiusY ? radiusX : radiusY-100;

    Crop c = Crop(
      controller: controller,
      shape: shape,
      backgroundColor: getSecondary(),

      child: buffer != null ?  Image(
          image: MemoryImage(buffer!)) : _user != null ? Text(_user!.getInitiales(),style:TextStyle(color: Colors.white,fontSize: 10)) : Container(),
    );

    Container con = Container(width: radius, height: radius, child: c);
    return (con);
  }

  @override
  AvatarViewModel modelBuilder(Store<AppState> store) {
    return (AvatarViewModel.create(store));
  }

  @override
  void onInit(AvatarViewModel vm) {
    _initAvatar(vm);
  }


  @override
  Widget getAppBar(BuildContext context, AvatarViewModel vm) {
    return AppBar(
      actions: _getCmdAppBar(vm),
      title: Text("Avatar"),
    );
  }

  @override
  Widget getBody(BuildContext context, AvatarViewModel vm) {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _getCmdPick(),
            _getCropImage(vm),
            _getCmdImage(),
          ],
        ));
  }
}
