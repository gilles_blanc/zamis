import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionSavePersist.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/LoginViewModel.dart';
import 'Base/BasePageStatefull.dart';

class Login extends StatefulWidget {


  Login() {

  }

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends BasePageStatefull<LoginViewModel,Login> {

  final ctrlEmail = TextEditingController();
  final ctrlPassword = TextEditingController();

  String version = "";

  _savePersist(LoginViewModel vm){
    ActionSavePersist action = ActionSavePersist(
        flagPersistLogin: vm.flagPersistLogin,
        email : ctrlEmail.text,
        password : ctrlPassword.text);
    vm.action(action);
  }

  @override
  LoginViewModel modelBuilder(Store<AppState> store) {
    return(LoginViewModel.create(store));
  }

  @override
  void onInit(LoginViewModel vm) {
    ctrlPassword.text=vm.password;
    ctrlEmail.text=vm.email;
    if((vm.email == null) || (vm.email == "")) {
      if (kDebugMode) {
        ctrlPassword.text = "123456";
        ctrlEmail.text = "test@test.fr";
      }
    }
  }

  void _getVersion() {
    // TO DO
    /*ParamService.getInstance().getParam().then((p) {
      setState(() {
        version = p.getStrVersion();
      });
    });*/
  }

  @override
  Future<bool> confirmePop(BuildContext context,LoginViewModel vm) async {
    bool ret = await Util.confirme(context,
     "Voulez vous vraiment quitter l'application ?", "Déconnexion");

    if (ret == true) {
      _savePersist(vm);
      SystemNavigator.pop;
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    }

    return (ret);
  }

  @override
  Widget getBody(BuildContext context,LoginViewModel vm) {
    return ListView(
      //crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          _title(context,"ZAMIS FC"),
          logo(context),
          _input(context,
              Icon(
                Icons.mail,
                color: Theme
                    .of(context)
                    .colorScheme
                    .primary,
                size: 20.0,
              ),
              "Email",
              ctrlEmail,
              false,
              TextInputType.emailAddress,
              true),
          _input(context,
              Icon(
                Icons.lock,
                color: Theme
                    .of(context)
                    .colorScheme
                    .primary,
                size: 20.0,
              ),
              "Mot de passe",
              ctrlPassword,
              true,
              TextInputType.text,
              false),
          CheckboxListTile(
              checkColor: Colors.white,
              activeColor: Theme
                  .of(context)
                  .colorScheme
                  .primary,
              value: vm.flagPersistLogin,
              onChanged: (bool? newValue) {
                vm.savePersist(flagPersistLogin: newValue);
              },
              title: Text('Se souvenir de moi')),
          Util.filledButton(context, "S'identifier", (){
            vm.savePersist(flagPersistLogin: vm.flagPersistLogin,email: ctrlEmail.text,password: ctrlPassword.text);
            vm.login(ctrlEmail.text,ctrlPassword.text);
          }),
          GestureDetector(
              onTap: () {
                vm.resetPassword(ctrlEmail.text);
              },

              child: Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Mot de passe oublié",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color:
                            Theme.of(context).colorScheme.secondary),
                      ),
                      Text(version,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey))
                    ],
                  )
              )
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Text("version " + vm.version + " (" + vm.nrBuild + ")"),
          )



        ]);
  }

  Widget _title(BuildContext context,String hint) {
    return Container(
        //constraints: BoxConstraints(maxWidth: Constantes.MAX_WIDTH_RESPONSIVE),
        margin: const EdgeInsets.all(5.0),
        padding: EdgeInsets.only(left: 4, right: 4,top:10),
        child: Text(
          hint,
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 30.0,
              color: Theme.of(context).colorScheme.primary),
        ));
  }

  Widget _input(BuildContext context,Icon icon, String hint, TextEditingController controller,
      bool obsecure, TextInputType kbType, bool def) {
    return Container(
      //width:150,
      constraints: BoxConstraints(minWidth: 100, maxWidth: 150),
      //constraints: BoxConstraints(maxWidth: Constantes.MAX_WIDTH_RESPONSIVE),
      margin: const EdgeInsets.all(4.0),
      padding: EdgeInsets.only(left: 4, right: 4),
      child: TextField(
        keyboardType: kbType,
        //autofocus: def,
        controller: controller,
        obscureText: obsecure,
        style: TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
            hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 10),
            hintText: hint,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
              borderSide: BorderSide(
                color: Theme.of(context).colorScheme.primary,
                width: 2,
              ),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
              borderSide: BorderSide(
                color: Theme.of(context).colorScheme.primary,
                width: 3,
              ),
            ),
            prefixIcon: Padding(
              child: IconTheme(
                data:
                    IconThemeData(color: Theme.of(context).colorScheme.primary),
                child: icon,
              ),
              padding: EdgeInsets.only(left: 30, right: 10),
            )),
      ),
    );
  }



  Widget logo(BuildContext context) {
    return Center(
        child: Container(
      margin: const EdgeInsets.all(4.0),
      padding: EdgeInsets.only(left: 4, right: 4),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 150,
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Container(
                  height: 150,
                  //width: MediaQuery.of(context).size.width,
                  child: Align(
                      alignment: Alignment.center,
                      child: new ClipRRect(
                        borderRadius: new BorderRadius.circular(15.0),
                        child: Image(
                            image: AssetImage('assets/images/bk_terrain.jpg')),
                      ))),
            ),
          ],
        ),
      ),
    ));
  }
}
