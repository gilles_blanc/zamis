import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/NewVersionModel.dart';
import 'package:zamis/widget/Base/BasePageStateless.dart';

class NewVersion extends BasePageStateless<NewVersionModel> {


  @override
  NewVersionModel modelBuilder(Store<AppState> store) {

    return (NewVersionModel.create(store));
  }

  @override
  Widget? getAppBar(BuildContext context,NewVersionModel vm){
    String titre="Zamis FC";
    Widget w = AppBar(
      title: Text(titre),
      leading: vm.currentUser != null ? Container(
        padding: EdgeInsets.only(left: 5, top: 5),
      ) : null,

    );
    return (w);
  }

  @override
  Widget getBody(BuildContext context, NewVersionModel vm) {

    Widget w;
    List<Widget> lw = [];
    String suffixe = vm.param.force_maj_version  ? "" : " disponible";
    lw.add(_getText("Nouvelle version" + suffixe  ,20));
    lw.add(_getText("Attention, votre version actuelle",15));
    lw.add(_getText("v" + vm.version + " (" + vm.nrBuild + ")",15));
    lw.add(_getText("nécessite une mise à jour",15));
    if((vm.param.force_maj_version == false) || (vm.isAdmin() == true)){
      lw.add(_getText("Voulez-vous la télécharger ?",15));
      lw.add(
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Util.filledButton(context, "Oui", () {
                vm.downloadNewVersion();
              }),
              Util.filledButton(context, "Non", () {
                vm.cancelNewVersion();
              },getSecondary())
            ],
          ));
    }
    else{
      lw.add(
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Util.filledButton(context, "Mettre à jour", () {
                vm.downloadNewVersion();
              }),
            ],
          ));
    }
    w = ListView(
      children: lw);

    Container cont = Container(
        padding:EdgeInsets.all(10),
        child:w
    );
    return (cont);
  }

  Widget _getText(String txt,double size){
    Text t=Text(txt,style: TextStyle(fontSize: size));
    Padding p=Padding(
      padding:EdgeInsets.all(10),
      child:t
    );
    Center c = Center(
      child: p
    );

    return(c);
  }
}
