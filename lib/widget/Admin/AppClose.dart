import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/AppCloseModel.dart';
import 'package:zamis/viewModel/NewVersionModel.dart';
import 'package:zamis/widget/Base/BasePageStateless.dart';

class AppClose extends BasePageStateless<AppCloseModel> {


  @override
  AppCloseModel modelBuilder(Store<AppState> store) {

    return (AppCloseModel.create(store));
  }

  @override
  Widget? getAppBar(BuildContext context,AppCloseModel vm){
    String titre="Zamis FC";
    Widget w = AppBar(
      title: Text(titre),
      leading: vm.currentUser != null ? Container(
        padding: EdgeInsets.only(left: 5, top: 5),
      ) : null,

    );
    return (w);
  }

  @override
  Widget getBody(BuildContext context, AppCloseModel vm) {

    Widget w;
    List<Widget> lw = [];
    lw.add(_getText("Application indisponible",20));
    lw.add(_getText("L'application ZAMIS Foot",15));
    lw.add(_getText("est actuellement indisponible pour des raisons techniques.",15));
    lw.add(_getText("merci pour votre compréhention.",15));
    if(vm.isAdmin() == true){
      lw.add(_getText("Voulez-vous fermer l'application ?",15));
      lw.add(
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Util.filledButton(context, "Oui", () {
                vm.exit();
              }),
              Util.filledButton(context, "Non", () {
                vm.cancelClose();
              },getSecondary())
            ],
          ));
    }
    else {
      lw.add(
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Util.filledButton(context, "Fermer", () {
                vm.exit();
              }),
            ],
          ));
    }
    w = ListView(
        children: lw);

    Container cont = Container(
        padding:EdgeInsets.all(10),
        child:w
    );
    return (cont);
  }

  Widget _getText(String txt,double size){
    Text t=Text(txt,style: TextStyle(fontSize: size));
    Padding p=Padding(
        padding:EdgeInsets.all(10),
        child:t
    );
    Center c = Center(
        child: p
    );

    return(c);
  }
}
