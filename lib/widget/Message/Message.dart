import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:zamis/model/EntMessage.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/MessageViewModel.dart';
import 'package:zamis/widget/Base/BasePageStatefull.dart';
import 'ElemMessage.dart';

class Message extends StatefulWidget {
  @override
  _MessagePageState createState() => _MessagePageState();
}

class _MessagePageState extends BasePageStatefull<MessageViewModel,Message> {

  @override
  MessageViewModel modelBuilder(Store<AppState> store) {
    return (MessageViewModel.create(store));
  }

  ItemScrollController itemScrollController=ItemScrollController();
  ItemPositionsListener itemPositionListener= ItemPositionsListener.create();
  final ctrlMessage = TextEditingController();

  void sendNotif() {
   /* int typeNotif=Constantes.TYPE_NOTIFICATION_MESSAGE;


    UserService.getInstance().getObsUsers().first.then((lu){
      EntNotification notif = EntNotification(typeNotif, _currentUser!,lu);
      NotificationService.getInstance().sendNotificationFCM(notif);
    });
*/

  }

  _jumpToFirstNewMessage(MessageViewModel vm) {
    if (vm.listeMessage.length > 0) {
      int indexNew = vm.listeMessage.length - 1;
      EntMessage elem;
      for (int i = vm.listeMessage.length - 1; i >= 0; i--) {
        elem = vm.listeMessage[i];
        if (elem.flagNew == true) {
          indexNew = i;
        } else {
          break;
        }
      }
      //print("************** indexNew="+indexNew.toString());
      //itemScrollController.jumpTo(index: indexNew);

      itemScrollController.scrollTo(
          index: indexNew,
          duration: Duration(seconds: 1),
          curve: Curves.easeInOutCubic);
    }
  }


  @override
  void onInit(MessageViewModel vm) {
    Timer(Duration(milliseconds: 50), () => _jumpToFirstNewMessage(vm));
  }

  @override
  void onChange(MessageViewModel vm,BuildContext context) {
    Timer(Duration(milliseconds: 50), () => _jumpToFirstNewMessage(vm));
  }

  @override
  Widget getAppBar(BuildContext context, MessageViewModel vm) {
    return(AppBar(
      title: Text("Messages"),
    ));
  }

  @override
  Widget getBody(BuildContext context, MessageViewModel vm) {
    ListView lv = ListView(
        children: [
          _getBodyMessage(vm),
          _getInputMessage(vm)
        ]);

    return(lv);
  }

  Widget _getInputMessage(MessageViewModel vm) {
    Widget input= Container(
      width: MediaQuery.of(context).size.width - 70,
      margin: const EdgeInsets.only(bottom:8.0),
      padding: EdgeInsets.only(left: 4, right: 4),
      child: TextField(
        minLines: 1,
        maxLines: 5,
        controller: ctrlMessage,
        style: TextStyle(
          fontSize: 18,
        ),

        decoration: InputDecoration(

          filled: true,
          fillColor: Colors.white,
          contentPadding: EdgeInsets.only(top:8,left :20,bottom:4),
          hintStyle: TextStyle(fontSize: 18),
            hintText: "Message",
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide(
                color: Colors.white,
                width: 1,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
               borderSide: BorderSide(
                color: Colors.white,
                width: 1,
              ),
            ),
            ),
      ),
    );

    Widget cmd =  IconButton(
        icon: Icon(Icons.send),
        tooltip: 'Envoyer',
        color:Theme.of(context).colorScheme.primary,
        onPressed: () {
          vm.sendMessage(ctrlMessage.text);
          ctrlMessage.text="";
        });

    ElevatedButton b=ElevatedButton(
        onPressed: () {
          vm.sendMessage(ctrlMessage.text);
          ctrlMessage.text="";
        },
      child: Icon(Icons.send, color: Colors.white),
      style: ElevatedButton.styleFrom(
        shape: CircleBorder(),
        padding: EdgeInsets.all(10),
        primary: getPrimary(), // <-- Button color
        onPrimary: getSecondary(), // <-- Splash color
      ),
    );

    Row w = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [input,b],
    );
    return(w);
  }

  Widget _getBodyMessage(MessageViewModel vm) {

    Widget body = Container(
        height: MediaQuery.of(context).size.height-150,
        margin: const EdgeInsets.only(bottom: 10),
        child: _getScroolViewMessage(vm)
    );
    return (body);
  }

  Widget _getScroolViewMessage(MessageViewModel vm){
    String lastDate = "";
    String strDate = "";

    Widget w=ScrollablePositionedList.builder(
        itemScrollController: itemScrollController,
        itemCount: vm.listeMessage.length,
        itemBuilder: (BuildContext ctxt, int index) {
          bool flagMe = false;
          if (vm.currentUser != null) {
            flagMe = (vm.currentUser!.id == vm.listeMessage[index].userId);
          }
          strDate = DateFormat.MMMMEEEEd("fr_FR")
              .format(vm.listeMessage[index].date);
          Column c;
          List<Widget> lw = [];

          if (strDate != lastDate) {
            lw.add(_getElemDate(strDate));
            lastDate = strDate;
          }
          lw.add(_getElemMesage(index, flagMe,vm));
          c = new Column(children: lw);

          return c;
        }
        );
    return(w);
  }

  Widget _getElemDate(String strDate) {
    Widget w =
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: MediaQuery.of(context).size.width / 2,
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.primary,
                    borderRadius: BorderRadius.circular(20),
                    //border: Border.all()
                  ),
                  margin: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
                  padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
                  child: Text(strDate,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white)))
            ]
    );
    return (w);
  }

  Widget _getElemMesage(int index,bool flagMe,MessageViewModel vm){
   if(index < 0){
     print("index < 0 !!!");
     return(Container());
   }
    Widget w=
    Row(
    mainAxisAlignment: flagMe
    ? MainAxisAlignment.end
        : MainAxisAlignment.start,
    children: <Widget>[
    //Text("dd")
    ElemMessage(vm.listeMessage[index], flagMe)
    ],
    );

   return(w);


  }


}
