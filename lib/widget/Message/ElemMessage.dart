import 'package:collection/src/iterable_extensions.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:zamis/model/EntMessage.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/MessageViewModel.dart';
import 'package:zamis/widget/Base/BaseStatelessWidget.dart';
import '../Avatar/Avatar.dart';

class ElemMessage extends BaseStatelessWidget<MessageViewModel> {

  @override
  MessageViewModel modelBuilder(Store<AppState> store) {
    return (MessageViewModel.create(store));
  }

  late EntMessage _msg;
  late bool _flagMe;

  ElemMessage(EntMessage m, bool flagMe) {
    _msg = m;
    _flagMe = flagMe;
  }


  @override
  Widget getBody(BuildContext context, MessageViewModel vm) {
    Color c = Theme.of(context).colorScheme.primary;
    Color colorNew = Color.fromRGBO(c.red, c.green, c.blue, 0.3);

    return new Container(
        width: (5 * MediaQuery.of(context).size.width) / 6,
        decoration: BoxDecoration(
          color: (_msg.flagNew && !_flagMe) ? colorNew : Color.fromRGBO(230, 230, 230, 1.0),
          borderRadius: BorderRadius.circular(20),
          //border: Border.all()
        ),
        margin: EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
        padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _getHeader(vm),
            _getCorpsText(),
            // _getThumbViewPJ(context),
            /*listePJ.length > 0 ? Column(
              children: listePJ,
            ) : Text("Pas de pj")*/
          ],
        ));
  }



  Widget _getCorpsText() {
    String corps = _msg.message;

    TextStyle textstyleCorps = TextStyle(fontSize: 20);
    return Text(corps, style: textstyleCorps, textAlign: TextAlign.start);
  }


  Widget _getAvatar(EntUser? user) {
    Widget w=Container();
    if(user != null)  w=Padding(padding: EdgeInsets.only(right: 25), child: Avatar(user));
    return(w);
  }

  Widget _getTitre1(EntUser? user) {
    TextStyle textstyleOwner =
        TextStyle(fontWeight: FontWeight.bold, fontSize: 15);

    TextStyle textstylePJ =
        TextStyle(fontWeight: FontWeight.bold, fontSize: 15, color: Colors.red);

    String heure =
        DateFormat('HH:mm').format(_msg.date); //.text.substring(0,indice);

    List<Widget> lh = [];
    lh.add(Text(heure));
    if (_msg.tabPJ.length > 0) {
      //lh.add(Text(" (" + _msg.tabPJ.length.toString() + " pj)",style: textstylePJ));
      //lh.add(Text(_msg.tabPJ.length.toString(),style: textstylePJ));
      lh.add(Padding(
          padding: EdgeInsets.only(left: 5),
          child: Icon(Icons.file_copy_outlined, color: Colors.red, size: 15)));
    }
    Row rHeure = Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //mainAxisSize: MainAxisSize.max,
        children: lh);

    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          _flagMe
              ? Text("Moi", style: textstyleOwner)
              : user != null
                  ? Text(user.getLibelle(), style: textstyleOwner)
                  : Text("?", style: textstyleOwner),
          rHeure
        ]);
  }

  Widget _getTitre2() {
    TextStyle textstyleTitre =
        TextStyle(fontSize: 15, fontStyle: FontStyle.italic);

    String titre = _msg.titre;
    if (titre.startsWith("<") == true) titre = "..";

    return Text(titre, style: textstyleTitre);
  }

  Widget _getHeader(MessageViewModel vm) {

    EntUser? user=vm.listeUser.firstWhereOrNull((u) => u.id == _msg.userId);

    Widget w = new Container(
        padding: EdgeInsets.only(bottom: 25),
        width: (5 * getW()) / 6,
        /*decoration: BoxDecoration(
                  color: Color.fromRGBO(230, 0, 0, 0.5),
                ),*/
        child: new Row(

          //mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              _getAvatar(user),
              Expanded(
                  child: Container(
                      child: Column(
                        //crossAxisAlignment: CrossAxisAlignment.end,

                          children: <Widget>[
                            _getTitre1(user),
                            //_getTitre2()
                          ])))
            ]));

   return(w);
  }
}
