import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/MessageViewModel.dart';
import 'package:zamis/widget/Base/BasePageStatefull.dart';

import '../../viewModel/CompositionModel.dart';

class Composition extends StatefulWidget {
  @override
  _CompositionPageState createState() => _CompositionPageState();
}

class _CompositionPageState extends BasePageStatefull<CompositionViewModel,Composition> {

  @override
  CompositionViewModel modelBuilder(Store<AppState> store) {
    return (CompositionViewModel.create(store));
  }

   void sendNotif() {
   /* int typeNotif=Constantes.TYPE_NOTIFICATION_MESSAGE;


    UserService.getInstance().getObsUsers().first.then((lu){
      EntNotification notif = EntNotification(typeNotif, _currentUser!,lu);
      NotificationService.getInstance().sendNotificationFCM(notif);
    });
*/

  }

  @override
  void onInit(CompositionViewModel vm) {
  }

  @override
  void onChange(CompositionViewModel vm,BuildContext context) {
  }

  @override
  Widget getAppBar(BuildContext context, CompositionViewModel vm) {
    return(AppBar(
      title: Text("Composition"),
    ));
  }

  @override
  Widget getBody(BuildContext context, CompositionViewModel vm) {
    ListView lv = ListView(
        children: [
          Text("To do")
        ]);

    return(lv);
  }

}
