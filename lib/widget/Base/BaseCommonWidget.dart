import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';
import 'package:zamis/common/MyKey.dart';

abstract class  BaseCommonWidget<T extends BaseViewModel> extends StatelessWidget {

  Widget getBody(BuildContext context,T vm){
    return Text("TO DO override");
  }

  void onInit(T vm){

  }

  void onDispose(Store<AppState> store){

  }

  T modelBuilder(Store<AppState> store);

  Color getPrimary(){
    return(Theme.of(MyKey.navKey.currentContext!).colorScheme.primary);
  }

  Color getSecondary(){
    return(Theme.of(MyKey.navKey.currentContext!).colorScheme.secondary);
  }

  double getW(){
    return(MediaQuery.of(MyKey.navKey.currentContext!).size.width);
  }

  double getH(){
    return(MediaQuery.of(MyKey.navKey.currentContext!).size.height);
  }

}