import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';

import 'BaseCommonWidget.dart';

abstract class  BasePageStateless<T extends BaseViewModel> extends BaseCommonWidget<T> {

  Future<bool> confirmePop(BuildContext context,T vm) async{
    return true;
  }

  void onChange(T vm,BuildContext context){

  }

  Widget? getBottomNavBar(BuildContext context,T vm){
    return(null);
  }

  Widget? getAppBar(BuildContext context,T vm){
    return(null);
  }

  PreferredSize? _getPreferredSizeAppBar(BuildContext context,T vm){
    Widget? w = getAppBar(context,vm);
    PreferredSize? ret=null;
    if(w != null) {
      ret = PreferredSize(
          preferredSize: Size.fromHeight(60.0),
          child: w
      );
    }
    return(ret);
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, T>(
        onWillChange: (T? vmPrev,T? vm) {
         /* if((vm != null) && (vm.message != "")) {
            if(vm.typeMessage == Constantes.TYPE_MESSAGE_ERROR){
              Util.error(context,vm.message,"Error",vm.heightDlg);
            };
            if(vm.typeMessage == Constantes.TYPE_MESSAGE_INFO){
              Util.alert(context,vm.message,"Information",vm.heightDlg);
            };
          }*/
          if(vm != null) onChange(vm,context);
        },
        onInitialBuild: (T vm){
          onInit(vm);
        },
        onDispose: (Store<AppState> store){
          onDispose(store);
        },
        converter: (Store<AppState> store) => modelBuilder(store),
        builder: (BuildContext context,T vm) {

          /**/
          Widget w;
          WillPopScope wp =WillPopScope(
              onWillPop: () {
                return (confirmePop(context,vm));
              },
              child:  Scaffold(
                  bottomNavigationBar: getBottomNavBar(context, vm),
                  appBar: _getPreferredSizeAppBar(context, vm),
                  resizeToAvoidBottomInset: false,
                  body:
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Util.loader(context, vm.flagWait),
                        Expanded(
                          child: _setFiligramme(getBody(context, vm), vm.flagWait),
                        ),
                      ]
                  )
              )

          );


          return wp;
        }
    );
  }

  Widget _setFiligramme(Widget w,bool flagWait){
    Widget ret=Material(
        type: MaterialType.transparency,
        child: Container(
        decoration: BoxDecoration(
        image: DecorationImage(
          opacity:  0.1,
          image: AssetImage("assets/images/logo.png"),
          fit: BoxFit.fitWidth,
          alignment:Alignment.bottomCenter
    ),
    ),
    //color: Colors.white,
    child:w
    ));

    return(ret);
  }
}