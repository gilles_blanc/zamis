import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';
import 'package:zamis/common/MyKey.dart';

abstract class BaseStatefullWidget<T extends BaseViewModel,B extends StatefulWidget> extends State<B> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Color getPrimary(){
    return(Theme.of(MyKey.navKey.currentContext!).colorScheme.primary);
  }

  Color getSecondary(){
    return(Theme.of(MyKey.navKey.currentContext!).colorScheme.secondary);
  }

  double getW(){
    return(MediaQuery.of(MyKey.navKey.currentContext!).size.width);
  }

  double getH(){
    return(MediaQuery.of(MyKey.navKey.currentContext!).size.height);
  }

  void onInit(T vm){

  }

  void onChange(T vm,BuildContext context){

  }

  void onDispose(Store<AppState> store){

  }

  T modelBuilder(Store<AppState> store);

  Widget getBody(BuildContext context,T vm){
    return Text("TO DO override");
  }

  @override
  Widget build(BuildContext context) {

    return StoreConnector<AppState, T>(
        onWillChange: (T? vmPrev,T? vm) {
          if(vm != null) onChange(vm,context);
        },
        onInitialBuild: (T vm){
          onInit(vm);
        },
        onDispose: (Store<AppState> store){
          onDispose(store);
        },
        converter: (Store<AppState> store) => modelBuilder(store),
        builder: (BuildContext context,T vm) {

          return getBody(context, vm);
        }
    );
  }


}
