import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';

import 'BaseCommonWidget.dart';

abstract class  BaseStatelessWidget<T extends BaseViewModel> extends BaseCommonWidget<T> {

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, T>(
        onInitialBuild: (T vm){
          onInit(vm);
        },
        onDispose: (Store<AppState> store){
          onDispose(store);
        },
        converter: (Store<AppState> store) => modelBuilder(store),
        builder: (BuildContext context,T vm) {
          return getBody(context, vm);
        }
    );
  }
}