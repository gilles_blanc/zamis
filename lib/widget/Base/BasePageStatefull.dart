import 'dart:async';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';
import 'package:zamis/common/MyKey.dart';

abstract class BasePageStatefull<T extends BaseViewModel,B extends StatefulWidget> extends State<B> with SingleTickerProviderStateMixin {

  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    _controller = AnimationController(
      duration: const Duration(milliseconds: 2000),
      vsync: this,
    );
    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.easeIn,
    );
    _controller.repeat(reverse: true);
    super.initState();
  }

  @override
  void dispose() {
    if(_controller != null) _controller.dispose();
    super.dispose();
  }

  Color getPrimary(){
    return(Theme.of(MyKey.navKey.currentContext!).colorScheme.primary);
  }

  Color getSecondary(){
    return(Theme.of(MyKey.navKey.currentContext!).colorScheme.secondary);
  }

  double getW(){
    return(MediaQuery.of(MyKey.navKey.currentContext!).size.width);
  }

  double getH(){
    return(MediaQuery.of(MyKey.navKey.currentContext!).size.height);
  }

  void onInit(T vm){

  }

  void onChange(T vm,BuildContext context){

  }

  void onDispose(Store<AppState> store){

  }

  T modelBuilder(Store<AppState> store);

  Future<bool> confirmePop(BuildContext context,T vm) async{
    return true;
  }

  Widget? getBottomNavBar(BuildContext context,T vm){
    return(null);
  }

  Widget? getAppBar(BuildContext context,T vm){
    return(null);
  }

  PreferredSize? _getPreferredSizeAppBar(BuildContext context,T vm){
    Widget? w = getAppBar(context,vm);
    PreferredSize? ret=null;
    if(w != null) {
      ret = PreferredSize(
          preferredSize: Size.fromHeight(60.0),
          child: w
      );
    }
    return(ret);
  }

  Widget getBody(BuildContext context,T vm){
    return Text("TO DO override");
  }

  @override
  Widget build(BuildContext context) {

    return StoreConnector<AppState, T>(
        onWillChange: (T? vmPrev,T? vm) {
          /*if((vm != null) && (vm.message != "")) {
            if(vm.typeMessage == Constantes.TYPE_MESSAGE_ERROR){
              Util.error(context,vm.message,"Error",vm.heightDlg);
            };
            if(vm.typeMessage == Constantes.TYPE_MESSAGE_INFO){
              Util.alert(context,vm.message,"Information",vm.heightDlg);
            };
          }*/
          if(vm != null) onChange(vm,context);
        },
        onInitialBuild: (T vm){
          onInit(vm);
        },
        onDispose: (Store<AppState> store){
          onDispose(store);
        },
        converter: (Store<AppState> store) => modelBuilder(store),
        builder: (BuildContext context,T vm) {

          Widget w;
          WillPopScope wp =WillPopScope(
              onWillPop: () {
                return (confirmePop(context,vm));
              },
              child: Scaffold(
                  bottomNavigationBar: getBottomNavBar(context, vm),
                  appBar: _getPreferredSizeAppBar(context, vm),
                  resizeToAvoidBottomInset: true,
                  body:
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        //Util.loader(context, vm.flagWait),
                        Expanded(
                          child: _setFiligramme(getBody(context, vm)),
                        ),
                      ]
                  )
              )
          );

          if(vm.flagWait == true) {
            w=_getLoader(wp);
          }
          else{
            w=wp;
          }
          return w;
        }
    );
  }

  Widget _setFiligramme(Widget w){
    Widget ret=Material(
        type: MaterialType.transparency,
        child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                opacity: 0.1,
                image: AssetImage("assets/images/logo.png"),
                fit: BoxFit.fitWidth,
                alignment:Alignment.center
              ),
            ),
            //color: Colors.white,
            child:w
        ));

    return(ret);
  }

  Widget _getLoader(Widget wp){
    Widget w;

    Column col=Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image(
              image : AssetImage("assets/images/logo-small.png")
          ),
          Util.loader(context, true),
        ],
      );

    Container cont = Container(
      color: getPrimary().withOpacity(0.3),
      //margin: EdgeInsets.all(10),
      width: getW(),
      height: getH(),
      child: col

      /*FadeTransition(
        opacity: _animation,
        child: Image(
            image : AssetImage("assets/images/logo.png")
        ),
      ),*/

    );



    w = Stack(
      children: <Widget>[
        wp,cont
      ],
    );

    return(w);
  }
}
