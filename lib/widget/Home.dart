import 'dart:async';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionInitEntrainement.dart';
import 'package:zamis/action/ActionInitMessage.dart';
import 'package:zamis/action/ActionLogout.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/HomeViewModel.dart';
import '../model/EntFCM.dart';
import 'Activite/ElemActivite.dart';
import 'Avatar/Avatar.dart';
import 'Base/BasePageStateless.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:fluttertoast/fluttertoast.dart';

enum ActionMenu { message,calendrier,parametre,team,deconnexion,joueurCreate, match,equipe, entrainement,composition }

class Home extends BasePageStateless<HomeViewModel> {

  late FToast fToast;

  @override
  void onChange(HomeViewModel vm,BuildContext context) {
    switch(vm.typeMessage){
      case Constantes.TYPE_MESSAGE_ERROR:
        Util.error(context,vm.message,"Error",vm.heightDlg);
        break;
      case  Constantes.TYPE_MESSAGE_INFO:
        Util.alert(context,vm.message,"Information",vm.heightDlg);
        break;
    }

    EntFCM? fcm=vm.getFCM();
    if(fcm != null){
      _showToast(context,vm,fcm.titre,fcm.message,fcm.idUserEmit);

    }

  }

 Widget _getFixedRow(BuildContext context,Widget w,int len){
    Widget ret=Container(
        width: MediaQuery.of(context).size.width-len,
        child : w
    );

    return(ret);
  }

  _showToast(BuildContext context,HomeViewModel vm,String title,String body,String idUser) {

    fToast.init(context);

    EntUser? u=null;
    u=vm.currentUser!;

    Widget w=Container();
    if(u != null) w=Avatar(u);

    Widget mes=Padding(
          padding:EdgeInsets.only(left:10,right:10),
          child:Column(
            children: [
              _getFixedRow(context,Text(title,style:TextStyle(fontSize: 10,color:getSecondary())),150) ,
              _getFixedRow(context,Text(body,style:TextStyle(fontSize: 15,color:getPrimary())),150)
            ],
          )
      );



      Widget toast = Container(
          width: getW() - 10,
          //height: getH()/3,
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25.0),
            border: Border.all(
              color: getPrimary(),
              width: 2,
            ),
            color: Colors.white,
          ),
          child: Row(
            children: [
              w,mes
            ],
          )


      );

      fToast.showToast(
        child: toast,
        gravity: ToastGravity.BOTTOM,
        toastDuration: Duration(seconds: 5),

      );




  }

  void goAlbum() {
    /*EntNotification notif = EntNotification(Constantes.TYPE_NOTIFICATION_PARTICIPATION_MATCH,
      _currentUser!,[_currentUser!],4);
  NotificationService.getInstance().sendNotificationFCM(notif);
  */
  }

  @override
  HomeViewModel modelBuilder(Store<AppState> store) {



    return (HomeViewModel.create(store));
  }

  @override
  void onInit(HomeViewModel vm) {
    initializeDateFormatting("fr_FR", null).then((onValue) {
      vm.action(ActionInitEntrainement());
      vm.action(ActionInitMessage());
    });
    fToast = FToast();


  }


  @override
  Future<bool> confirmePop(BuildContext context,HomeViewModel vm) async {
    bool ret = await Util.confirme(context,
         "Voulez vous vraiment vous déconnecter ?", "Déconnexion");

    if (ret == true) {
      vm.action(ActionLogout());
    }

    return (false);
  }

  @override
  Widget? getAppBar(BuildContext context,HomeViewModel vm){
    String titre="Zamis FC ";
    Widget w = AppBar(
      title: Text(titre),
      leading: vm.currentUser != null ? Container(
        padding: EdgeInsets.only(left: 5, top: 5),
        child: Avatar(vm.currentUser!,Constantes.ACTION_AVATAR_EDIT),
      ) : null,
      actions: <Widget>[
        _getButtonMenu(context,vm)
      ],
    );
    return (w);
  }

  Widget _getButtonMenu(BuildContext context,HomeViewModel vm) {
    if(vm.currentUser == null) return(Container());
    bool flagAdmin=(vm.currentUser!.role == Constantes.ROLE_ADMIN);

    PopupMenuButton menu = PopupMenuButton<ActionMenu>(
        onSelected: (ActionMenu result) {
          _gestMenuAction(result,context,vm);
        },
        icon: Icon(
          Icons.more_vert,
          color: Colors.white,
        ),
        itemBuilder: (BuildContext context) {
          List<PopupMenuEntry<ActionMenu>> l = [];

          l.add(_createItemMenu(ActionMenu.message, Icons.question_answer,
              "Message"));
          l.add(_createItemMenu(ActionMenu.team, Icons.group,
              "Team ZAMIS"));
          l.add(_createItemMenu(ActionMenu.parametre, Icons.settings,
              "Mes infos"));
          l.add(PopupMenuDivider());
          l.add(_createItemMenu(ActionMenu.deconnexion, Icons.exit_to_app,
              "Déconnexion",Colors.red,Colors.red));
          if(flagAdmin == true) {
            l.add(PopupMenuDivider());
            /*l.add(_createItemMenu(
                ActionMenu.entrainement, Icons.calendar_today, "Entrainement"));*/
            l.add(_createItemMenu(ActionMenu.joueurCreate, Icons.group_add,
                "Création joueur"));
            l.add(_createItemMenu(ActionMenu.composition, Icons.group_work,
                "Composition"));
          }

          return (l);
        });
    return (menu);
  }

  _gestMenuAction(ActionMenu action,BuildContext context,HomeViewModel vm) {
    String url = "";

    switch (action) {
      case ActionMenu.joueurCreate:
        EntUser newUser = EntUser();
        vm.nav('/JoueurEdit',newUser);
        break;
      case ActionMenu.composition:
        vm.nav('/Composition');
        break;
      case ActionMenu.entrainement:
        vm.nav('/AdminEntrainement');
        break;
      case ActionMenu.deconnexion:
        confirmePop(context,vm);
        break;
      case ActionMenu.message:
        vm.nav('/Message');
        break;
      case ActionMenu.parametre:
        vm.nav("/JoueurEdit",vm.currentUser!);
        break;
      case ActionMenu.team:
        vm.nav("/Team");
        break;

    }
  }

  PopupMenuItem<ActionMenu> _createItemMenu(ActionMenu action,IconData icon,String libelle,[Color colorText=Colors.black,Color? colorIcon=null]){
    if(colorIcon == null) colorIcon=getPrimary();
    PopupMenuItem<ActionMenu> item=PopupMenuItem<ActionMenu>(
      value: action,
      child: Row(
        children: <Widget>[
          Icon(icon, color: colorIcon),
          Padding(
              padding: EdgeInsets.only(left: 20, top: 2),
              child: Text(libelle,style:TextStyle(color: colorText)))
        ],
      ),
    );

    return(item);
  }

  @override
  Widget getBottomNavBar(BuildContext context, HomeViewModel vm) {
    MainAxisAlignment align=MainAxisAlignment.end;
    List<Widget> lw =[];
    Color colIcon = Colors.white;
    Color colIconParam = Colors.black;
    double iconSize = 30;

    if(vm.currentUser != null) {
      align = MainAxisAlignment.spaceBetween;
      lw.add(Util.getIconBadge(
          vm.nbNewMessage,
          Icon(Icons.question_answer),
          colIcon,
          getSecondary().withOpacity(0.3),
          () {
            vm.nav("/Message");
          },
          iconSize,
          true));

      lw.add(Util.getIconBadge(
          vm.listeUser.length,
          Icon(Icons.group),
          colIcon,
          getSecondary().withOpacity(0.1),
              () {
            vm.nav("/Team");
          },
          iconSize,
          false));


    }

    lw.add(IconButton(
        color: colIconParam,
        icon: Icon(Icons.settings),
        iconSize: iconSize,
        tooltip: 'Mes informations',
        onPressed: () {
          vm.nav("/JoueurEdit",vm.currentUser!);
        }));

    Row r=Row(
        mainAxisAlignment: align,
        children: lw);

    Container cont=Container(
      decoration: BoxDecoration(
            color: getPrimary().withOpacity(0.5)
      ),
        child:r

    );
    return(cont);
  }

  @override
  Widget getBody(BuildContext context, HomeViewModel vm) {

    Widget w;
    w = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
      _getWelcome(vm),
      _getNextEntrainement(context,vm),
       Spacer(flex:1),
      _getNextAnniversaire(context,vm),
    ]);

    return (w);
  }


  Widget _getWelcome(HomeViewModel vm) {
  double sizeWelcome = 20;
  Row r = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text("Bonjour ",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: sizeWelcome,
                    color: Colors.white)),
            Text(vm.currentUser != null ? vm.currentUser!.prenom : "",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: sizeWelcome,
                        color: Colors.white))

          ],
        ),
        //Util.getThumb(_currentUser, context, Constantes.ACTION_CAGNOTTE_VIEW)
      ],
    );

    Container cont = Container(
        margin: const EdgeInsets.all(10.0),
        padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
            //border: Border.all(),
            color: getSecondary(),
            borderRadius: BorderRadius.all(
                Radius.circular(10))), //       <--- BoxDecoration here

        child: r);
    return (cont);
}



  Widget _getNextEntrainement(BuildContext context,HomeViewModel vm) {
    Widget w;

    if(vm.currentUser == null) w=Util.loader(context,true);
    else {
      if (vm.nextTraining != null) {
        w = ElemActivite(vm.nextTraining!);
      }
      else {
        w = Text("Aucun match programmé");
      }
    }
    return(w);

  }


 Widget _getNextAnniversaire(BuildContext context, HomeViewModel vm) {
    double sizeText = 15;
    if (vm.listeAnniversaire.length == 0) {
      return (Text(""));
    }
    ListView lv = new ListView.builder(
        itemCount: vm.listeAnniversaire.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext ctxt, int index) {
          String strDate = DateFormat.MMMMEEEEd("fr_FR")
              .format(vm.listeAnniversaire[index].dateAnniversaire);

          return new Padding(
              padding: EdgeInsets.only(right: 20),
              child: Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(vm.listeAnniversaire[index].user.getLibelle(),
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: sizeText)),
                  Text(" :" + strDate, style: TextStyle(fontSize: sizeText))
                ],
              ));
    });

    String titre = "Anniversaire";
    if (vm.listeAnniversaire.length > 1)
      titre = titre + "s (" + vm.listeAnniversaire.length.toString() + ")";

    Container cont = Container(
        height: 30.0,
        width: getW() / 2,
        margin: const EdgeInsets.all(10.0),
        padding: const EdgeInsets.all(5.0),
        child: Row(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 10),
                child: Text(titre,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: sizeText,
                        color: getPrimary()))),
            Flexible(child: lv)
          ],
    ));

    return (cont);
  }

  /*Widget getTitre(String titre, DateTime d) {
    double sizeTitre = 20;
    Color colTitre = Theme.of(context).colorScheme.primary;
    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(titre,
            style: TextStyle(
                color: colTitre,
                fontWeight: FontWeight.bold,
                fontSize: sizeTitre)),
        d != null ? Text(" (" + Util.getElapse(d) + ")") : Text("")
      ],
    );
    return (r);
  }*/





}
