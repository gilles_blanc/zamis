import 'dart:async';

import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/TeamViewModel.dart';
import 'package:zamis/widget/Base/BasePageStatefull.dart';


import 'ElemJoueur.dart';

class Team extends StatefulWidget {
  @override
  _TeamPageState createState() => _TeamPageState();
}
enum ModeMenu { consultation,notation}

class _TeamPageState extends BasePageStatefull<TeamViewModel,Team> {

  int mode=Constantes.MODE_TEAM_CONSULTATION;
  ElemJoueur? lastJoueurEdit=null;

  ItemScrollController itemScrollController=ItemScrollController();

  @override
  void onInit(TeamViewModel vm) {
    Timer(Duration(milliseconds: 50), () => _jumpToCurrentJoueur(vm));
  }

  @override
  void onChange(TeamViewModel vm,BuildContext context) {
    Timer(Duration(milliseconds: 50), () => _jumpToCurrentJoueur(vm));
  }

  _jumpToCurrentJoueur(TeamViewModel vm) {
    if (vm.listeUser.length > 0) {
      int indexCurrent = 0;
      for (int i = 0 ; i< vm.listeUser.length;i++) {
        if(vm.isMe(vm.listeUser[i].id) == true){
          indexCurrent=i;
          break;
        }
      }
      itemScrollController.scrollTo(
          index: indexCurrent,
          duration: Duration(seconds: 1),
          curve: Curves.easeInOutCubic);
    }
  }

  @override
  TeamViewModel modelBuilder(Store<AppState> store) {
    return (TeamViewModel.create(store));
  }

  @override
  Widget getAppBar(BuildContext context, TeamViewModel vm) {
    String titre1="Team Zamis FC";
    String titre2=mode == Constantes.MODE_TEAM_CONSULTATION ? "informations" : "évaluations";

    return(AppBar(
      title: Column(
        children: [
          Text(titre1),
          Text(titre2,style : TextStyle(fontSize: 12))
        ],
      ) ,

      actions: <Widget>[
        _getButtonMenu()
      ],
    ));
  }

  Widget _getButtonMenu() {

    PopupMenuButton menu = PopupMenuButton<ModeMenu>(
        onSelected: (ModeMenu result) {
          _gestMenuAction(result);
        },
        icon: Icon(
          Icons.more_vert,
          color: Colors.white,
        ),
        itemBuilder: (BuildContext context) {
          List<PopupMenuEntry<ModeMenu>> l = [];

          l.add(_createItemMenu(ModeMenu.consultation, Icons.check,"Informations joueurs",Colors.black,mode == Constantes.MODE_TEAM_CONSULTATION ? getPrimary():Colors.white));
          l.add(_createItemMenu(ModeMenu.notation, Icons.check ,"Notation joueurs",Colors.black,mode != Constantes.MODE_TEAM_CONSULTATION ? getPrimary() : Colors.white ));

          return (l);
        });
    return (menu);
  }

  _gestMenuAction(ModeMenu action) {
    int newMode = Constantes.MODE_TEAM_CONSULTATION;

    switch (action) {
      case ModeMenu.consultation:
        newMode = Constantes.MODE_TEAM_CONSULTATION;
        break;
      case ModeMenu.notation:
        newMode = Constantes.MODE_TEAM_NOTATION;
        break;
    }

    setState(() {
      mode=newMode;
    });
  }

  PopupMenuItem<ModeMenu> _createItemMenu(ModeMenu action,IconData? icon,String libelle,[Color colorText=Colors.black,Color? colorIcon=null]){
    if(colorIcon == null) colorIcon=getPrimary();
    PopupMenuItem<ModeMenu> item=PopupMenuItem<ModeMenu>(
      value: action,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          icon != null ? Icon(icon, color: colorIcon) : Container(),
          Padding(
              padding: EdgeInsets.only(left: 20, top: 2),
              child: Text(libelle,style:TextStyle(color: colorText)))
        ],
      ),
    );

    return(item);
  }

  @override
  Widget getBody(BuildContext context, TeamViewModel vm) {
    List<EntUser> lu = [];
    vm.listeUser.forEach((element) {
      if((vm.isMe(element.id) == false) || (mode == Constantes.MODE_TEAM_CONSULTATION)) lu.add(element);
    });

    lu.sort((u1,u2) {
      int ret=0;
      switch(mode){
        case Constantes.MODE_TEAM_CONSULTATION:
          ret=u1.getNomPrenomFormat().compareTo(u2.getNomPrenomFormat());
          break;
        case Constantes.MODE_TEAM_NOTATION:
          ret=1;
          if(vm.currentUser!.getNoteFromUserId(u1.id) == vm.currentUser!.getNoteFromUserId(u2.id)){
            ret=u1.getNomPrenomFormat().compareTo(u2.getNomPrenomFormat());
          }
          else {
            if (vm.currentUser!.getNoteFromUserId(u1.id) >
                vm.currentUser!.getNoteFromUserId(u2.id)) {
                ret=-1;
            }
          }
          break;
      }
      return(ret);
    });



    return(ScrollablePositionedList.builder(
        itemScrollController: itemScrollController,
        itemCount: lu.length,
        itemBuilder: (BuildContext ctxt, int index) {

          ElemJoueur j=ElemJoueur(editJoueur
          ,lu[index],vm.isMe(lu[index].id),vm.isAdmin(),mode, UniqueKey());


          Widget w;
          if(mode == Constantes.MODE_TEAM_CONSULTATION){
            w=j;
          }
          else{
            w = Badge(
                badgeContent: Text(
                  vm.currentUser!.getNoteFromUserId(lu[index].id).toString(),
                  style: TextStyle(fontWeight: FontWeight.bold, color: getSecondary()),
                ),
                badgeColor: getPrimary(), //Theme.of(context).colorScheme.secondary,
                position: BadgePosition(end: 16, top: 12),
                child: j);
          }
          return (w);
        }));
  }

  void editJoueur(ElemJoueur? j){
    MyKey.elemJoueurEdit = j;
    setState(() {
    });

  }


}
