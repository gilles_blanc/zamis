import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:redux/redux.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/TeamViewModel.dart';
import 'package:zamis/widget/Avatar/Avatar.dart';
import 'package:zamis/widget/Base/BasePageStatefull.dart';
import 'package:collection/src/iterable_extensions.dart';

class JoueurEdit extends StatefulWidget {
  JoueurAdmin() {}

  @override
  _JoueurEditPageState createState() => _JoueurEditPageState();
}

enum OptionMenu { tel,email,message,presence }

class _JoueurEditPageState extends BasePageStatefull<TeamViewModel,JoueurEdit>  {
  final ctrlNom = TextEditingController();
  final ctrlPrenom = TextEditingController();
  final ctrlTel = TextEditingController();
  final ctrlPassword = TextEditingController();
  final ctrlEmail = TextEditingController();
  final ctrlDateNaissance = TextEditingController();
  EntUser? _user;
  bool flagCreate =true;
  bool flagValide=false;

  @override
  TeamViewModel modelBuilder(Store<AppState> store) {
    return (TeamViewModel.create(store));
  }

  @override
  void onInit(TeamViewModel vm) {
    Future.delayed(
      const Duration(milliseconds: 50),
          () {
        _init(vm);
      },
    );
  }


  @override
  void onChange(TeamViewModel vm,BuildContext context) {
    if(_user != null){
      EntUser? u=vm.listeUser.firstWhereOrNull((u) => u.id == _user!.id);
      if(u != null){
        setState(() {
          _user = u;
        });
      }
    }


  }

  void _init(TeamViewModel vm){
    setState(() {
      _user = (ModalRoute.of(context)!.settings.arguments as EntUser).clone();
      if(_user!.id != "") flagCreate=false;
      ctrlEmail.text = _user!.getEMailVisible();
      ctrlNom.text = _user!.nom;
      ctrlPrenom.text = _user!.prenom;
      ctrlPassword.text="";
      ctrlTel.text=_user!.getTelVisible();
      ctrlDateNaissance.text = DateFormat("dd/MM/yyyy").format(_user!.dateNaissance);
      isValide();
    });
  }

  void editUser(TeamViewModel vm) async {
    _user!.nom=ctrlNom.text;
    _user!.prenom=ctrlPrenom.text;
    _user!.setTel(ctrlTel.text);
    _user!.dateNaissance=DateFormat("dd/MM/yyyy").parse(ctrlDateNaissance.text);

    vm.editUser(_user!);

  }

  void createUser(TeamViewModel vm) async {
    _user!.nom=ctrlNom.text;
    _user!.prenom=ctrlPrenom.text;
    _user!.setEmail(ctrlEmail.text);
    _user!.setTel(ctrlTel.text);
    _user!.dateNaissance=DateFormat("dd/MM/yyyy").parse(ctrlDateNaissance.text);
    _user!.role ="joueur";

    vm.createUser(_user!,ctrlPassword.text);


  }


  @override
  Widget getAppBar(BuildContext context, TeamViewModel vm) {
    return(AppBar(
      title: Text(flagCreate ? "Création joueur": vm.isAdmin() ? "Modification Joueur" : "Mes informations"),
    ));
  }

  @override
  Widget getBody(BuildContext context, TeamViewModel vm) {
    if(_user == null) return(Container());
    Column col = Column(
        children: <Widget>[
          Flexible(
            child: userInfo(),
          ),
          Util.filledButton(context,
              flagCreate ? "Création" : vm.isAdmin() ? "Modification" : "Valider",
              flagValide ? flagCreate ? () {
                createUser(vm);
              } : () {
                editUser(vm);
              }: null),
    ],
    );
     return(col);
  }



    isValide(){
      bool ret=false;
      do {
        if(ctrlNom.text == "") break;
        if(ctrlPrenom.text == "") break;
        //if(ctrlTel.text == "") break;
        if(ctrlDateNaissance.text == "") break;

        try {
          DateTime dt = DateFormat("dd/MM/yyyy").parse(ctrlDateNaissance.text);
        }
        catch(e) {
          break;
        }

          if(flagCreate){
            if(ctrlPassword.text.length < 6) break;
            if(ctrlEmail.text == "") break;

          }
          ret=true;

      }while(false);

      //print("IS VALIDE :" + ret.toString());
      setState(() {
        flagValide=ret;
      });
    }

    Widget userInfo() {

      List<Widget> lw = [];

      if(flagCreate) {
        lw.add(_input("Email", ctrlEmail, TextInputType.emailAddress));
        lw.add(_input("Password", ctrlPassword));
      }

      lw.add(_input("Nom",ctrlNom));
      lw.add(_input("Prénom",ctrlPrenom));
      lw.add(_inputTel("Téléphone", ctrlTel));
      lw.add(_inputDate("Date de naissance",ctrlDateNaissance));

      Padding p=Padding(
          padding: EdgeInsets.only(left: 14, right: 14,top : 10),
          child: Row(
            children: [
              Text("Avatar "),
              Avatar(_user!,Constantes.ACTION_AVATAR_EDIT)
            ],
          )
      );

      lw.add(p);
      lw.add(_getOptionMenu());
      return(ListView(
         children: lw,
      ));
    }

  Widget _inputTel(String hint, TextEditingController controller) {
    MaskTextInputFormatter telFormatter = new MaskTextInputFormatter(mask: '## ## ## ## ##', filter: { "#": RegExp(r'[0-9]') });

    /*controller.addListener(() {
      isValide();
    });*/

    TextField tf= TextField(
                    inputFormatters: [telFormatter],
                    keyboardType: TextInputType.number,
                    controller: controller,
                    decoration: new InputDecoration(
                      //hintText: hint,
                        labelText: hint),
                    style: TextStyle(
                      fontSize: 15,
                    )


                );
    Padding p=Padding(
        padding: EdgeInsets.only(left: 14, right: 14),
        child: tf
    );
    return(p);
  }

  Widget _input(String hint, TextEditingController controller,[TextInputType kbType=TextInputType.text]) {

    controller.addListener(() {
      isValide();
    });

    Widget w=TextField(

      keyboardType: kbType,
      decoration: new InputDecoration(
        //hintText: hint,
          labelText: hint),
      style: TextStyle(
        fontSize: 15,
      ),
      controller: controller,


    );

    Padding p=Padding(
        padding: EdgeInsets.only(left: 14, right: 14),
        child: w
    );
    return(p);
  }

  Widget _inputDate(String hint, TextEditingController controller) {
    MaskTextInputFormatter dateFormatter = new MaskTextInputFormatter(mask: '##/##/####', filter: { "#": RegExp(r'[0-9]') });

    controller.addListener(() {
      isValide();
    });

    Row r= Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        new Flexible(
            child: TextField(

              //maxLength: 10,
              keyboardType: TextInputType.number,
              inputFormatters: [dateFormatter],
              decoration: new InputDecoration(
                //hintText: hint,
                  labelText: hint),
              style: TextStyle(
                fontSize: 15,
              ),
              controller: controller,

              /*inputDecoration: new InputDecoration(
                  counterText: "",

                  //hintText: hint,
                  labelText: hint),*/
            )),
        IconButton(
            icon: Icon(Icons.calendar_today),
            tooltip: 'Date du match',
            onPressed:  () {
              modifDate(_user!.dateNaissance);
            } ),
      ],
    );
    Padding p=Padding(
        padding: EdgeInsets.only(left: 14, right: 14),
        child: r
    );
    return(p);
  }

  void modifDate(DateTime initDate) async {
    // Imagine that this function is
    // more complex and slow.
    DateTime? d = await showDatePicker(
        context: context,
        initialDate: DateTime.now().subtract(Duration(days:365 *20)),
        firstDate: DateTime.now().subtract(Duration(days:365 *80)),
        lastDate: DateTime.now().subtract(Duration(days:365 *20)),
        builder: (BuildContext context, Widget? child) {
    return Theme(
    data: ThemeData.light(),
    child: child!,
    );
    },
    );
    if (d != null) {
    d = new DateTime(d.year,d.month,d.day);
    ctrlDateNaissance.text = DateFormat.yMd("fr_FR").format(d);
    _user!.dateNaissance = d;
    }
  }

  Widget _getOptionMenu(){
    Widget w= PopupMenuButton<OptionMenu>(
        /*onSelected: (OptionMenu result) {
            //_gestMenuAction(result);
        },*/
      enableFeedback: true,
        elevation: 10,
          child:
          Text("Options ..."),
          /*icon: Icon(
            Icons.more_vert,
            color: Theme.of(context).colorScheme.primary,
          ),*/
          itemBuilder: (BuildContext context) {
            List<PopupMenuEntry<OptionMenu>> l = [];


            l.add(PopupMenuItem<OptionMenu>(
              value: OptionMenu.tel,
              child: StatefulBuilder(
                builder: (_context, _setState) =>
                    CheckboxListTile(
                        checkColor: Colors.white,
                        activeColor: Theme.of(context).colorScheme.primary,
                        value: _user!.flagTelVisible,
                        onChanged: (value) =>
                            _setState(() =>_user!.flagTelVisible = value!),
                        title: Text('N° téléphone visible',style: TextStyle(
                            fontSize: 15))),
              ),
            ));

            l.add(PopupMenuItem<OptionMenu>(
              value: OptionMenu.email,
              child: StatefulBuilder(
                builder: (_context, _setState) =>
                    CheckboxListTile(
                        checkColor: Colors.white,
                        activeColor: Theme.of(context).colorScheme.primary,
                        value: _user!.flagMailVisible,
                        onChanged: (value) =>
                            _setState(() =>_user!.flagMailVisible = value!),
                        title: Text('Mail visible',style: TextStyle(
                            fontSize: 15))),
              ),
            ));

            l.add(PopupMenuItem<OptionMenu>(
              value: OptionMenu.presence,
              child: StatefulBuilder(
                builder: (_context, _setState) =>
                    CheckboxListTile(
                        checkColor: Colors.white,
                        activeColor: Theme.of(context).colorScheme.primary,
                        value: _user!.flagNotifPresence,
                        onChanged: (value) =>
                            _setState(() =>_user!.flagNotifPresence = value!),
                        title: Text('Notifications presences',style: TextStyle(
                            fontSize: 15))),
              ),
            ));

            l.add(PopupMenuItem<OptionMenu>(
              value: OptionMenu.message,
              child: StatefulBuilder(
                builder: (_context, _setState) =>
                    CheckboxListTile(
                        checkColor: Colors.white,
                        activeColor: Theme.of(context).colorScheme.primary,
                        value: _user!.flagNotifMessage,
                        onChanged: (value) =>
                            _setState(() =>_user!.flagNotifMessage = value!),
                        title: Text('Notifications messages',style: TextStyle(
                            fontSize: 15))),
              ),
            ));

            return (l);
          });

    Padding p=Padding(
        padding: EdgeInsets.only(left: 14, right: 14,top:10),
        child: w
    );
      return (p);
    }



}

