import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/common/NumberWrapper.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/TeamViewModel.dart';
import 'package:zamis/widget/Avatar/Avatar.dart';
import 'package:zamis/widget/Base/BaseStatefullWidget.dart';
import 'package:zamis/widget/Base/BaseStatelessWidget.dart';

enum ActionMenu { telephone, sms, mail }

class ElemJoueur extends StatefulWidget {

  late EntUser user;
  late bool flagMe;
  late bool flagAdmin;
  late int mode;
  late bool flagEdit;
  late Function(ElemJoueur? j) onEdit;

  ElemJoueur(void onEdit(ElemJoueur? j),EntUser u,bool me,bool admin,int mode,Key key) : super(key :key) {
    user = u;
    flagMe = me;
    flagAdmin=admin;
    this.mode = mode;
    this.onEdit = onEdit;
    this.flagEdit=false;
    if(MyKey.elemJoueurEdit != null){
      if(MyKey.elemJoueurEdit!.user.id == u.id) flagEdit=true;
    }



  }

  @override
  _ElemJoueurPageState createState() => _ElemJoueurPageState();
}

class _ElemJoueurPageState extends BaseStatefullWidget<TeamViewModel,ElemJoueur>  {

  @override
  TeamViewModel modelBuilder(Store<AppState> store) {
    return (TeamViewModel.create(store));
  }



  int actionAvatar = Constantes.ACTION_AVATAR_NONE;
  //NumberWrapper nbW= NumberWrapper();
  int note=0;

  @override
  void onInit(TeamViewModel vm) {
    if(widget.flagAdmin) {
      actionAvatar = Constantes.ACTION_AVATAR_ADMIN;
    }
    else {
      if(widget.flagMe) {
        actionAvatar = Constantes.ACTION_AVATAR_EDIT;
      }
    }

  }

  /*@override
  void onChange(TeamViewModel vm, BuildContext context) {
    nbW.note=vm.currentUser!.getNoteFromUserId(widget.user.id);
  }*/

  _gestMenuAction(ActionMenu action) {
    String url = "";

    switch (action) {
      case ActionMenu.telephone:
        url = "tel:" + widget.user.getTel();
        break;
      case ActionMenu.sms:
        url = "sms:" + widget.user.getTel();
        break;
      case ActionMenu.mail:
        url = "mailto:" + widget.user.getEMail();
        break;
    }
    launch(url);
  }

  PopupMenuButton _getButtonMenu(BuildContext context) {
    PopupMenuButton menu = PopupMenuButton<ActionMenu>(
        onSelected: (ActionMenu result) {
          _gestMenuAction(result);
        },
        icon: Icon(
          Icons.more_vert,
          color: Theme.of(context).colorScheme.primary,
        ),
        itemBuilder: (BuildContext context) {
          List<PopupMenuEntry<ActionMenu>> l = [];


          if(!kIsWeb) l.add(PopupMenuItem<ActionMenu>(
            value: ActionMenu.telephone,
            child: Row(
              children: <Widget>[
                Icon(Icons.phone, color: Theme.of(context).colorScheme.primary),
                Padding(
                    padding: EdgeInsets.only(left: 20, top: 2),
                    child: Text('Téléphoner'))
              ],
            ),
            enabled: widget.user.getTel() != "",
          ));

          if(!kIsWeb) l.add(PopupMenuItem<ActionMenu>(
            value: ActionMenu.sms,
            child: Row(
              children: <Widget>[
                Icon(Icons.message,
                    color: Theme.of(context).colorScheme.primary),
                Padding(
                    padding: EdgeInsets.only(left: 20, top: 2),
                    child: Text('Envoyer SMS'))
              ],
            ),
            enabled: widget.user.getTel() != "",
          ));

          l.add(PopupMenuItem<ActionMenu>(
            value: ActionMenu.mail,
            child: Row(
              children: <Widget>[
                Icon(Icons.mail, color: Theme.of(context).colorScheme.primary),
                Padding(
                    padding: EdgeInsets.only(left: 20, top: 2),
                    child: Text('Envoyer MAIL'))
              ],
            ),
            enabled: widget.user.getEMail() != "",
          ));

          return (l);
        });
    return (menu);
  }


  _majNote(TeamViewModel vm){
    vm.majUserNote(widget.user.id,note);
    widget.onEdit(null);
  }

  @override
  Widget getBody(BuildContext context, TeamViewModel vm) {
    List<Widget> lw =[];

    lw.add(Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width-130,
          //color:Colors.red,
          child: Wrap(
            children: [
              Text(widget.user.getNomFormat(),style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
              Text(" " + widget.user.getPrenomFormat(),style: TextStyle(fontSize: 20)),
            ],
          ),
        ),

        widget.mode == Constantes.MODE_TEAM_CONSULTATION ? _getButtonMenu(context) : Container()
      ],
    ),);

    if(widget.mode == Constantes.MODE_TEAM_CONSULTATION){
      lw.add(Row(
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width-100,
              //color:Colors.red,
              child: widget.user.getEMail() != ""
                  ? Text(widget.user.getEMail(), style: TextStyle(fontSize: 15))
                  : Text("", style: TextStyle(fontSize: 15))
          )


        ],
      ));
      lw.add(Row(
        children: <Widget>[
          widget.user.getTel() != ""
              ? Text(widget.user.getTel(), style: TextStyle(fontSize: 15))
              : Text("", style: TextStyle(fontSize: 15))
        ],
      ));
    }
    else{
      lw.add(_getInputNotation(context,vm));
    }

    Container c = Container(
      margin: EdgeInsets.only(top: 5, bottom: 5, right: 10, left: 10),
      padding: EdgeInsets.only(left: 5, bottom: 10),
      //color: Colors.orangeAccent,
      decoration: BoxDecoration(
          border: Border.all(
            color: Theme.of(context).colorScheme.primary,
            width: widget.flagMe ? 2 : 2,
          ),
          borderRadius: BorderRadius.circular(12),
          color: widget.flagMe ? /*Theme.of(context).colorScheme.secondaryVariant*/Colors.black12 : Colors.white
      ),
      child: Row(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(right:10),
              child : Avatar(widget.user, actionAvatar)
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: lw,
          ),
        ],
      ),
    );

    GestureDetector gd = GestureDetector(
        onTap: () {
          if(widget.mode != Constantes.MODE_TEAM_CONSULTATION){
            setState(() {
              if(widget.mode == Constantes.MODE_TEAM_NOTATION){
                widget.onEdit(widget);
              }
            });
          }
        },

        child: c);

    return (gd);
  }

  Widget getLimite(BuildContext context,Widget w){
    Container cont =Container(
      width: MediaQuery.of(context).size.width/2,
      color:Colors.green,
      child: w,
    );
    return(cont);
  }

  Widget _getInputNotation(BuildContext context,TeamViewModel vm){
    Widget w=Container();
        if(widget.flagEdit == true) {
          if(note == 0) note = vm.currentUser!.getNoteFromUserId(widget.user.id);
          w = Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Util.saisieNotation(context, note, "", (value) {
                setState(() {
                  note = value;
                });
              }),
              IconButton(
                  color: Colors.green,
                  icon: Icon(Icons.check_circle),

                  //padding: EdgeInsets.all(0),
                  constraints: BoxConstraints(),
                  tooltip: 'Enregistrer',
                  iconSize: 30,
                  onPressed: () {
                    _majNote(vm);
                  }),
              IconButton(
                  color: Colors.red,
                  iconSize: 30,
                  icon: Icon(Icons.cancel),
                  //padding: EdgeInsets.all(0),
                  constraints: BoxConstraints(),
                  tooltip: 'Enregistrer',
                  onPressed: () {
                    widget.onEdit(null);
                  })
            ],
          );
        }

    return(w);
  }

}
