import 'dart:async';

class BaseMiddleware {


  late List<StreamSubscription<dynamic>> listSub;

  BaseMiddleware() {
    listSub=[];
  }

  addSub(StreamSubscription<dynamic> sub){
    listSub.add(sub);
  }

  clearSubs(){
    listSub.forEach((element) {
      element.cancel();
    });
    listSub.clear();
  }

}





