import 'package:flutter/widgets.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/state/AppState.dart';

import 'BaseMiddleware.dart';

class NavigateMiddleware extends BaseMiddleware {
  static NavigateMiddleware _singleton=NavigateMiddleware._();

  factory NavigateMiddleware() {
    return _singleton;
  }

  NavigateMiddleware._() {

  }

  Middleware<AppState> navigate() {
    return (Store<AppState> store, action, NextDispatcher next) {
      String target = (action as ActionNavigate).target;
      bool flagRet = (action as ActionNavigate).flagRet;
      Object? data = (action as ActionNavigate).data;

      if (target == "") {
        MyKey.navKey.currentState!.pop();
      }
      else {
        if (flagRet == false) {
          //print("NAV remove !!! ");
          MyKey.navKey.currentState!.pushReplacementNamed(
              target, arguments: data);
        }
        else
          MyKey.navKey.currentState!.pushNamed(target, arguments: data);
      }
    };
  }
}

