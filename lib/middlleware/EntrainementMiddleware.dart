import 'package:redux/redux.dart';
import 'package:zamis/action/ActionNextEntrainement.dart';
import 'package:zamis/model/EntEntrainement.dart';
import 'package:zamis/service/EntrainementService.dart';
import 'package:zamis/service/PresenceActiviteService.dart';
import 'package:zamis/state/AppState.dart';
import 'BaseMiddleware.dart';
import 'package:collection/src/iterable_extensions.dart';

class EntrainementMiddleware extends BaseMiddleware{

  static EntrainementMiddleware _singleton=EntrainementMiddleware._();

  factory EntrainementMiddleware() {
    return _singleton;
  }

  EntrainementMiddleware._() {

  }

  List<EntEntrainement>? _listTraining=null;
  List<EntEntrainement>? _listInterrupTraining=null;

  Middleware<AppState> init() {
    return (Store<AppState> store, action, NextDispatcher next) async {

      clearSubs();
      PresenceActiviteService.getInstance().cleanOldPresenceActivite();
      EntrainementService.getInstance().cleanOldEntrainement();
      EntrainementService.getInstance().initListenerTraining();
      EntrainementService.getInstance().initListenerInterruption();

      addSub(EntrainementService.getInstance().getObsNextEntrainement().listen((listTraining) {
        _listTraining=listTraining;

        _consolideTraining(store);
      }));

      addSub(EntrainementService.getInstance().getObsInterruption().listen((listInterrupTraining) {
        _listInterrupTraining=listInterrupTraining;
        _consolideTraining(store);
      }));

    };

  }

  _consolideTraining(Store<AppState> store){
    if((_listTraining != null) && (_listInterrupTraining != null)){
      DateTime next=EntrainementService.getInstance().findNextDateTraining(_listInterrupTraining!);

      EntEntrainement? nextTraining = _listTraining!.firstWhereOrNull((e) {
        bool ret=false;
        if(e.date.compareTo(next) == 0) ret=true;
        return(ret);
      });

      if(nextTraining == null){
        nextTraining = EntEntrainement();
        nextTraining.date=next;
        EntrainementService.getInstance().majTraining(nextTraining);
      }
      else{
        store.dispatch(new ActionNextEntrainement(nextEntrainement :nextTraining));
      }
    }

  }


}





