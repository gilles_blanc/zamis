import 'package:redux/redux.dart';
import 'package:zamis/action/ActionAddMessage.dart';
import 'package:zamis/action/ActionCreateUser.dart';
import 'package:zamis/action/ActionInitApp.dart';
import 'package:zamis/action/ActionInitAuthent.dart';
import 'package:zamis/action/ActionInitEntrainement.dart';
import 'package:zamis/action/ActionInitMessage.dart';
import 'package:zamis/action/ActionInitPresenceActivite.dart';
import 'package:zamis/action/ActionLogin.dart';
import 'package:zamis/action/ActionLoginUser.dart';
import 'package:zamis/action/ActionLogout.dart';
import 'package:zamis/action/ActionLogoutUser.dart';
import 'package:zamis/action/ActionMajAvatar.dart';
import 'package:zamis/action/ActionMajUser.dart';
import 'package:zamis/action/ActionMajUserNote.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/action/ActionResetPassword.dart';
import 'package:zamis/action/ActionRestorePersist.dart';
import 'package:zamis/action/ActionSavePersist.dart';
import 'package:zamis/action/ActionTogglePresenceActivite.dart';
import 'package:zamis/middlleware/EntrainementMiddleware.dart';
import 'package:zamis/middlleware/InitAppMiddleware.dart';
import 'package:zamis/middlleware/PresenceActiviteMiddleware.dart';
import 'package:zamis/state/AppState.dart';
import '../action/ActionInitFCM.dart';
import 'AuthentMiddleware.dart';
import 'FCMMiddleware.dart';
import 'MessageMiddleware.dart';
import 'NavigateMiddleware.dart';
import 'PersistMiddleware.dart';
import 'UserMiddleware.dart';


List<Middleware<AppState>> getMiddlewares() {
    return [
      // App
      TypedMiddleware<AppState, ActionInitApp>(InitAppMiddleware().initApp()),

      // Navigte
      TypedMiddleware<AppState, ActionNavigate>(NavigateMiddleware().navigate()),

      // Persist
      TypedMiddleware<AppState, ActionRestorePersist>(PersistMiddleware().restorePersist()),
      TypedMiddleware<AppState, ActionSavePersist>(PersistMiddleware().savePersist()),

      // Authent
      TypedMiddleware<AppState, ActionInitAuthent>(AuthentMiddleware().initAuthentListener()),
      TypedMiddleware<AppState, ActionLogin>(AuthentMiddleware().login()),
      TypedMiddleware<AppState, ActionResetPassword>(AuthentMiddleware().resetPassword()),
      TypedMiddleware<AppState, ActionLogout>(AuthentMiddleware().logout()),
      TypedMiddleware<AppState, ActionCreateUser>(AuthentMiddleware().createUser()),

      // User
      TypedMiddleware<AppState, ActionLoginUser>(UserMiddleware().userLogin()),
      TypedMiddleware<AppState, ActionLogoutUser>(UserMiddleware().userLogout()),
      TypedMiddleware<AppState, ActionMajAvatar>(UserMiddleware().majAvatar()),
      TypedMiddleware<AppState, ActionMajUser>(UserMiddleware().majUser()),
      TypedMiddleware<AppState, ActionMajUserNote>(UserMiddleware().majUserNote()),

      // Entrainement
      TypedMiddleware<AppState, ActionInitEntrainement>(EntrainementMiddleware().init()),

      // Message
      TypedMiddleware<AppState, ActionInitMessage>(MessageMiddleware().init()),
      TypedMiddleware<AppState, ActionAddMessage>(MessageMiddleware().sendMessage()),

      // PresenceActivite
      TypedMiddleware<AppState, ActionInitPresenceActivite>(PresenceActiviteMiddleware().init()),
      TypedMiddleware<AppState, ActionTogglePresenceActivite>(PresenceActiviteMiddleware().toggle()),

      // FCM
      TypedMiddleware<AppState, ActionInitFCM>(FCMMiddleware().init()),

    ];
}
