import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionInitFCM.dart';
import 'package:zamis/action/ActionLoginUser.dart';
import 'package:zamis/action/ActionMajAvatar.dart';
import 'package:zamis/action/ActionMajUser.dart';
import 'package:zamis/action/ActionMajUserNote.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/action/ActionStatus.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/model/StatusUpload.dart';
import 'package:zamis/service/BinaryService.dart';
import 'package:zamis/service/UserService.dart';
import 'package:zamis/state/AppState.dart';

import 'BaseMiddleware.dart';

class UserMiddleware extends BaseMiddleware {

  static UserMiddleware _singleton=UserMiddleware._();

  factory UserMiddleware() {
    return _singleton;
  }

  UserMiddleware._() {

  }

  Middleware<AppState> userLogin() {
    return (Store<AppState> store, action, NextDispatcher next) async {
      //print("***** CALL MIDDLEWARE USER ******");

      clearSubs();

      UserService.getInstance().initListenerListUser();
      addSub(UserService.getInstance().getObsUsers().listen((listUser) {
        //print("***** EVENT USER ******");
        User userFB = (action as ActionLoginUser).uFB!;
        EntUser currentUser=listUser.firstWhere((element) => element.id == userFB.uid);
        store.dispatch(ActionInitFCM(currentUser));

        ActionLoginUser a = ActionLoginUser(currenrUser: currentUser,listeUser: listUser,uFB:userFB);
        next(a);
      }));

      addSub(UserService.getInstance().getObsAnniversaires().listen((listAnniversaire) {
        ActionLoginUser a = ActionLoginUser(listeAnniversaire:listAnniversaire);
        next(a);
      }));

      //print("*** SS=" + ss.hashCode.toString());

    };
  }

  Middleware<AppState> userLogout() {
    return (Store<AppState> store, action, NextDispatcher next) async {
      UserService.getInstance().cancelListenerListUser();
      clearSubs();
      next(action);
    };
  }

  Middleware<AppState> majAvatar() {
    return (Store<AppState> store, action, NextDispatcher next) async {

      store.dispatch(new ActionStatus(flagWait: true));

      StatusUpload su=await BinaryService.getInstance().majAvatar((action as ActionMajAvatar).buffer,(action as ActionMajAvatar).user);
      if(su.status == false){
        store.dispatch(new ActionStatus(titre :"Maj Avatar",
            message : su.reason + " " + su.strType,
            typeMessage :Constantes.TYPE_MESSAGE_ERROR));
      }
      else{
        (action as ActionMajAvatar).user.urlAvatar=su.url;
        UserService.getInstance().majUser((action as ActionMajAvatar).user).then((ret){
          store.dispatch(new ActionStatus());
          next(action);
        }).catchError((e){
          store.dispatch(new ActionStatus(titre :"Maj Avatar",
                message : "erreur lors de la maj du user :" + e.toString(),
                typeMessage :Constantes.TYPE_MESSAGE_ERROR));
       });
      }
    };
  }

  Middleware<AppState> majUser() {
    return (Store<AppState> store, action, NextDispatcher next) async {

      store.dispatch(new ActionStatus(flagWait: true));

      UserService.getInstance().majUser((action as ActionMajUser).user).then((ret){
        store.dispatch(new ActionStatus());
        if((action as ActionMajUser).flagPop == true){
          store.dispatch(new ActionNavigate());
        }
        next(action);
      }).catchError((e){
        store.dispatch(new ActionStatus(titre :"Maj Avatar",
            message : "erreur lors de la maj du user :" + e.toString(),
            typeMessage :Constantes.TYPE_MESSAGE_ERROR));
      });
    };
  }

  Middleware<AppState> majUserNote() {
    return (Store<AppState> store, action, NextDispatcher next) async {

      store.dispatch(new ActionStatus(flagWait: true));

      UserService.getInstance().majUserNote((action as ActionMajUserNote).idUser,(action as ActionMajUserNote).idUserNote,(action as ActionMajUserNote).note).then((ret){
        store.dispatch(new ActionStatus());

        next(action);
      }).catchError((e){
        store.dispatch(new ActionStatus(titre :"Maj User Note",
            message : "erreur lors de la maj du user :" + e.toString(),
            typeMessage :Constantes.TYPE_MESSAGE_ERROR));
      });
    };
  }

}





