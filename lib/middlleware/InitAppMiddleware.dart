import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionCreateUser.dart';
import 'package:zamis/action/ActionInitAuthent.dart';
import 'package:zamis/action/ActionLogin.dart';
import 'package:zamis/action/ActionLogoutUser.dart';
import 'package:zamis/action/ActionMajUser.dart';
import 'package:zamis/action/ActionParam.dart';
import 'package:zamis/action/ActionRestorePersist.dart';
import 'package:zamis/action/ActionStatus.dart';
import 'package:zamis/action/ActionLoginUser.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/action/ActionResetPassword.dart';
import 'package:zamis/action/ActionVersion.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/service/AuthService.dart';
import 'package:zamis/service/ParamService.dart';
import 'package:zamis/state/AppState.dart';

import 'BaseMiddleware.dart';

class InitAppMiddleware extends BaseMiddleware{

  static InitAppMiddleware _singleton=InitAppMiddleware._();

  factory InitAppMiddleware() {
    return _singleton;
  }

  InitAppMiddleware._() {

  }

  Middleware<AppState> initApp() {
    return (Store<AppState> store, action, NextDispatcher next) async {

      if (!kIsWeb) {
        if(kDebugMode){
          MyKey.appFB= await Firebase.initializeApp(options: Util.getFirebaseOptions());
       }
        else{
          MyKey.appFB=await Firebase.initializeApp(options: Util.getFirebaseOptions());
        }
      }
      FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

      ParamService.getInstance().getObsParam().listen((param) async {
        if(param.open_appli == false){
          store.dispatch(new ActionNavigate("/AppClose",null,false));
        }
        else{
          store.dispatch(new ActionParam(param:param));
          if (!kIsWeb) {
            PackageInfo packageInfo = await PackageInfo.fromPlatform();
            store.dispatch(new ActionVersion(version: packageInfo.version,nrBuild:packageInfo.buildNumber));
            if (int.parse(packageInfo.buildNumber) < param.version_android) {
              store.dispatch(new ActionNavigate("/NewVersion", null, false));
            }
            else{
              store.dispatch(new ActionInitAuthent());
            }
          }
          else{
            store.dispatch(new ActionInitAuthent());
          }
        }
      });

      next(action);
    };
  }


}


