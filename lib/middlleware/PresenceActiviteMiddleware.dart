import 'package:redux/redux.dart';
import 'package:zamis/action/ActionInitPresenceActivite.dart';
import 'package:zamis/action/ActionPresenceActivite.dart';
import 'package:zamis/action/ActionTogglePresenceActivite.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/model/EntPresenceActivite.dart';
import 'package:zamis/service/PresenceActiviteService.dart';
import 'package:zamis/state/AppState.dart';
import 'BaseMiddleware.dart';

class PresenceActiviteMiddleware extends BaseMiddleware{

  static PresenceActiviteMiddleware _singleton=PresenceActiviteMiddleware._();

  factory PresenceActiviteMiddleware() {
    return _singleton;
  }

  PresenceActiviteMiddleware._() {

  }

  Map<String,EntPresenceActivite> _dicPresenceActivite = Map();

  /*Middleware<AppState> exit() {
    return (Store<AppState> store, action, NextDispatcher next) async {
      PresenceActiviteService.getInstance().cancelListener();
      clearSubs();
      _dicPresenceActivite.clear();
      next(action);
    };
  }*/

  Middleware<AppState> init() {
    return (Store<AppState> store, action, NextDispatcher next) async {

      String idActivite=(action as ActionInitPresenceActivite).idActivite!;
      String idUser=(action as ActionInitPresenceActivite).idUSer!;
      if(!_dicPresenceActivite.containsKey(idActivite)) {
        EntPresenceActivite pa = EntPresenceActivite();
        pa.idUser=idUser;
        pa.idActivite=idActivite;
        _dicPresenceActivite[idActivite]=pa;
        addSub(PresenceActiviteService.getInstance().getObsListPresenceActivites(idActivite).listen((listPresenceActivite) {
          _analysePresence(listPresenceActivite,idUser,pa);
          store.dispatch(new ActionPresenceActivite(_dicPresenceActivite));
        }));
      }
    };
  }

  Middleware<AppState> toggle() {
    return (Store<AppState> store, action, NextDispatcher next) async {

      EntPresenceActivite pa=(action as ActionTogglePresenceActivite).pa;

      if(pa.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_YES) pa.codePresence =Constantes.CODE_PRESENCE_ACTIVITE_NO;
      else pa.codePresence =Constantes.CODE_PRESENCE_ACTIVITE_YES;

      PresenceActiviteService.getInstance().majPresenceActivite(pa);

      //_sendEvent(pa);
    };
  }


  void _sendEvent(EntPresenceActivite pa){



  }

  void _analysePresence(List<EntPresenceActivite> lpa,String idUser,EntPresenceActivite pa){
    pa.listIdAbsent=[];
    pa.listIdIndecis=[];
    pa.listIdPresent=[];

    EntPresenceActivite? ep=null;

    lpa.forEach((element) {
      if(element.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_YES) pa.listIdPresent.add(element.idUser);
      if(element.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_NO) pa.listIdAbsent.add(element.idUser);
      if(element.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_UNKNOWN) pa.listIdIndecis.add(element.idUser);

      if(element.idUser == idUser){
          pa.codePresence=element.codePresence;
          pa.id=element.id;
      }
    });
  }
}





