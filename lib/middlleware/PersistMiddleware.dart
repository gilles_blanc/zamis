import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionRestorePersist.dart';
import 'package:zamis/action/ActionLogin.dart';
import 'package:zamis/action/ActionSavePersist.dart';
import 'package:zamis/action/ActionStatus.dart';
import 'package:zamis/action/ActionLoginUser.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/action/ActionResetPassword.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/service/AuthService.dart';
import 'package:zamis/state/AppState.dart';

import 'BaseMiddleware.dart';

class PersistMiddleware extends BaseMiddleware {

  static PersistMiddleware _singleton=PersistMiddleware._();

  factory PersistMiddleware() {
    return _singleton;
  }

  PersistMiddleware._() {

  }

  Middleware<AppState> restorePersist() {
    return (Store<AppState> store, action, NextDispatcher next) async {
      bool flagPersistLogin = await Util.getPersist(
          Constantes.PERSIST_REMEMBER_CREDENTIALS);
      String password = "";
      String email = "";
      if (flagPersistLogin == true) {
        password = await Util.getPersist(Constantes.PERSIST_PASSWORD);
        email = await Util.getPersist(Constantes.PERSIST_EMAIL);
      }
      next(ActionRestorePersist(flagPersistLogin: flagPersistLogin,
          password: password,
          email: email));
    };
  }

  Middleware<AppState> savePersist() {
    return (Store<AppState> store, action, NextDispatcher next) async {
      bool? flagPersistLogin = (action as ActionSavePersist).flagPersistLogin;
      String? email = (action as ActionSavePersist).email;
      String? password = (action as ActionSavePersist).password;

      if (flagPersistLogin != null) {
        Util.setPersist(
            Constantes.PERSIST_REMEMBER_CREDENTIALS, flagPersistLogin);
        if (flagPersistLogin == true) {
          if(password == null) password="";
          if(email == null) email="";
          Util.setPersist(Constantes.PERSIST_PASSWORD, password);
          Util.setPersist(Constantes.PERSIST_EMAIL, email);
        }
        else {
          Util.setPersist(Constantes.PERSIST_PASSWORD, "");
          Util.setPersist(Constantes.PERSIST_EMAIL, "");
        }
      }

      next(ActionRestorePersist(flagPersistLogin: flagPersistLogin,
          email: email,
          password: password));
    };
  }
}
