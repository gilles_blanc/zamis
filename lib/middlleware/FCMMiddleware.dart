import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionCreateUser.dart';
import 'package:zamis/action/ActionInitFCM.dart';
import 'package:zamis/action/ActionLogin.dart';
import 'package:zamis/action/ActionLogoutUser.dart';
import 'package:zamis/action/ActionMajUser.dart';
import 'package:zamis/action/ActionNewFCM.dart';
import 'package:zamis/action/ActionRestorePersist.dart';
import 'package:zamis/action/ActionStatus.dart';
import 'package:zamis/action/ActionLoginUser.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/action/ActionResetPassword.dart';
import 'package:zamis/action/ActionVersion.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/service/AuthService.dart';
import 'package:zamis/state/AppState.dart';

import '../service/NotificationService.dart';
import '../service/UserService.dart';
import 'BaseMiddleware.dart';

class FCMMiddleware extends BaseMiddleware{

  static FCMMiddleware _singleton=FCMMiddleware._();

  factory FCMMiddleware() {
    return _singleton;
  }

  FCMMiddleware._() {

  }

  Middleware<AppState> init() {
    return (Store<AppState> store, action, NextDispatcher next) async {

      NotificationService.getInstance().initListenerFCM();

      NotificationService.getInstance().getUserTokenFCM().then((token){
        print("token MOB courant (user) :" + (action as ActionInitFCM).currentUser.tokenFCMMob);
        print("token WEB courant (user) :" + (action as ActionInitFCM).currentUser.tokenFCMWeb);
        print("token récupéré :" + token!);
        UserService.getInstance().majTokenFCM((action as ActionInitFCM).currentUser,token);
      });

      clearSubs();
      addSub(NotificationService.getInstance().getObsFCM().listen((fcm) async {
        store.dispatch(new ActionNewFCM(fcm));
      }));

      next(action);
    };
  }



}


