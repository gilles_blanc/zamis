import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionCreateUser.dart';
import 'package:zamis/action/ActionLogin.dart';
import 'package:zamis/action/ActionLogoutUser.dart';
import 'package:zamis/action/ActionMajUser.dart';
import 'package:zamis/action/ActionRestorePersist.dart';
import 'package:zamis/action/ActionStatus.dart';
import 'package:zamis/action/ActionLoginUser.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/action/ActionResetPassword.dart';
import 'package:zamis/action/ActionVersion.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/service/AuthService.dart';
import 'package:zamis/state/AppState.dart';

import 'BaseMiddleware.dart';

class AuthentMiddleware extends BaseMiddleware{

  static AuthentMiddleware _singleton=AuthentMiddleware._();

  factory AuthentMiddleware() {
    return _singleton;
  }

  AuthentMiddleware._() {

  }

  Middleware<AppState> initAuthentListener() {
    return (Store<AppState> store, action, NextDispatcher next) async {

      clearSubs();
      addSub(AuthService.getInstance().getObsUserFB().listen((userFB) async{
        String target;
        store.dispatch(new ActionStatus());

        if (userFB != null) {
          target = "/Home";
          store.dispatch(new ActionLoginUser(uFB: userFB));
        }
        else {
          store.dispatch(new ActionLogoutUser());
          store.dispatch(new ActionRestorePersist());
          target = "/Login";
        }
        store.dispatch(new ActionNavigate(target,null,false));

      }));
      next(action);
    };
  }

  Middleware<AppState> login() {
    return (Store<AppState> store, action, NextDispatcher next) async {
      store.dispatch(new ActionStatus(flagWait: true));

      int delay=10;
      //if (kDebugMode) delay=4000;
      Future.delayed(
        Duration(milliseconds: delay),
            () {
              AuthService.getInstance()
                  .loginEmailPassword((action as ActionLogin).email,(action as ActionLogin).password)
                  .then((u) {

              }, onError: (e) {
                store.dispatch(new ActionStatus(titre :"Login",
                    message : _getErrorLogin(e.code),
                    typeMessage :Constantes.TYPE_MESSAGE_ERROR));
              });
              next(action);
        },
      );




    };


  }

  Middleware<AppState> createUser() {
    return (Store<AppState> store, action, NextDispatcher next) async {
      store.dispatch(new ActionStatus(flagWait: true));

      AuthService.getInstance().createUser((action as ActionCreateUser).user, (action as ActionCreateUser).password).then((u) {

        store.dispatch(new ActionMajUser(u,true));

        },
          onError: (e) {
            store.dispatch(new ActionStatus(titre :"Création user",
                message : _getErrorLogin(e.code),
                typeMessage :Constantes.TYPE_MESSAGE_ERROR));
            //next(action);
      });
    };
   }

  Middleware<AppState> logout() {
    return (Store<AppState> store, action, NextDispatcher next) async {
      AuthService.getInstance().logout();
      next(action);
    };
  }

  Middleware<AppState> resetPassword() {
    return (Store<AppState> store, action, NextDispatcher next) async {
      String mes="Un mail a été envoé à votre adresse (Attention, il se trouvera peut être dans le dossier des mails indésirables ..), veuillez suivre le lien pour saisir un nouveau mot de passe.";
      String tit="Mot de passe oublié";

      store.dispatch(new ActionStatus(flagWait: true));
      AuthService.getInstance().resetPassword(
          (action as ActionResetPassword).email).then((ret) {
        store.dispatch(new ActionStatus(message : mes,
            titre:tit,
            heightDlg: Constantes.DLG_BIG_HEIGHT,
            typeMessage :Constantes.TYPE_MESSAGE_INFO));
      }).catchError((onError) {
        store.dispatch(new ActionStatus(message :onError.message,titre:tit,typeMessage :Constantes.TYPE_MESSAGE_ERROR));
      });
      next(action);
    };
  }

  String _getErrorLogin(String code) {

    String msg = code;


    do {
      if (code.indexOf("invalid-email") >= 0) {
        msg = "Adresse mail invalide.";
        break;
      }
      if (code.indexOf("user-not-found") >= 0) {
        msg = "Adresse mail inconnue.";
        break;
      }
      if (code.indexOf("wrong-password") >= 0) {
        msg = "Mot de passe erroné.";
        break;
      }
      if (code.indexOf("too-many-request") >= 0) {
        msg =
        "Trops d'essais en erreur conséqutifs, veuillez ré-essayer dans 10 mns.";
        break;
      }
    } while (false);

    return(msg);


  }

}


