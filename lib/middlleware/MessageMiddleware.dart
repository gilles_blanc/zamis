import 'package:redux/redux.dart';
import 'package:zamis/action/ActionAddMessage.dart';
import 'package:zamis/action/ActionListeMessage.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/model/EntMessage.dart';
import 'package:zamis/service/MessageService.dart';
import 'package:zamis/state/AppState.dart';

import 'BaseMiddleware.dart';

class MessageMiddleware extends BaseMiddleware{
  static MessageMiddleware _singleton=MessageMiddleware._();

  factory MessageMiddleware() {
    return _singleton;
  }

  MessageMiddleware._() {

  }

  Middleware<AppState> init() {

    return (Store<AppState> store, action, NextDispatcher next) async {
      clearSubs();
      MessageService.getInstance().initListenerListeMessage();


      addSub(MessageService.getInstance().getObsMessage().listen((l) async {
        int tsLastMessage= await Util.getPersist(Constantes.PERSIST_LAST_ID_MESSAGE);
        if(tsLastMessage == 0) tsLastMessage = DateTime.now().millisecondsSinceEpoch;

        l.forEach((e) {
          if(e.ts <= tsLastMessage){
            e.flagNew=false;
          }
          else{
            e.flagNew=true;
          }
        });

        if(l.length > 0){
          tsLastMessage = l.last.date.millisecondsSinceEpoch;
        }

        Util.setPersist(Constantes.PERSIST_LAST_ID_MESSAGE, tsLastMessage);

        store.dispatch(new ActionListeMessage(listeMessage :l));


      }));
    };
  }

  Middleware<AppState> sendMessage() {
    return (Store<AppState> store, action, NextDispatcher next) async {

      String message = (action as ActionAddMessage).message;
      String userId = (action as ActionAddMessage).userId;

      if(message != "") {
        EntMessage newMessage = new EntMessage();
        newMessage.userId = userId;
        newMessage.date = DateTime.now();
        newMessage.message = message;
        newMessage.titre = "";

        MessageService.getInstance().createMessage(newMessage);

      }
    };
  }


}





