import 'EntUser.dart';

class EntAnniversaire {
  late EntUser user;
  late DateTime dateAnniversaire;

  EntAnniversaire(EntUser u, DateTime d) {
    user = u;
    dateAnniversaire = d;
  }
}