import 'package:intl/intl.dart';
import 'package:zamis/common/Constantes.dart';

import 'EntParam.dart';


abstract class EntActivite {
  late String id;
  late DateTime date;
  late int typeActivite;


  EntActivite(int type){
    typeActivite=type;
    id="";
    DateTime t = DateTime.now();
    date =DateTime(t.year, t.month, t.day, 9,30);


  }

  String getDateLong(){
    String ret="le ";

    ret+=DateFormat("MMMMEEEEd", "fr_FR").format(date);
    ret+=" à ";
    ret+=DateFormat("Hm", "fr_FR").format(date);

    return(ret);
  }

  String getDateShort(){
    String ret="le ";

    ret+=DateFormat("MMMMEEEEd", "fr_FR").format(date);

    return(ret);
  }

  String getHour(){
    String ret="à ";

    ret+=DateFormat("Hm", "fr_FR").format(date);

    return(ret);
  }

  String getTitre(){
    String ret="Activité";
    return(ret);
  }

  int _getTimeStatus(EntParam p){
    int ret=Constantes.TIME_STATUS_UNKNOWN;
    DateTime now = DateTime.now();
    DateTime dateStartInscription = date.subtract(new Duration(hours: p.ouverture_heure));
    DateTime dateEndInscription = date.subtract(new Duration(minutes: p.fermeture_minute));
    DateTime dateEndMatch = date.add(new Duration(minutes: 90));

    do {
      if (now.isBefore(dateStartInscription)){
        ret = Constantes.TIME_STATUS_BEFORE_INSCRIPTION;
        break;
      }
      if (now.isAfter(dateStartInscription) && now.isBefore(dateEndInscription)){
        ret = Constantes.TIME_STATUS_INSCRIPTION;
        break;
      }
      if (now.isAfter(dateEndInscription) && now.isBefore(date)){
        ret = Constantes.TIME_STATUS_TOO_LATE_INSCRIPTION;
        break;
      }
      if (now.isAfter(date) && now.isBefore(dateEndMatch)){
        ret = Constantes.TIME_STATUS_MATCH_BEGIN;
        break;
      }
      if (now.isAfter(dateEndMatch)){
        ret = Constantes.TIME_STATUS_MATCH_END;
        break;
      }

    }while(false);

    return(ret);

  }

  @override
  bool isOpen(EntParam p){

    bool ret=false;
    switch(_getTimeStatus(p)){
      case Constantes.TIME_STATUS_MATCH_END:
      case Constantes.TIME_STATUS_MATCH_BEGIN:
      case Constantes.TIME_STATUS_UNKNOWN:
      case Constantes.TIME_STATUS_BEFORE_INSCRIPTION:
      case Constantes.TIME_STATUS_TOO_LATE_INSCRIPTION:
        ret=false;
        break;
      case Constantes.TIME_STATUS_INSCRIPTION:
        ret=true;
        break;
    }
    return(ret);
  }

  @override
  String getInformationOpen(EntParam p){
    String ret="";
    DateTime now = DateTime.now();
    DateTime dateStartInscription = date.subtract(new Duration(hours: p.ouverture_heure));
    DateTime dateEndInscription = date.subtract(new Duration(minutes: p.fermeture_minute));
    DateTime dateEndMatch = date.add(new Duration(minutes: 90));

    switch(_getTimeStatus(p)){
      case Constantes.TIME_STATUS_MATCH_END:
        ret = "Activité terminée depuis " + _getStrDifference(dateEndMatch,now);
        break;
      case Constantes.TIME_STATUS_MATCH_BEGIN:
        ret = "Activité démarré depuis " + _getStrDifference(date,now);
        break;
      case Constantes.TIME_STATUS_UNKNOWN:
        ret="";
        break;
      case Constantes.TIME_STATUS_BEFORE_INSCRIPTION:
        ret = "Ouverture : ";
        ret = ret + _getStrDifference(now,dateStartInscription);
        break;
      case Constantes.TIME_STATUS_TOO_LATE_INSCRIPTION:
        ret = "Inscription fermée depuis " + _getStrDifference(dateEndInscription,now);
        break;
      case Constantes.TIME_STATUS_INSCRIPTION:
        ret="Fermeture : ";
        ret=ret + _getStrDifference(now,dateEndInscription);
        break;
    }
    return(ret);
  }

  String _getStrDifference(DateTime d1,DateTime d2){
    String ret="";
    int nbDay= d2.difference(d1).inDays;
    d1=d1.add(new Duration(days: nbDay));
    int nbHour= d2.difference(d1).inHours;
    d1=d1.add(new Duration(hours: nbHour));
    int nbMinute=d2.difference(d1).inMinutes;
    d1=d1.add(new Duration(minutes: nbMinute));
    int nbSecond=d2.difference(d1).inSeconds;

    ret=_addInfo(ret,"jr",nbDay);
    ret=_addInfo(ret,"hr",nbHour);
    ret=_addInfo(ret,"min",nbMinute);
    if(nbDay == 0) ret=_addInfo(ret,"s",nbSecond,false);

    return(ret);
  }

  String _addInfo(String root,String unit,int nb,[bool flagSuffixe=true]){
    String ret=root;
    if(nb > 0) {
      ret = ret + nb.toString() + unit;
      if ((nb > 1) && (flagSuffixe == true)) ret = ret + "s";
      ret = ret + " ";
    }
    return(ret);

  }
}
