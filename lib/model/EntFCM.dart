
import 'package:firebase_messaging/firebase_messaging.dart';

import '../common/Constantes.dart';

class EntFCM {

  late String titre;
  late String message;
  late String idUserEmit;
  late int typeNotif;

  EntFCM() {
    titre="";
    message = "";
    idUserEmit="";
    typeNotif = 0;
  }

  static EntFCM fromFCM(RemoteMessage event) {
    EntFCM m = EntFCM();

    if (event.notification!.title != null) m.titre = event.notification!.title!;
    if (event.notification!.body != null) m.message = event.notification!.body!;
    if (event.data.containsKey(Constantes.KEY_FCM_ID_USER)) m.idUserEmit = event.data[Constantes.KEY_FCM_ID_USER];
    if (event.data.containsKey(Constantes.KEY_FCM_TYPE)) m.typeNotif = int.parse(event.data[Constantes.KEY_FCM_TYPE]);


    return (m);
  }

  EntFCM clone() {
    EntFCM fcm = EntFCM();
    fcm.titre = titre;
    fcm.message = message;
    fcm.typeNotif = typeNotif;
    fcm.idUserEmit = idUserEmit;

    return (fcm);
  }

  }
