import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zamis/common/Util.dart';

class EntParam  {
  late int version_android;
  late bool open_appli;
  late bool force_maj_version;
  late int ouverture_heure;
  late int fermeture_minute;

  static final int DEF_VALUE_VERSION_ANDROID=1000;
  static final bool DEF_VALUE_OPEN_APPLI=true;
  static final bool DEF_VALUE_FORCE_MAJ_VERSION=false;
  static final int DEF_VALUE_OUVERTURE_HEURE=4;
  static final int DEF_VALUE_FERMETURE_MINUTE=30;

  EntDepense() {
    version_android = DEF_VALUE_VERSION_ANDROID;
    open_appli = DEF_VALUE_OPEN_APPLI;
    ouverture_heure=DEF_VALUE_OUVERTURE_HEURE;
    fermeture_minute=DEF_VALUE_FERMETURE_MINUTE;
    force_maj_version=DEF_VALUE_FORCE_MAJ_VERSION;
  }

  static EntParam fromFB(DocumentSnapshot o) {
    EntParam p = EntParam();
    p.version_android = Util.safeMap<int>(o, "version_android",DEF_VALUE_VERSION_ANDROID);
    p.ouverture_heure = Util.safeMap<int>(o, "ouverture_heure",DEF_VALUE_OUVERTURE_HEURE);
    p.fermeture_minute = Util.safeMap<int>(o, "fermeture_minute",DEF_VALUE_FERMETURE_MINUTE);
    p.open_appli = Util.safeMap<bool>(o, "open_appli",DEF_VALUE_OPEN_APPLI);
    p.force_maj_version = Util.safeMap<bool>(o, "force_maj_version",DEF_VALUE_FORCE_MAJ_VERSION);
    return (p);
  }

  Object toFB() {
    Object o = {
      "version_android": version_android,
      "ouverture_heure": ouverture_heure,
      "fermeture_minute": fermeture_minute,
      "open_appli": open_appli,
      "force_maj_version": force_maj_version
    };
    return (o);
  }
}
