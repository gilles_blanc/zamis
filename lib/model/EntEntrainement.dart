import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/Util.dart';
import 'EntActivite.dart';
import 'EntParam.dart';

class EntEntrainement extends EntActivite{

  EntEntrainement() : super(Constantes.TYPE_ACTIVITE_NEXT_TRAINING){

    date = new DateTime(date.year, date.month, date.day, Constantes.TRAINING_HOUR);

  }

  static EntEntrainement fromFB(QueryDocumentSnapshot o) {
    EntEntrainement m = EntEntrainement();
    m.id = o.id;
    m.date = Util.safeMap<DateTime>(o,"date");
    return (m);
  }

  Object toFB() {
    Object o = {
      "date": date,
    };
    return (o);
  }

  EntEntrainement clone(){
    EntEntrainement clone = EntEntrainement();
    clone.date=date;
    clone.id=id;
    clone.typeActivite=typeActivite;
    return(clone);
  }

  @override
  String getTitre(){
    String ret="";

    ret="Prochain match";
    return(ret);
  }


}
