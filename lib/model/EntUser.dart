
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/Util.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'EntUserNotation.dart';

class EntUser{
  late String id;
  late String _email;
  late bool flagNotifMessage;
  late bool flagNotifPresence;
  late String nom;
  late String prenom;
  late String _tel;
  late String urlAvatar;
  late DateTime dateNaissance;
  late String lastTsCommentaire;
  late String role;
  late String tokenFCMMob;
  late String tokenFCMWeb;
  late bool flagTelVisible;
  late bool flagMailVisible;
  late List<EntUserNotation> listeUserNotation;

  EntUser() {
    id="";
    _email="";
    nom="";
    prenom="";
    _tel="";
    urlAvatar="";
    dateNaissance=DateTime.now();
    lastTsCommentaire="";
    role="";
    tokenFCMMob="";
    tokenFCMWeb="";
    flagNotifMessage=true;
    flagNotifPresence=true;
    flagTelVisible=false;
    flagMailVisible=false;
    listeUserNotation=[];
   // flagEntrainement=false;
  }

  static EntUser fromFB(DocumentSnapshot o) {
    EntUser u = EntUser();

    u.id=o.id;
    u.nom=Util.safeMap<String>(o, "nom");
    u.nom=Util.safeMap<String>(o,"nom");
    u.prenom=Util.safeMap<String>(o,"prenom");
    u.role=Util.safeMap<String>(o,"role");
    u.dateNaissance=Util.safeMap<DateTime>(o,"dateNaissance");
    u.flagNotifMessage = Util.safeMap<bool>(o,"flagNotifMessage");
    u.flagNotifPresence = Util.safeMap<bool>(o,"flagNotifPresence");
    u._email = Util.safeMap<String>(o,"email");
    u._tel = Util.safeMap<String>(o,"tel");
    u.urlAvatar = Util.safeMap<String>(o,"urlAvatar");
    u.tokenFCMWeb = Util.safeMap<String>(o,"tokenFCMWeb");
    u.tokenFCMMob = Util.safeMap<String>(o,"tokenFCMMob");
    u.flagTelVisible = Util.safeMap<bool>(o,"flagTelVisible");
    u.flagMailVisible = Util.safeMap<bool>(o,"flagMailVisible");
    u.listeUserNotation = [];
    return (u);
  }

  void fillNotes(QuerySnapshot<Map<String,dynamic>> snap){
    listeUserNotation=[];
    snap.docs.forEach((o) {
      listeUserNotation.add(EntUserNotation.fromFB(o));
    });
  }

  EntUser clone() {
    EntUser u = EntUser();
    u.id = id;
    u.dateNaissance = dateNaissance;
    u.urlAvatar = urlAvatar;
    u._email = _email;
    u.flagNotifMessage = flagNotifMessage;
    u.flagNotifPresence = flagNotifPresence;
    u.lastTsCommentaire = lastTsCommentaire;
    u.nom = nom;
    u.prenom = prenom;
    u._tel = _tel;
    u.role = role;
    u.tokenFCMMob = tokenFCMMob;
    u.tokenFCMWeb = tokenFCMWeb;
    u.flagTelVisible=flagTelVisible;
    u.flagMailVisible=flagMailVisible;

    listeUserNotation.forEach((element) {
      u.listeUserNotation.add(element.clone());
    });



    return (u);
  }

  Object toFB() {
    Object o = {
      "urlAvatar": urlAvatar,
      "dateNaissance": dateNaissance,
      "email": _email,
      "flagNotifMessage": flagNotifMessage,
      "flagNotifPresence": flagNotifPresence,
      "flagTelVisible": flagTelVisible,
      "flagMailVisible": flagMailVisible,
      "lastTsCommentaire": lastTsCommentaire,
      "nom": nom,
      "prenom": prenom,
      "tel": _tel,
      "role": role,
      "tokenFCMMob": tokenFCMMob,
      "tokenFCMWeb": tokenFCMWeb,
    };
    return (o);
  }

  int getNoteFromUserId(String userId){
    int ret=Constantes.DEFAUT_USER_NOTE;
    EntUserNotation? un=listeUserNotation.firstWhereOrNull((element) => element.userId == userId);
    if(un != null){

      ret=un.note;
    }

    return(ret);
  }

  String getLibelle() {
    if((nom == "") && (prenom == "")) return"?";
    return (prenom + " " + nom);
  }

  String getPrenomLimite() {
    if((nom == "") && (prenom == "")) return"?";
    if ((prenom.length + nom.length) > 17) return ("");

    return (prenom);
  }

  String getInitiales() {
    if((nom == "") || (prenom == "")) return"?";

    return (prenom.substring(0, 1) + nom.substring(0, 1));
  }

  String getLibelleCourt() {
    if((nom == "") || (prenom == "")) return"?";
    return (prenom.substring(0, 1) + ". " + nom);
  }

  String getNomPrenomFormat() {
    if((nom == "") || (prenom == "")) return"?";

    String ret = "";
    List<String> tmp = this.prenom.split(" ");
    for (String s in tmp) {
      List<String> tmp2 = s.split(".");
      for (String s2 in tmp2) {
        ret = ret + s2.substring(0, 1).toUpperCase() + s2.substring(1) + " ";
      }
    }

    return (nom.toUpperCase() + " " + ret);
  }

  String getPrenomFormat() {
    if(prenom == "") return"?";

    String ret = "";
    List<String> tmp = this.prenom.split(" ");
    for (String s in tmp) {
      List<String> tmp2 = s.split(".");
      for (String s2 in tmp2) {
        ret = ret + s2.substring(0, 1).toUpperCase() + s2.substring(1) + " ";
      }
    }

    return (ret);
  }

  String getNomFormat() {
    if(nom == "") return"?";

    return (nom.toUpperCase());
  }

  String getEMail(){
    String ret="";
    if(flagMailVisible == true) ret=_email;
    return(ret);
  }

  String getTel(){
    String ret="";
    if(flagTelVisible == true) ret=_tel;
    return(ret);
  }

  String getEMailVisible(){
    return(_email);
  }

  String getTelVisible(){
    return(_tel);
  }

  void setTel(String tel){
    _tel=tel;
  }

  void setEmail(String email){
    _email=email;
  }

  String getTelFormat() {
    if(flagTelVisible == false) return("");
    if(_tel == "") return "";

    String ret = "";
    int i=0;
    while(i < _tel.length){
      if(i > 0){
        if((i % 2) == 0){
          ret=ret+" ";
        }
      }
      ret=ret+_tel.substring(i, i+1);
      i++;
    }


    return (ret);
  }

}
