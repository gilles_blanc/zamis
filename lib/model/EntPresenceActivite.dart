import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/Util.dart';

class EntPresenceActivite {
  late String id;
  late String idActivite;
  late String idUser;
  late int codePresence;
  late List<String> listIdPresent;
  late List<String> listIdAbsent;
  late List<String> listIdIndecis;

  EntPresenceActivite() {
    this.id="";
    this.idActivite="";
    this.idUser="";
    this.listIdAbsent=[];
    this.listIdIndecis=[];
    this.listIdPresent=[];

    codePresence=Constantes.CODE_PRESENCE_ACTIVITE_UNKNOWN;
  }

  static EntPresenceActivite fromFB(QueryDocumentSnapshot o) {
    EntPresenceActivite a = EntPresenceActivite();
    a.id = o.id;
    a.idUser = Util.safeMap<String>(o,"idUser");
    a.idActivite = Util.safeMap<String>(o,"idActivite");
    a.codePresence = Util.safeMap<int>(o,"codePresence");
    return (a);
  }

  Object toFB() {

    Object o = {
      "idUser": idUser,
      "idActivite": idActivite,
      "codePresence": codePresence

    };
    return (o);
  }
}
