
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/Util.dart';

class EntUserNotation{
  late String userId;
  late int note;

  EntUser() {
    userId="";
    note=Constantes.DEFAUT_USER_NOTE;

  }

  static EntUserNotation fromFB(DocumentSnapshot o) {
    EntUserNotation u = EntUserNotation();

    u.userId=o.id;
    u.note=Util.safeMap<int>(o,"note",Constantes.DEFAUT_USER_NOTE);

    return (u);
  }

  EntUserNotation clone() {
    EntUserNotation u = EntUserNotation();
    u.userId = userId;
    u.note = note;
    return (u);
  }

  Object toFB() {
    Object o = {
      "userId": userId,
      "note": note
    };
    return (o);
  }
}
