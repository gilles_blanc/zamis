import 'dart:convert';
import '../common/Constantes.dart';
import 'EntUser.dart';
import 'fcm/GoogleFCM.dart';

class EntNotification {
  late EntUser _from;
  late List<EntUser> _to;
  late int _typeNotif;
  late int _nbPresent;

  EntNotification(int typeNotif,EntUser from,List<EntUser> lu,[int nbPresent=0]) {
    _typeNotif = typeNotif;
    _from=from;
    _to=lu;
    _nbPresent=nbPresent;
  }

  String build(){
    GoogleFCM fcm =GoogleFCM();

    switch(_typeNotif) {
      case Constantes.TYPE_NOTIFICATION_PARTICIPATION_ENTRAINEMENT:
        fcm.notification.body= Constantes.TOKEN_BODY_PARTICIPATION_ENTRAINEMENT;
        fcm.notification.title=Constantes.TITLE_ENTRAINEMENT;
        break;
      case Constantes.TYPE_NOTIFICATION_RETRAIT_PARTICIPATION_ENTRAINEMENT:
        fcm.notification.body= Constantes.TOKEN_BODY_RETRAIT_PARTICIPATION_ENTRAINEMENT;
        fcm.notification.title=Constantes.TITLE_ENTRAINEMENT;
        break;
      case Constantes.TYPE_NOTIFICATION_MESSAGE:
        fcm.notification.body = Constantes.TOKEN_BODY_MESSAGE;
        fcm.notification.title=Constantes.TITLE_MESSAGE;
        break;
      default:
        return("");
    }
    fcm.data.idUser=_from.id;
    fcm.notification.body=fcm.notification.body.replaceFirst(Constantes.TOKEN_LIBELLE_FROM, _from.getLibelle());
    fcm.notification.title=fcm.notification.title.replaceFirst(Constantes.TOKEN_NB_PRESENT, _getNbPresent());

    fcm.notification.image=_getUrlAvatarFrom();
    fcm.registration_ids=_getListeTokenTo();


    return(jsonEncode(fcm.toJson()));
  }

  String _getUrlAvatarFrom(){
    String ret="";
    if(_from.urlAvatar != null){
      ret=_from.urlAvatar;
    }
    return(ret);
  }

  String _getNbPresent(){
    String ret="";
    if(_nbPresent > 0){
      ret="(Nb présent=" + _nbPresent.toString() + ")";
    }
    return(ret);
  }

  List<String> _getListeTokenTo(){
    List<String> lRet=[];
    bool flagNotif;

    _to.forEach((user) {
      flagNotif=false;
      switch(_typeNotif){
        case Constantes.TYPE_NOTIFICATION_MESSAGE:
          flagNotif=user.flagNotifMessage;
          break;
        case Constantes.TYPE_NOTIFICATION_PARTICIPATION_ENTRAINEMENT:
        case Constantes.TYPE_NOTIFICATION_RETRAIT_PARTICIPATION_ENTRAINEMENT:
          flagNotif=user.flagNotifPresence;
          break;
      }

      if (user.id == _from.id) flagNotif = false;

      if(flagNotif == true){
        if(user.tokenFCMMob != "") lRet.add(user.tokenFCMMob);
        if(user.tokenFCMWeb != "") lRet.add(user.tokenFCMWeb);
      }

    });
    return(lRet);
  }
}
