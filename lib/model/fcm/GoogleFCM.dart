import 'GoogleFCMData.dart';
import 'GoogleFCMNotification.dart';

class GoogleFCM{

 static const String PRIORITY_HIGHT="high";


 late GoogleFCMNotification notification;
 late List<String> registration_ids;
 late String priority;
 late GoogleFCMData data;

 GoogleFCM(){
   notification = GoogleFCMNotification();
   registration_ids=[];
   priority=PRIORITY_HIGHT;
   data = GoogleFCMData();
 }
 Map<String, dynamic> toJson() => _itemToJson(this);

 Map<String, dynamic> _itemToJson(GoogleFCM instance) {
   return <String, dynamic>{
     'notification': instance.notification.toJson(),
     'registration_ids': instance.registration_ids,
     'priority': instance.priority,
     'data': instance.data.toJson()
   };
 }
}
