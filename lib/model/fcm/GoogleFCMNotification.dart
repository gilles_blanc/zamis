class GoogleFCMNotification{
 static const String DEFAULT_NOTIFICATION_SOUND="sound";
 static const String DEFAULT_NOTIFICATION_IMAGE="";
 static const String DEFAULT_NOTIFICATION_BODY="Notification ZAMIS";
 static const String DEFAULT_NOTIFICATION_TITLE="Zamis FC";

 late String body;
 late String title;
 late String sound;
 late String image;

 GoogleFCMNotification(){
   sound = DEFAULT_NOTIFICATION_SOUND;
   image=DEFAULT_NOTIFICATION_IMAGE;
   title=DEFAULT_NOTIFICATION_TITLE;
   body=DEFAULT_NOTIFICATION_BODY;
 }

 Map<String, dynamic> toJson() => _itemToJson(this);

 Map<String, dynamic> _itemToJson(GoogleFCMNotification instance) {
   return <String, dynamic>{
     'body': instance.body,
     'title': instance.title,
     'image': instance.image,
     'sound': instance.sound
   };
 }
}
