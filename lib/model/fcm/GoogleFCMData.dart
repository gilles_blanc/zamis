import '../../common/Constantes.dart';

class GoogleFCMData{

 late String idUser;
 late int type;


 GoogleFCMData(){
  idUser="";
  type=0;
 }

 Map<String, dynamic> toJson() => _itemToJson(this);

 Map<String, dynamic> _itemToJson(GoogleFCMData instance) {
  return <String, dynamic>{
   Constantes.KEY_FCM_ID_USER : instance.idUser,
   Constantes.KEY_FCM_TYPE: instance.type
  };
 }
}
