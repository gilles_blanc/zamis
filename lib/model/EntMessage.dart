import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zamis/common/Util.dart';

import 'EntUploadObject.dart';

class EntMessage {

  late String id;
  late String userId;
  late int ts;
  late String titre;
  late String message;
  late DateTime date;
  late List<EntUploadObject> tabPJ;
  late bool flagNew;

  EntMessage() {
    tabPJ = [];
  }

  static EntMessage fromFB(QueryDocumentSnapshot o) {
    EntMessage m = EntMessage();

    m.userId = Util.safeMap<String>(o,"userId");
    m.titre = "";//o.get("titre");
    m.message = Util.safeMap<String>(o,"message");;
    _fillPJ(m, o);
    m.date=Util.safeMap<DateTime>(o,"date");
    m.ts=m.date.millisecondsSinceEpoch;
    m.flagNew = false;
    return (m);
  }

  static void _fillPJ(EntMessage m, QueryDocumentSnapshot o) async {
    /*if (lpj == null) return;

    EntUploadObject elem;
    lpj.forEach((pjFB) {
      elem = new EntUploadObject();
      elem.urlBig = pjFB["urlBig"] ?? "";
      elem.urlSmall = pjFB["urlSmall"] ?? "";
      elem.typeFile = pjFB["typeFile"] ?? "";
      elem.typeMime = pjFB["typeMime"] ?? "";
      elem.name=pjFB["name"] ?? "";
      m.tabPJ.add(elem);
    });*/
  }

  Object toFB() {
    List<Map<String, dynamic>> tab = [];

    tabPJ.forEach((element) {
      tab.add({
        "urlBig": element.urlBig,
        "urlSmall": element.urlSmall,
        "typeMime": element.typeMime,
        "typeFile": element.typeFile,
        "name": element.name
      });
    });

    Object o = {
      "message": message,
      "titre": titre,
      "userId": userId,
      "date": date,
      "tabPJ": tab
    };
    return (o);
  }
}
