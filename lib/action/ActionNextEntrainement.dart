
import 'package:firebase_auth/firebase_auth.dart';
import 'package:zamis/model/EntAnniversaire.dart';
import 'package:zamis/model/EntEntrainement.dart';
import 'package:zamis/model/EntUser.dart';

class ActionNextEntrainement {

  EntEntrainement? nextEntrainement;

  ActionNextEntrainement({this.nextEntrainement});

}