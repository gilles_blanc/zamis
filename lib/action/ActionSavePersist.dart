// lib/actions/actions.dart
class ActionSavePersist {
  String? email;
  String? password;
  bool? flagPersistLogin;

  ActionSavePersist({this.flagPersistLogin,this.email,this.password});
}