// lib/actions/actions.dart
class ActionRestorePersist {
  String? email;
  String? password;
  bool? flagPersistLogin;

  ActionRestorePersist({this.flagPersistLogin,this.email,this.password});
}