import 'dart:typed_data';

import 'package:zamis/model/EntUser.dart';

class ActionMajAvatar {
  Uint8List buffer;
  EntUser user;

  ActionMajAvatar(this.user,this.buffer);
}