// lib/actions/actions.dart
import 'package:zamis/common/Constantes.dart';

class ActionStatus {
  String  message;
  String titre;
  int typeMessage;
  bool flagWait;
  double heightDlg;

  ActionStatus({this.flagWait=false,this.message="",this.typeMessage=0,this.titre="",this.heightDlg=Constantes.DLG_HEIGHT});
}