// lib/actions/actions.dart
import 'package:zamis/common/Constantes.dart';

class ActionVersion {
  String  version;
  String  nrBuild;

   ActionVersion({required this.version,required this.nrBuild});
}