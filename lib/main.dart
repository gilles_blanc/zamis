import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/reducer/AppReducer.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/widget/Activite/DetailActivite.dart';
import 'package:zamis/widget/Admin/AppClose.dart';
import 'package:zamis/widget/Admin/NewVersion.dart';
import 'package:zamis/widget/Composition/Composition.dart';
import 'package:zamis/widget/Team/JoueurEdit.dart';
import 'package:zamis/widget/Avatar/AvatarEdit.dart';
import 'package:zamis/widget/Home.dart';
import 'package:zamis/widget/Login.dart';
import 'package:zamis/widget/Message/Message.dart';
import 'package:zamis/widget/Mire.dart';
import 'package:zamis/widget/Team/ElemJoueur.dart';
import 'package:zamis/widget/Team/Team.dart';

import 'common/Constantes.dart';
import 'common/Util.dart';
import 'middlleware/AppMiddleware.dart';

void main() {
  final _store = Store<AppState>(
    appReducer,
    initialState: new AppState.initial(),
    middleware:getMiddlewares()
  );

  runApp(MyApp(store: _store));

}

class MyApp extends StatelessWidget {
  final ThemeData theme = ThemeData();
  final Store<AppState>? store;



  MyApp({this.store});

  @override
  Widget build(BuildContext context) {

    // RGB LOGO BE9B6B (190,155,107)

    return StoreProvider<AppState>(
      store: store!,
      child:
        MaterialApp(
          navigatorKey: MyKey.navKey,
          title: 'ZAMIS foot',
          theme: theme.copyWith(
            colorScheme: theme.colorScheme
                .copyWith(primary: Color.fromRGBO(190,155, 107,1), secondary: Colors.black),
          ),
          initialRoute: '/',
          routes: {
            '/': (context) => Mire(),
            '/Login': (context) => Login(),
            '/Home': (context) => Home(),
            '/Message': (context) => Message(),
            '/Team': (context) => Team(),
            '/JoueurEdit': (context) => JoueurEdit(),
            '/AvatarEdit': (context) => AvatarEdit(),
            '/Composition': (context) => Composition(),
            '/DetailActivite': (context) => DetailActivite(),
            '/NewVersion': (context) => NewVersion(),
            '/AppClose': (context) => AppClose(),

          },
        )
     ,
    );
  }
}
