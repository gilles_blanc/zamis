import 'package:meta/meta.dart';
import 'package:zamis/model/EntParam.dart';
import 'package:zamis/state/EntrainementState.dart';
import 'package:zamis/state/FCMState.dart';
import 'package:zamis/state/PresenceActiviteState.dart';

import 'MessageState.dart';
import 'TeamState.dart';
import 'UserState.dart';
import 'PersistState.dart';
import 'StatusState.dart';

@immutable
class AppState {
  late FCMState fcmState;
  late PresenceActiviteState presenceActiviteState;
  late EntrainementState entrainementState;
  late MessageState messageState;
  late UserState currentUserState;
  late StatusState statusState;
  late PersistState persistState;
  late TeamState teamState;
  late String version;
  late String nrBuild;
  late EntParam param;

  AppState({
    required this.presenceActiviteState,
    required this.entrainementState,
    required this.messageState,
    required this.currentUserState,
    required this.statusState,
    required this.persistState,
    required this.teamState,
    required this.version,
    required this.param,
    required this.nrBuild,
    required this.fcmState});

  factory AppState.initial() {
    return new AppState(
        presenceActiviteState: PresenceActiviteState.initial(),
        entrainementState: EntrainementState.initial(),
        messageState:MessageState.initial(),
        currentUserState: UserState.initial(),
        statusState: StatusState.initial(),
        persistState: PersistState.initial(),
        teamState: TeamState.initial(),
        version:"",
        param : EntParam(),
        nrBuild: "",
        fcmState: FCMState.initial());
  }

  AppState copyWith({
    PresenceActiviteState? presenceActiviteState,
    EntrainementState? entrainementState,
    MessageState? messageState,
    UserState? currentUserState,
    StatusState? statusState,
    PersistState? persistState,
    TeamState? teamState,
    String? version,
    EntParam? param,
    String? nrBuild,
    FCMState? fcmState
  }) {

    return AppState(
        presenceActiviteState : presenceActiviteState ?? this.presenceActiviteState,
        entrainementState: entrainementState ?? this.entrainementState,
        messageState: messageState ?? this.messageState,
        currentUserState : currentUserState ?? this.currentUserState,
        statusState : statusState ?? this.statusState,
        persistState: persistState ?? this.persistState,
        teamState: teamState ?? this.teamState,
        version : version ?? this.version,
      param : param ?? this.param,
        fcmState : fcmState ?? this.fcmState,
      nrBuild: nrBuild ?? this.nrBuild
    );

  }

}