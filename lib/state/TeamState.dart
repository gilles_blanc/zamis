import 'package:meta/meta.dart';
import 'package:zamis/model/EntAnniversaire.dart';
import 'package:zamis/model/EntUser.dart';

@immutable
class TeamState {
  List<EntUser> listUser;
  List<EntAnniversaire> listAnniversaire;

  TeamState({
    required this.listUser,required this.listAnniversaire}){
  }

  factory TeamState.initial() {
    return new TeamState(listUser: [],listAnniversaire: []);
  }

  TeamState copyWith({List<EntUser>? listUser,List<EntAnniversaire>? listAnniversaire}) {
    return new TeamState(
        listUser: listUser ?? this.listUser,
        listAnniversaire: listAnniversaire ?? this.listAnniversaire);
  }



}