import 'package:meta/meta.dart';
import 'package:zamis/model/EntUser.dart';

@immutable
class PersistState {
  bool flagPersistLogin;
  String emailPersist;
  String passwordPersist;


  PersistState({
    this.passwordPersist="",
    this.emailPersist="",
    this.flagPersistLogin=false
    }){

  }

  factory PersistState.initial() {
    return new PersistState();
  }

  PersistState copyWith({bool? flagPersistLogin,String? emailPersist,String? passwordPersist}) {
    return new PersistState(
        flagPersistLogin: flagPersistLogin ?? this.flagPersistLogin,
        passwordPersist: passwordPersist ?? this.passwordPersist,
        emailPersist: emailPersist ?? this.emailPersist
      );
  }



}