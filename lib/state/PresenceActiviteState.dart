import 'package:meta/meta.dart';
import 'package:zamis/model/EntPresenceActivite.dart';

@immutable
class PresenceActiviteState {
  Map<String,EntPresenceActivite> dicPresenceActivite;

  PresenceActiviteState({
    required this.dicPresenceActivite}){
  }

  factory PresenceActiviteState.initial() {
    return new PresenceActiviteState(dicPresenceActivite: Map());
  }

  PresenceActiviteState copyWith({Map<String,EntPresenceActivite>? dicPresenceActivite}) {
    return new PresenceActiviteState(
        dicPresenceActivite: dicPresenceActivite ?? this.dicPresenceActivite);
  }
}