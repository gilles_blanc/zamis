import 'package:meta/meta.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/model/EntFCM.dart';

import 'UserState.dart';

@immutable
class FCMState {
  EntFCM? fcm;

  FCMState({this.fcm});

  factory FCMState.initial() {
    return new FCMState();
  }

  FCMState copyWith({
      EntFCM? fcm}) {

    return FCMState(
        fcm: fcm);

  }

}