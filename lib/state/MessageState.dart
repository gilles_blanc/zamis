import 'package:meta/meta.dart';
import 'package:zamis/model/EntMessage.dart';

@immutable
class MessageState {
  List<EntMessage> listeMessage;

  MessageState({
    required this.listeMessage}){
  }

  factory MessageState.initial() {
    return new MessageState(listeMessage: []);
  }

  MessageState copyWith({List<EntMessage>? listeMessage}) {
    return new MessageState(
        listeMessage: listeMessage ?? this.listeMessage);
  }



}