import 'package:meta/meta.dart';
import 'package:zamis/common/Constantes.dart';

import 'UserState.dart';

@immutable
class StatusState {
  late String message;
  late String titre;
  late int typeMessage;
  late bool flagWait;
  late double heightDlg;

  StatusState({this.flagWait=false,this.message="",this.titre="",this.typeMessage=0,this.heightDlg=Constantes.DLG_HEIGHT});

  factory StatusState.initial() {
    return new StatusState();
  }

  StatusState copyWith({
      int? typeMessage,
    String? titre,
    String? message,
  bool? flagWait,
  double? heightDlg}) {

    return StatusState(
        flagWait: flagWait ?? this.flagWait,
        titre : titre ?? this.titre,
        message : message ?? this.message,
        typeMessage: typeMessage ?? this.typeMessage,
      heightDlg: heightDlg ?? this.heightDlg
    );

  }

}