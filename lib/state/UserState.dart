import 'package:meta/meta.dart';
import 'package:zamis/model/EntUser.dart';

@immutable
class UserState {
  EntUser? user;

  UserState({
    this.user}){
  }

  factory UserState.initial() {
    return new UserState(user: null);
  }

  UserState copyWith({EntUser? user}) {
    return new UserState(
        user: user ?? this.user);
  }



}