import 'package:meta/meta.dart';
import 'package:zamis/model/EntEntrainement.dart';

@immutable
class EntrainementState {
  EntEntrainement? nextEntrainement;

  EntrainementState({
    required this.nextEntrainement}){
  }

  factory EntrainementState.initial() {
    return new EntrainementState(nextEntrainement: null);
  }

  EntrainementState copyWith({EntEntrainement? nextEntrainement}) {
    return new EntrainementState(
        nextEntrainement: nextEntrainement ?? this.nextEntrainement);
  }



}