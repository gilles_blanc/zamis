import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/model/EntMessage.dart';

class MessageService {
  late BehaviorSubject<List<EntMessage>> _messageSubject;
  StreamSubscription<QuerySnapshot<Object?>>? _subListMessage=null;
  late FirebaseFirestore _fbInstanceFirestore;

  static MessageService? _singleton;

  static void razSingleton() {
    _singleton = null;
  }

  static MessageService getInstance() {
    if (_singleton == null) {
      _singleton = MessageService._();
    }
    return (_singleton!);
  }

  MessageService._() {
    _fbInstanceFirestore = FirebaseFirestore.instanceFor(app:MyKey.appFB!);
    _messageSubject = BehaviorSubject<List<EntMessage>>();

  }

  void cancelListenerListeMessage() {
    if(_subListMessage != null){
      _subListMessage!.cancel();
      _subListMessage=null;
    }
  }

  void initListenerListeMessage() {

    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_MESSAGES);

    EntMessage m;
    List<EntMessage> listeMessage =[];

    cancelListenerListeMessage();
    _subListMessage =refCollection.snapshots().listen((l) {
      listeMessage = [];
      l.docs.forEach((QueryDocumentSnapshot<Object?> o) {
        if (o.exists) {
          m = EntMessage.fromFB(o);
          listeMessage.add(m);
        }
      });

      listeMessage.sort((m1,m2){
        int ret=-1;
        if(m1.date.millisecondsSinceEpoch > m2.date.millisecondsSinceEpoch) ret=1;
        return(ret);
      });
      _messageSubject.add(listeMessage);


    });
  }

  /*void initNewMessage(){
    if(_subListMessage != null) _subListMessage!.cancel();
    _subListMessage=getObsMessage().listen((l) {
      int nb=0;
      Util.getPersist(Constantes.PERSIST_LAST_ID_MESSAGE).then((val) {
        int tsLastMessage = val ?? DateTime.now().millisecondsSinceEpoch;
        l.forEach((e) {
          if(e.ts > tsLastMessage){
            nb++;
          }
        });

        _newMessageSubject.add(nb);
      });
    });
  }*/

  /*Stream<int> getObsNewMessage() {
    return (_newMessageSubject.stream);
  }*/

  Stream<List<EntMessage>> getObsMessage() {
    return (_messageSubject.stream);
  }


  Future<dynamic> createMessage(EntMessage message) async {

    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_MESSAGES);

    return (refCollection.add(message.toFB()));


  }


}
