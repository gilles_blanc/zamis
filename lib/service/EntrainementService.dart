import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/model/EntEntrainement.dart';

class EntrainementService {
  static int SEANCE_LUNDI=1;
  static int SEANCE_MERCREDI=3;

  late FirebaseFirestore _fbInstanceFirestore;

  late BehaviorSubject<List<EntEntrainement>> _trainingSubject;
  late BehaviorSubject<List<EntEntrainement>> _interruptionSubject;

  StreamSubscription<QuerySnapshot<Object?>>? _subListTraining=null;
  StreamSubscription<QuerySnapshot<Object?>>? _subListInterrupTraining=null;

  static EntrainementService? _singleton;

  static void razSingleton() {
    _singleton = null;
    
  }

  static EntrainementService getInstance() {
    if (_singleton == null) {
      _singleton = EntrainementService._();
    }
    return (_singleton!);
  }

  EntrainementService._() {
    _interruptionSubject = BehaviorSubject<List<EntEntrainement>>();
    _trainingSubject = BehaviorSubject<List<EntEntrainement>>();
    _fbInstanceFirestore = FirebaseFirestore.instanceFor(app:MyKey.appFB!);

  }

  void cancelListenerTraining() {
    if(_subListTraining != null){
      _subListTraining!.cancel();
      _subListTraining=null;
    }
  }

  void cancelListenerInterrupTraining() {
    if(_subListInterrupTraining != null){
      _subListInterrupTraining!.cancel();
      _subListInterrupTraining=null;
    }
  }


  void initListenerTraining(){
    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_ENTRAINEMENTS);



    cancelListenerTraining();

    _subListTraining=refCollection.snapshots().listen((l) async {

      List<EntEntrainement> le = [];
      EntEntrainement e;

      l.docs.forEach((QueryDocumentSnapshot<Object?> o) async {
        if (o.exists) {
          e = EntEntrainement.fromFB(o);
          if(e.date.isAfter(DateTime.now())) le.add(e);
        }
      });

      le.sort((e1,e2){
        int ret=e1.date.compareTo(e2.date);
        if(ret == 0) ret=e1.id.compareTo(e2.id);
        return(ret);
      });

      _trainingSubject.add(le);

    });
  }

  void initListenerInterruption() async {

    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_INTERRUPTIONS);

    cancelListenerInterrupTraining();

    _subListInterrupTraining= refCollection.snapshots().listen((l) async {

      List<EntEntrainement> le = [];
      EntEntrainement e;

      l.docs.forEach((QueryDocumentSnapshot<Object?> o) async {
        if (o.exists) {
          e = EntEntrainement.fromFB(o);
          le.add(e);
        }
      });

      _interruptionSubject.add(le);
    });
  }

  Stream<List<EntEntrainement>> getObsNextEntrainement() {
    return (_trainingSubject.stream);
  }

  Stream<List<EntEntrainement>> getObsInterruption() {
    return (_interruptionSubject.stream);
  }

  Future<void> cleanOldEntrainement() async{

    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_ENTRAINEMENTS);

    DateTime dateLimite=DateTime.now();
    dateLimite=dateLimite.subtract(new Duration(hours: 4));
    int nbOld;
    refCollection.where("date", isLessThan : dateLimite).get().then((value) async{
      nbOld=value.docs.length;
      int i=0;
      await Future.forEach<QueryDocumentSnapshot<Object?>>(value.docs, (element) async {
      i++;
        print ("delete doc nb="+i.toString()+"/"+ nbOld.toString());
       await FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_ENTRAINEMENTS).doc(element.id).delete();
      });
    });
  }

  // ***********************************
  Future<void> majTraining(EntEntrainement e){
    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_ENTRAINEMENTS);

    Future<void> ret;
    if(e.id == "") ret=refCollection.add(e.toFB());
    else ret=refCollection.doc(e.id).set(e.toFB());

    return(ret);

  }

  Future<void> majInterruption(EntEntrainement e){
    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_INTERRUPTIONS);

    Future<void> ret;
    if(e.id == "") ret=refCollection.add(e.toFB());
    else ret=refCollection.doc(e.id).set(e.toFB());

    return(ret);

  }

  Future<void> deleteInterruption(EntEntrainement e){
    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_INTERRUPTIONS);

    Future<void> ret;
    ret=refCollection.doc(e.id).delete();

    return(ret);

  }

  DateTime findNextDateTraining(List<EntEntrainement> li) {
    DateTime now = new DateTime.now();
    now = new DateTime(now.year, now.month, now.day, Constantes.TRAINING_HOUR);

    while((!_isTrainingDay(now)) || (_isInInterruption(li,now) == true))
    {
      now=now.add(new Duration(days: 1));
    }

    return(now);
  }

  bool _isTrainingDay(DateTime d){
    bool ret=false;
    if((d.weekday == SEANCE_LUNDI) || (d.weekday == SEANCE_MERCREDI)) ret=true;
    return(ret);
  }

  bool _isInInterruption(List<EntEntrainement> li,DateTime d){
    bool ret=false;

    if(li != null) {
      EntEntrainement? ei = li.firstWhereOrNull((e) {
        bool ret = false;
        if (e.date.compareTo(d) == 0) ret = true;
        return (ret);
      });
      if (ei != null) ret = true;
    }
    return(ret);
  }
}
