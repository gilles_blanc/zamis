import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:rxdart/rxdart.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/common/Util.dart';
import 'package:zamis/model/EntUser.dart';

class AuthService {

  // ignore: close_sinks
  //late Stream<User?> _streamUserFB;
  late FirebaseAuth _fbInstanceAuth;
  static AuthService? _singleton;
  late BehaviorSubject<User?> _userSubject;
  //late User? _currentFBUSer;

  static void razSingleton() {
    _singleton = null;
  }

  static AuthService getInstance() {
    if (_singleton == null) {
      _singleton = AuthService._();
    }
    return (_singleton!);
  }

  AuthService._() {

    _fbInstanceAuth = FirebaseAuth.instanceFor(app:MyKey.appFB!);
    _initListener();
  }

  void _initListener(){
    _userSubject = new BehaviorSubject<User?>();

    _fbInstanceAuth.authStateChanges().listen((user){
      //_currentFBUSer=user;
      _userSubject.add(user);
    });
  }

  Stream<User?> getObsUserFB() {
    return (_userSubject.stream);
  }

  /*User? getCurrentUserFB() {
    return (_currentFBUSer);
  }*/

  Future<String> loginEmailPassword(String email, String password) async {

    Completer<String> completer = new Completer<String>();

     _fbInstanceAuth.signInWithEmailAndPassword(
          email: email, password: password).then((uc) {
         completer.complete(uc.user!.uid);
    }).catchError((error){
         completer.completeError(error);
    });

    return(completer.future);
  }

  Future<void> logout() async {
    var result;

    Future<dynamic> ret=_fbInstanceAuth.signOut();

    return result;
  }

  Future<dynamic> resetPassword(String email) async {
    Future<dynamic> ret;

    ret=_fbInstanceAuth.sendPasswordResetEmail(email: email);

    return ret;
  }

  Future<EntUser> createUser(EntUser userCreate, String password) async {
    FirebaseApp? tmpApp=null;

    try {
      tmpApp = await Firebase.initializeApp(
          name: "tmp", options: Util.getFirebaseOptions());
      var result = await FirebaseAuth.instanceFor(app: tmpApp)
          .createUserWithEmailAndPassword(
          email: userCreate.getEMail(), password: password);

      userCreate.id = result.user!.uid;
    }
    catch(e) {
      userCreate.id="";
      throw e;
    }
    finally {
      if(tmpApp != null) {
        tmpApp.delete();
      }
    }
    return userCreate;

  }

  /*void sendMailConfirmation() async{

    FirebaseUser fbu = await _auth.currentUser();

    await fbu.sendEmailVerification();

  }*/
 /* Future<void> modifEmail(String newEmail) async {
    Future<void> ret;
    if (kIsWeb) {
      ret = _web.modifEmail(newEmail);
    } else {
      ret = _mob.modifEmail(newEmail);
    }
    return (ret);
  }

  Future<void> modifPassword(String newPassword) async {
    Future<void> ret;
    if (kIsWeb) {
      ret = _web.modifPassword(newPassword);
    } else {
      ret = _mob.modifPassword(newPassword);
    }
    return (ret);
  }
*/
/*void _initFromAuthResult(AuthResult result) {
    _currentUser.idp = result.user.providerId;
    _currentUser.email = result.user.email;
    _currentUser.flagVerif = result.user.isEmailVerified;
  }*/

/*Future<EntUser> _getUserById(String id) async {
    DataSnapshot snapshot = await _db.child("users").child(id).once();

    EntUser u;

    u = EntUser.fromFB(snapshot.value, id);

    return (u);
  }*/

/************

 */

//}
}
