import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/model/EntParam.dart';

class ParamService {

  late BehaviorSubject<EntParam> _paramSubject;
  //StreamSubscription<DocumentSnapshot<Object?>>? _subParam=null;
  static ParamService? _singleton;
  late FirebaseFirestore _fbInstanceFirestore;

  static void razSingleton() {
    _singleton = null;
    
  }

  static ParamService getInstance() {
    if (_singleton == null) {
      _singleton = ParamService._();
    }
    return (_singleton!);
  }

  ParamService._() {
    _fbInstanceFirestore = FirebaseFirestore.instanceFor(app:MyKey.appFB!);
    _paramSubject = BehaviorSubject<EntParam>();
    initListenerParam();
  }

  /*void cancelListenerParam() {
    if(_subParam != null){
      _subParam!.cancel();
      _subParam=null;
    }
  }*/

  void initListenerParam() {

    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_PARAMS);

    refCollection.doc(Constantes.DOC_PARAM_ADMIN).snapshots().listen((ds) {

      EntParam p = EntParam.fromFB(ds);
      _paramSubject.add(p);
    });
  }

  Stream<EntParam> getObsParam() {
    return (_paramSubject.stream);
  }



}
