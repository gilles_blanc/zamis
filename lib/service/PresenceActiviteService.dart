import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:rxdart/rxdart.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/model/EntPresenceActivite.dart';
import 'UserService.dart';

class PresenceActiviteService {

  List<StreamSubscription<QuerySnapshot<Object?>>> _listSub=[];
  late FirebaseFirestore _fbInstanceFirestore;

  Map<String,BehaviorSubject<List<EntPresenceActivite>>> _dicSubject = Map();
  static PresenceActiviteService? _singleton;

  static void razSingleton() {
    _singleton = null;
    
  }

  static PresenceActiviteService getInstance() {
    if (_singleton == null) {
      _singleton = PresenceActiviteService._();
    }
    return (_singleton!);
  }

  PresenceActiviteService._() {
    _fbInstanceFirestore = FirebaseFirestore.instanceFor(app:MyKey.appFB!);

  }

  void cancelListener() {

    _listSub.forEach((element) {
        element.cancel();
    });
    _listSub=[];
    _dicSubject.clear();
  }

  Stream<List<EntPresenceActivite>> getObsListPresenceActivites(String idActivite) {

    if(!_dicSubject.containsKey(idActivite)){
      BehaviorSubject<List<EntPresenceActivite>> sub = BehaviorSubject<List<EntPresenceActivite>>();
      _dicSubject[idActivite]=sub;
      _initListenerPresenceActivite(sub,idActivite);
    }

    return (_dicSubject[idActivite]!.stream);
  }
  
  void _initListenerPresenceActivite(BehaviorSubject<List<EntPresenceActivite>> sub,String idActivite) {
    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_PRESENCE_ACTIVITES);

    _listSub.add(refCollection
      .where("idActivite", isEqualTo: idActivite)
      .snapshots().listen((l) async {
        List<EntPresenceActivite> lp = [];
        EntPresenceActivite p;

        l.docs.forEach((QueryDocumentSnapshot<Object?> o) async {
          if (o.exists) {
            p = EntPresenceActivite.fromFB(o);
            lp.add(p);
          }
        });

        _consolidePresenceUser(lp,idActivite).then((lpRet){
          sub.add(lpRet);
        });


    }));
  }

  Future<List<EntPresenceActivite>> _consolidePresenceUser(List<EntPresenceActivite> lp,String idActivite) async{

    Completer<List<EntPresenceActivite>> completer = new Completer<List<EntPresenceActivite>>();
    bool flagAdd;
    UserService.getInstance().getObsUsers().first.then((lu) {
      lu.forEach((user) {
           EntPresenceActivite? p = lp.firstWhereOrNull((ep) => ep.idUser == user.id);
          if(p == null){
            p=EntPresenceActivite();
            p.idActivite=idActivite;
            p.idUser=user.id;
            p.id="";
            p.codePresence=Constantes.CODE_PRESENCE_ACTIVITE_UNKNOWN;
            lp.add(p);
          }

      });

      completer.complete(lp);
    });
    return (completer.future);
  }

  Future<void> cleanOldPresenceActivite() async{

    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_ENTRAINEMENTS);


    QuerySnapshot le=await refCollection.get();
    List<String> listeId = [];
    le.docs.forEach((QueryDocumentSnapshot<Object?> o) async {
      if (o.exists) {
        listeId.add(o.id);
      }
    });

    CollectionReference refCollectionActivite =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_PRESENCE_ACTIVITES);

    QuerySnapshot lpa=await refCollectionActivite.get();

    int i=0;
    EntPresenceActivite pa;
    await Future.forEach<QueryDocumentSnapshot<Object?>>(lpa.docs, (o) async {

      if (o.exists) {
        pa = EntPresenceActivite.fromFB(o);
        if(listeId.contains(pa.idActivite) == false) {
          i++;
          await FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_PRESENCE_ACTIVITES).doc(o.id).delete();
        }
      }
    });
  }

  Future<void> majPresenceActivite(EntPresenceActivite p) async{
    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_PRESENCE_ACTIVITES);

    Future<void> ret;
    if(p.id == "") ret=refCollection.add(p.toFB());
    else ret=refCollection.doc(p.id).set(p.toFB());

    /*FlutterError.onError = (errorDetails) {
      // handle error
      print("DANS on ERROR !!");
    };*/

    //await FirebaseCrashlytics.instance.sendUnsentReports();
  //print("unsent -> " + (await FirebaseCrashlytics.instance.checkForUnsentReports()).toString());
    //FirebaseCrashlytics.instance.log("fffffffffffffffffffffff");
    //throw new Exception("supr gilloux");
    //FirebaseCrashlytics.instance.crash();
    /*FirebaseCrashlytics.instance.log("gilloux");

    FirebaseAnalytics analytics = FirebaseAnalytics.instance;
    analytics.logEvent(
      name: 'test_event',
      parameters: <String, dynamic>{
        'string': 'string',
        'int': 42,
        'long': 12345678910,
        'double': 42.0,
        // Only strings and numbers (ints & doubles) are supported for GA custom event parameters:
        // https://developers.google.com/analytics/devguides/collection/analyticsjs/custom-dims-mets#overview
        'bool': true.toString(),
        'userIdActivite': "llll",
        'idActivite' : "nbnbnb"
      },
    );*/
    //try{
    //  throw new Exception("Test Crash");
    /*}
    catch(e){
      FirebaseCrashlytics.instance.setUserIdentifier("SUPER_GILLOUX");
      FlutterErrorDetails det=FlutterErrorDetails(exception: e,library: "test_activite");
      await FirebaseCrashlytics.instance.recordFlutterError(det);
    }*/
    //
   // FirebaseCrashlytics.instance.crash();
    return(ret);

  }

  Future<void> deletePresenceActivite(EntPresenceActivite p){
    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_PRESENCE_ACTIVITES);

    Future<void> ret;
    ret=refCollection.doc(p.id).delete();

    return(ret);

  }

}
