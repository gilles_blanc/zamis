import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/model/EntAnniversaire.dart';
import 'package:zamis/model/EntUser.dart';

class UserService {
  /*static
  late BehaviorSubject<List<EntUser>> _listSubject;*/
  /*
  static StreamSubscription<List<EntVersement>>? _subListeVersement=null;
  late List<EntUser> _listeUser;*/

  late BehaviorSubject<List<EntUser>> _listSubject;
  late BehaviorSubject<List<EntAnniversaire>> _listAnniversaireSubject;

  StreamSubscription<QuerySnapshot<Object?>>? _subListUser=null;
  static UserService? _singleton;
  late FirebaseFirestore _fbInstanceFirestore;

  static void razSingleton() {
    _singleton = null;
  }

  static UserService getInstance() {
    if (_singleton == null) {
      _singleton = UserService._();
    }
    return (_singleton!);
  }

  UserService._() {
    _fbInstanceFirestore = FirebaseFirestore.instanceFor(app:MyKey.appFB!);
    _listSubject = BehaviorSubject<List<EntUser>>();
    _listAnniversaireSubject = BehaviorSubject<List<EntAnniversaire>>();
  }

  void cancelListenerListUser() {
    if(_subListUser != null){
      _subListUser!.cancel();
      _subListUser=null;
    }
  }

  void initListenerListUser(){

    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_USERS);

    cancelListenerListUser();

    _subListUser=refCollection.snapshots().listen((l) async {
      List<EntUser> listeUser = [];
      EntUser u;

      await Future.forEach<QueryDocumentSnapshot<Object?>>(l.docs, (o) async {
        if (o.exists) {
          u = EntUser.fromFB(o);
          //print("GET USER :[" + u.id+"]");
          QuerySnapshot<Map<String,dynamic>> snap =await refCollection.doc(u.id).collection(Constantes.SUB_COLLECTION_FB_USERS_NOTES).get();// snapshots().single;
          u.fillNotes(snap);
          //print("GET SOUS COLLECTION :[" + u.id+"] nb notes = " + u.listeUserNotation.length.toString());


          listeUser.add(u);
        }

      });

      listeUser.sort((u1,u2){
        int ret=(u1.nom + u1.prenom).compareTo(u2.nom + u2.prenom);
        return(ret);
      });

      //print("GET LISTE USER OBS listeUSer nb=" + listeUser.length.toString());
      _listSubject.add(listeUser);
      _consolideAnniversaire(listeUser);
    });
  }

  void _consolideAnniversaire(List<EntUser> listeUser){

    DateTime today = DateTime.now();
    DateTime limite1 = DateTime.now().add(new Duration(days: Constantes.NB_DAY_ANNIVERSAIRE));
    DateTime? limite2 = null;
    DateTime dateAnniv;
    List<EntAnniversaire> lRet = [];
    bool flagAdd;

    if (limite1.year > today.year) {
      limite2 = new DateTime(today.year, 12, 31);
    }

    for (EntUser u in listeUser) {
      flagAdd = false;
      do {
        if (limite2 != null) {
          dateAnniv = new DateTime(
              today.year, u.dateNaissance.month, u.dateNaissance.day);
          if ((dateAnniv.compareTo(today) >= 0) &&
              (dateAnniv.compareTo(limite2) <= 0)) {
            flagAdd = true;
            break;
          }
          dateAnniv = new DateTime(
              limite1.year, u.dateNaissance.month, u.dateNaissance.day);

          if ((dateAnniv.compareTo(limite2) >= 0) &&
              (dateAnniv.compareTo(limite1) <= 0)) {
            flagAdd = true;
            break;
          }
        } else {
          dateAnniv = new DateTime(
              today.year, u.dateNaissance.month, u.dateNaissance.day);
          if ((dateAnniv.compareTo(today) >= 0) &&
              (dateAnniv.compareTo(limite1) <= 0)) {
            flagAdd = true;
            break;
          }
        }
      } while (false);
      if (flagAdd == true) {
        lRet.add(new EntAnniversaire(u, dateAnniv));
      }
    }

    lRet.sort((a, b) {
      return a.dateAnniversaire.compareTo(b.dateAnniversaire);
    });
    _listAnniversaireSubject.add(lRet);

  }

  Stream<List<EntUser>> getObsUsers() {
    return (_listSubject.stream);
  }

  Stream<List<EntAnniversaire>> getObsAnniversaires() {
    return (_listAnniversaireSubject.stream);
  }

  Future<String> majUser(EntUser u) async{
    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_USERS);


    String  ret;
    if(u.id == ""){
      DocumentReference refDoc=await refCollection.add(u.toFB());
      ret=refDoc.id;
    }
    else {
      ret=u.id;
      await refCollection.doc(u.id).set(u.toFB());
    }

    return(ret);

  }

  Future<void> majUserNote(String idUser,String idUserNote,int note) async{
    CollectionReference refCollection =
    _fbInstanceFirestore.collection(Constantes.COLLECTION_FB_USERS);

    Map<String,dynamic> map = Map();
    map["note"]=note;
    await refCollection.doc(idUser).collection(Constantes.SUB_COLLECTION_FB_USERS_NOTES).doc(idUserNote).set(map);

    Map<String,dynamic> mapTS = Map();
    mapTS["tsNote"]=DateTime.now().millisecondsSinceEpoch;

    await refCollection.doc(idUser).update(mapTS);

  }

  Future<void> majTokenFCM(EntUser u,String? token) async {
    bool flagMaj = false;
    do {
      if (token == null) break;

      if (kIsWeb) {
        if (u.tokenFCMWeb != token) {
          flagMaj = true;
          u.tokenFCMWeb = token;
          break;
        }
      }
      else {
        if (u.tokenFCMMob != token) {
          flagMaj = true;
          u.tokenFCMMob = token;
          break;
        }
      }
    } while (false);

    if (flagMaj == true) {
      await majUser(u);
    }
  }
}