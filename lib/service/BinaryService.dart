
import 'dart:async';
import 'dart:typed_data';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/model/StatusUpload.dart';

class BinaryService {
  static BinaryService? _singleton;
  late FirebaseStorage _fbInstanceStorage;

  static void razSingleton() {
    _singleton = null;
    
  }

  static BinaryService  getInstance() {
    if (_singleton == null) {
      _singleton = BinaryService._();
    }
    return (_singleton!);
  }

  BinaryService._() {
    _fbInstanceStorage = FirebaseStorage.instanceFor(app:MyKey.appFB!);
  }

  Future<StatusUpload> majAvatar(Uint8List buffer,EntUser u) async{
    String path="avatars/" + u.id;
    return(_uploadData(buffer,path,"(Avatar)","image/png"));
  }

  Future<StatusUpload> _uploadData(Uint8List buffer,String path,String strType,[String contentType=""]) async {


    UploadTask uploadTask;
    SettableMetadata? metadata = null;

    // Create a Reference to the file


    Reference ref = _fbInstanceStorage.ref().child(path);

    if(contentType != "") {
      metadata = SettableMetadata(contentType: contentType);
    }


    uploadTask = ref.putData(buffer,metadata);

    return(_gestTask(uploadTask,strType));

  }

  Future<StatusUpload> _gestTask(UploadTask? ut,String strType) async {
    Completer<StatusUpload> completer = new Completer<StatusUpload>();
    StatusUpload statusUpload = StatusUpload();
    statusUpload.strType=strType;

    do {
      if(ut == null){
        statusUpload.reason="UploadTask null";
        completer.complete(statusUpload);
        break;
      }

      ut.snapshotEvents.listen((TaskSnapshot snapshot) {
        switch(snapshot.state) {
          case TaskState.error:
            statusUpload.reason="Erreur lors de l'upload";
            completer.complete(statusUpload);
            break;
          case TaskState.canceled:
            statusUpload.reason="Annulation de l'upload";
            completer.complete(statusUpload);
            break;
          case TaskState.paused:
            statusUpload.reason="Interruption de l'upload";
            completer.complete(statusUpload);
            break;
          case TaskState.running:
            break;
          case TaskState.success:
            snapshot.ref.getDownloadURL().then((url) {
              statusUpload.url=url;
              statusUpload.status=true;
              completer.complete(statusUpload);
            }).catchError((e){
              statusUpload.reason="Erreur lors de la récupération de l'url :" + e.toString();
              completer.complete(statusUpload);
            });
            break;
        }
      }, onError: (e) {
        statusUpload.reason="Erreur lors de l'upload :" + e.toString();
        completer.complete(statusUpload);
      });
    } while (false);

    return (completer.future);
  }

}
