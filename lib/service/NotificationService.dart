import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:rxdart/rxdart.dart';

import '../common/Constantes.dart';
import '../common/MyKey.dart';
import '../model/EntFCM.dart';
import '../model/EntMessage.dart';
import '../model/EntNotification.dart';

class NotificationService {
  static NotificationService? _singleton;
  late FirebaseMessaging _fbInstanceMessaging;

  late BehaviorSubject<EntFCM?> _fcmSubject;
  StreamSubscription<RemoteMessage>? _subFCM=null;

  static void razSingleton() {
    _singleton = null;
    
  }

  static NotificationService getInstance() {
    if (_singleton == null) {
      _singleton = NotificationService._();
    }
    return (_singleton!);
  }

  NotificationService._() {

    _fbInstanceMessaging = FirebaseMessaging.instance;  //instanceFor(app:MyKey.appFB!);
    _fcmSubject = BehaviorSubject<EntFCM?>();

  }

  void cancelListenerFCM() {
    if(_subFCM != null){
      _subFCM!.cancel();
      _subFCM=null;
    }
  }

  Stream<EntFCM?> getObsFCM() {
    return (_fcmSubject.stream);
  }

  void initListenerFCM() {
    cancelListenerFCM();

    _fcmSubject.add(null);
    _subFCM = FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      EntFCM fcm = EntFCM.fromFCM(event);


      _fcmSubject.add(fcm);
    });
  }

  /*FirebaseMessaging.onMessageOpenedApp.listen((message) {
  switch(int.parse(message.data["type"])){
  case Constantes.TYPE_NOTIFICATION_PARTICIPATION_ENTRAINEMENT:
  case Constantes.TYPE_NOTIFICATION_RETRAIT_PARTICIPATION_ENTRAINEMENT:
  break;
  case Constantes.TYPE_NOTIFICATION_MESSAGE:
  goMessage();
  break;
  }

  });*/


  Future<String?> getUserTokenFCM() async{
    Future<String?> f;
    if(kIsWeb) f=_getUserTokenFCMWeb();
    else f=_getUserTokenFCMMob();
    return(f);
  }

  Future<String?> _getUserTokenFCMMob() async{

    return(_fbInstanceMessaging.getToken());
  }

  Future<String?> _getUserTokenFCMWeb() async{
    await _fbInstanceMessaging.requestPermission();
    Future<String?> ret;
    if(kDebugMode) ret=_fbInstanceMessaging.getToken(vapidKey :Constantes.SERVER_KEY_FCM_WEB_DEV);
    else ret=_fbInstanceMessaging.getToken(vapidKey :Constantes.SERVER_KEY_FCM_WEB_PROD);
    return(ret);
  }

  void sendNotificationFCM(EntNotification notif) {

    Uri uri =  Uri.https(Constantes.URI_FCM_SERVER, Constantes.URI_FCM_PATH);
    Map<String, String> headers = Map<String, String>();
    headers["Content-type"] = "application/json";
    if(kDebugMode) headers["Authorization"] = "key=" + Constantes.KEY_GOOGLE_PUSH_SERVICE_DEV;
    else if(kDebugMode) headers["Authorization"] = "key=" + Constantes.KEY_GOOGLE_PUSH_SERVICE_PROD;
    String body=notif.build();
    if(body != "") {
      print("body=[" + body + "]");
      post(uri, headers: headers, body: body).then((rep) {
        //print("RETOUR HTTP : " + rep.statusCode.toString());
      });
    }
  }
}
