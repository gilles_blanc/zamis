
import 'dart:ui';

import 'package:badges/badges.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zamis/common/MyKey.dart';
import 'package:zamis/state/AppState.dart';

import 'Constantes.dart';
import 'NumberWrapper.dart';

class Util {

  static Future<void> error(BuildContext context, String mes,
      [String title = "Erreur",double height=Constantes.DLG_HEIGHT]) async {
    print("IN ERROR mes= ["+ mes + "]");
    await showDialog(
        context: context,
        builder: (context) =>
            _baseDialog(context, mes, title, Constantes.TYPE_DLG_ERROR,height));

  }

  static Future<void> alert(BuildContext context,String mes,
      [String title = "Information",double height=Constantes.DLG_HEIGHT]) async {
   await showDialog(
        context: context,
        builder: (context) =>
            _baseDialog(context, mes, title, Constantes.TYPE_DLG_ALERT,height));

  }

  static Future<bool> confirme( BuildContext context,String mes,
      [String title = "Confirmation"]) async {
    bool ret;

    ret = await showDialog(
      context: context,
      builder: (context) =>
          _baseDialog(context, mes, title, Constantes.TYPE_DLG_CONFIRME),
    );
    return (ret);
  }

  static Dialog _baseDialog(
      BuildContext context, String mes, String title, int type,[double height=Constantes.DLG_HEIGHT]) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        height: height,
        width: Constantes.DLG_WIDTH,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _header(context, title),
            _body(context, mes, type),
            _footer(context, type)
          ],
        ),
      ),
    );
  }

  static Widget _header(BuildContext context, String title) {
    Container c = Container(
      decoration: BoxDecoration(
        //border: Border.all(),
          color: Theme.of(context).colorScheme.primary.withOpacity(0.5),
          borderRadius: BorderRadius.only(topLeft:Radius.circular(10),topRight: Radius.circular(10))), //       <--- BoxDecoration here
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(left: 10, top: 5,bottom:5),
              child: Text(title,
                  style:
                  TextStyle(color:Colors.white /*Theme.of(context).colorScheme.primary*/)))
        ],
      ),
    );

    return (c);
  }

  static Widget _body(BuildContext context, String mes, int type) {
    late Icon ico;
    switch (type) {
      case Constantes.TYPE_DLG_ERROR:
        ico = Icon(
          Icons.error,
          color: Colors.red,
          size: Constantes.SIZE_ICON_DLG,
        );
        break;
      case Constantes.TYPE_DLG_ALERT:
        ico = Icon(Icons.info,
            size: Constantes.SIZE_ICON_DLG,
            color: Theme.of(context).colorScheme.secondary);
        break;
      case Constantes.TYPE_DLG_CONFIRME:
        ico = Icon(Icons.message,
            size: Constantes.SIZE_ICON_DLG,
            color: Theme.of(context).colorScheme.secondary);
        break;
    }
    Container c = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: ico,
          ),
          Flexible(
              child: Padding(padding: EdgeInsets.all(10), child: Text(mes)))
        ],
      ),
    );

    return (c);
  }

  static Widget _footer(BuildContext context, int type) {

    Widget butOK = filledButton(context,"Ok",() {
          Navigator.of(context).pop();
        });

    Widget butCANCEL = filledButton(context,"Annuler",() {
      Navigator.of(context).pop();
    });

    Widget butOui = filledButton(context,"Oui",() {
      Navigator.of(context).pop(true);
    });

    Widget butNon = filledButton(context,"Non",() {
      Navigator.of(context).pop(false);
    });

    List<Widget> listButton = [];

    switch (type) {
      case Constantes.TYPE_DLG_ERROR:
        listButton.add(butOK);
        break;
      case Constantes.TYPE_DLG_ALERT:
        listButton.add(butOK);
        break;
      case Constantes.TYPE_DLG_CONFIRME:
        listButton.add(butOui);
        listButton.add(butNon);
        break;
    }

    Container c = Container(
      //margin: const EdgeInsets.all(10.0),
      //padding: const EdgeInsets.all(5.0),
      decoration: BoxDecoration(
        //border: Border.all(),
          color: Theme.of(context).colorScheme.primary.withOpacity(0.5),
          borderRadius: BorderRadius.only(bottomLeft : Radius.circular(10),bottomRight:  Radius.circular(10))), //       <--- BoxDecoration here


      child:
      Row(mainAxisAlignment: MainAxisAlignment.center, children: listButton),
    );

    return (c);
  }

  static FirebaseOptions getFirebaseOptions(){
    FirebaseOptions options;

    if(kDebugMode){
      options=FirebaseOptions(
        apiKey: "AIzaSyDSCooHM84_7kzjploRVgtiJAnE64PJrb8",
        authDomain: "zamis-dev.firebaseapp.com",
        projectId: "zamis-dev",
        storageBucket: "zamis-dev.appspot.com",
        messagingSenderId: "132349256473",
        appId: "11:132349256473:android:a260c96c4c21ac72ca1b88",
      );
    }
    else{
      options=FirebaseOptions(
          apiKey: "AIzaSyBGFuUK5VTVggj9vn5yDEKbWQBQXjfojU0",
          authDomain: "zamis-foot.firebaseapp.com",
          projectId: "zamis-foot",
          storageBucket: "zamis-foot.appspot.com",
          messagingSenderId: "611466106695",
          appId: "1:611466106695:android:81397b5226a4756507de62"
      );
    }
    return(options);
  }

  static DateTime getDateTimeFromFB(Timestamp ts){
    DateTime d=DateTime.now();
    if(ts != null) d = ts.toDate();
    return(d);
  }

  static T safeMap<T>(DocumentSnapshot o,String key,[T? defValue]){
    late T ret;

    double d=0;
    if(T == String) ret=defValue ?? "" as T;
    if(T == bool) ret=defValue ?? false as T;
    if(T == int) ret=defValue ?? 0 as T;
    if(T == DateTime) ret=defValue ?? DateTime.now() as T;
    if(T == double) ret=defValue ?? d as T;

    if(o.exists) {
      Map<String, dynamic> mapFB = o.data() as Map<String, dynamic>;
      if(mapFB.containsKey(key)){
        if(T == DateTime){
          ret=getDateTimeFromFB(mapFB[key]) as T;
        }
        else  ret=mapFB[key];
      }
      else{
        //if(!kIsWeb) print("KEY ["+ key + "] INCONNUE DANS FB valeur par défaut =[" + ret.toString() + "], key FB=["+o.id+"]");
      }
    }
    return(ret);
  }

  static Future<T> getPersist<T>(String key) async {
    String fullKey = key;
    if (kDebugMode) fullKey = fullKey + "_DEBUG";

    SharedPreferences sp= await SharedPreferences.getInstance();

    late T ret;
    double d=0;

    if(T == String) ret=(sp.getString(fullKey) ?? "") as T;
    if(T == bool) ret=(sp.getBool(fullKey) ?? false) as T;
    if(T == int) ret=(sp.getInt(fullKey) ?? 0) as T;
    if(T == double) ret=(sp.getDouble(fullKey) ?? d) as T;

    return (ret);
  }

  static Future<void> setPersist<T>(String key, T value) async {
    String fullKey = key;
    if (kDebugMode) fullKey = fullKey + "_DEBUG";

    SharedPreferences sp= await SharedPreferences.getInstance();



    if(T == String) await sp.setString(fullKey, value as String);
    if(T == bool) await sp.setBool(fullKey, value as bool);
    if(T == int) await sp.setInt(fullKey,value as int);
    if(T == double) await sp.setDouble(fullKey,value as double);
  }

  static Widget loader(BuildContext context,bool flagWait){
    return Container(
      color: Colors.white,
      width:150,
      height: 10,
      child: flagWait ? LinearProgressIndicator(
          backgroundColor: Colors.white,
          valueColor: new AlwaysStoppedAnimation<Color>(
              Theme
                  .of(context)
                  .colorScheme
                  .primary)) : Container()

    ) ;
  }

  static Widget filledButton(BuildContext context,String text,  void func()?,[Color? col=null] ) {
    if(col == null) col = Theme.of(context).colorScheme.primary;
    return Container(
      //constraints: BoxConstraints(maxWidth: Constantes.MAX_WIDTH_RESPONSIVE),
        margin: const EdgeInsets.all(4.0),

        padding: EdgeInsets.only(left: 4, right: 4),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(primary: col),
          child: Text(
            text,
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20),
          ),
          onPressed: func
        ));
  }

  static Widget getIconBadge(int nb, Icon ico, Color colorButton,
      Color colorBage, VoidCallback? handler, double iconSize, bool flagPresence,
      [bool flagAlway = false]) {
    Widget w;
    Color colorText = Colors.black;
    if (flagPresence == true) {
      colorBage = Colors.deepPurpleAccent;
      colorText = Colors.white;
    }
    IconButton b = IconButton(
        color: colorButton, icon: ico, iconSize: iconSize, onPressed: handler);
    // TO DO
    if ((nb > 0) || (flagAlway == true)) {
      w = Badge(
          badgeContent: Text(
            nb.toString(),
            style: TextStyle(fontWeight: FontWeight.bold, color: colorText),
          ),
          badgeColor: colorBage, //Theme.of(context).colorScheme.secondary,
          position: BadgePosition(end: -2, top: -2),
          child: b);
    } else {
      w = b;
    }

    GestureDetector gd = new GestureDetector(
        onTap:handler,
        child: w);
    return (gd);
  }

  static NumberPicker _createNumericPicker(BuildContext context,void onChanged(int value),int note){

    TextStyle tsSel = TextStyle(color : Theme.of(context).colorScheme.primary,fontWeight: FontWeight.bold, fontSize: 20);
    TextStyle tsDesel = TextStyle(color : Colors.grey, fontSize: 15);

    int v=1;
    NumberPicker np=NumberPicker(
        textStyle:tsDesel,
        selectedTextStyle:tsSel,
        value: note,
        minValue: 20,
        maxValue: 40,
        onChanged: (value)
        {
          v=value;
          onChanged(value);
        }
        ,
        itemCount:3,
        itemWidth: 40,
        axis: Axis.horizontal);

    return(np);
  }

  static Widget saisieNotation(BuildContext context,int note,String hint,void onChanged(int value)){

    NumberPicker np= _createNumericPicker(context,onChanged,note);
    Color textColor = Colors.black;
    Text t= Text(hint,style: TextStyle(color : textColor,fontWeight: FontWeight.bold, fontSize: 20));

    Container c = Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        border: Border.all(),
        color: Colors.white,
      ),
      margin: const EdgeInsets.all(2.0),
      padding: EdgeInsets.only(left: 10, right: 10),

      child: np,
    );

    Row r=Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        t,
        c
      ],
    );



    return(r);
  }
/*
  static Widget getResponsive(BuildContext context,Widget w,{double maxWidth=Constantes.MAX_WIDTH_RESPONSIVE,flagBorder = false}){
    if(!kIsWeb) return(w);
    double widthScreen =MediaQuery.of(context).size.width;
    double margeX = 1;
    double margeY = 1;
    if(flagBorder == true) margeY=5;
    if(widthScreen > maxWidth){
      margeX=(widthScreen-maxWidth)/2;
    }
    Container responsiveContainer =Container(
        margin: EdgeInsets.only(right:margeX,left:margeX,top: margeY,bottom: margeY),
        padding: flagBorder == true ? EdgeInsets.all(5) :null,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: flagBorder == true ? Border.all(
            color: Theme.of(context).colorScheme.primary,
            width: 2,

          ):null
        ),
        child: w
    );
    return(responsiveContainer);
  }
*/
}

