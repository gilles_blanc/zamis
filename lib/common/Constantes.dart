class Constantes {

  static const String KEY_FCM_ID_USER="idUser";
  static const String KEY_FCM_TYPE="type";

  static const int DEFAUT_USER_NOTE=20;

  static const int MODE_TEAM_CONSULTATION=1;
  static const int MODE_TEAM_NOTATION=2;

  static const int TIME_STATUS_BEFORE_INSCRIPTION = 1;
  static const int TIME_STATUS_INSCRIPTION = 2;
  static const int TIME_STATUS_TOO_LATE_INSCRIPTION = 3;
  static const int TIME_STATUS_MATCH_BEGIN = 4;
  static const int TIME_STATUS_MATCH_END = 5;
  static const int TIME_STATUS_UNKNOWN = 6;

  static const int TYPE_MESSAGE_ERROR = 1;
  static const int TYPE_MESSAGE_INFO = 2;
  static const int TYPE_MESSAGE_CONFIRME = 3;

  static const int TYPE_ACTIVITE_NEXT_TRAINING=1;

  static const double MAX_WIDTH_RESPONSIVE=500;
  static const String KEY_GOOGLE_PUSH_SERVICE_DEV="AAAAHtChYxk:APA91bFbNenPIxGOCTXwoE8-D0MKCizEMb7x8n7j7UZcfqTINRgZ_AZrq_dNyHuTgOdGv3fImRG4VRfcmf286SpEigk_t33EkW1M6FOdLf-JGwWYJSrktw0ESocJestW3v09EzpKji9e";
  static const String KEY_GOOGLE_PUSH_SERVICE_PROD="AAAAjl44V0c:APA91bGjc0RpGnl1ndTjCFHCpWE0I-XPJCxke95ZdFmgwQ-kKxOQ72RMd85EQtb5JCYE5gbSLl3vS6BDKFRPBNVquAhiNxgoo3bwJn0__aH7OfTjC_2Vjo7KWyK_3xhGLm7L2ldlYjsk";

  // FCM KEY WEB  DEV
  //static const String SERVER_KEY_FCM_WEB="BIytFelTklVMS7bxi7rpNRzRDDyXLaLyscB1xcj1F6ZAQt5rE7UkZrG_sLvXsD23qWbu2cH-Ovytg7ACW0-IsgY";

  //FCM KEY WEB PROD
  static const String SERVER_KEY_FCM_WEB_DEV="BEPgGm13ydZaAG41ulwF6gaqvUoPvAkCeg8YuuXXj0kpKqt2dlMj6kRDsxM74I9w3UZkjiUgcPMQes5SFZJqadE";
  static const String SERVER_KEY_FCM_WEB_PROD="BA0U6DA555pteOtD6NYnKkoxfaHya2sFVS89MwzZtAhqV9YIl_P_WtefiU1zsktooCjVGZtZ75RyE8VlFBjFiR8";

  static const int MODE_ELEM_ACTIVITE_SAISIE=1;
  static const int MODE_ELEM_ACTIVITE_ADMIN=2;

  static const int TRAINING_HOUR=12;
  static const int NB_DAY_ANNIVERSAIRE=30;

  static String SUB_COLLECTION_FB_USERS_NOTES = "notes";
  static String COLLECTION_FB_USERS = "users";
  static String COLLECTION_FB_ENTRAINEMENTS = "entrainements";
  static String COLLECTION_FB_PRESENCE_ACTIVITES = "presenceActivites";
  static String COLLECTION_FB_INTERRUPTIONS = "interruptionEntrainement";
  static String COLLECTION_FB_MESSAGES = "messages";
  static String COLLECTION_FB_PARAMS = "params";

  static String DOC_PARAM_ADMIN="admin";

  static const int  CODE_PRESENCE_ACTIVITE_YES=1;
  static const int  CODE_PRESENCE_ACTIVITE_NO=2;
  static const int  CODE_PRESENCE_ACTIVITE_UNKNOWN=3;

  static String SEP_TO = ",";

  static String TOKEN_NOTIFICATION_MES = "##mes##";
  static String TOKEN_NB_PRESENT = "##nb-present##";
  static String TOKEN_NOTIFICATION_TITLE = "##title##";
  static String TOKEN_NOTIFICATION_REGISTRATION_IDS = "##registrationIds##";
  static String TOKEN_LIBELLE_FROM = "##from##";
  static String TOKEN_URL_AVATAR = "##avatar##";

  static String BODY_NOTIFICATION_FCM = '{"notification": {"body": "' +
  TOKEN_NOTIFICATION_MES +
  '","image": "' +
  TOKEN_URL_AVATAR +
  '","title": "' +
  TOKEN_NOTIFICATION_TITLE +
  '","sound" : "default"},"priority": "high","data": {"click_action": "FLUTTER_NOTIFICATION_CLICK","id": "1","status": "done"},"registration_ids": [' +
      TOKEN_NOTIFICATION_REGISTRATION_IDS +
  ']}';

  static String BODY_NOTIFICATION_EMAIL = TOKEN_NOTIFICATION_MES;
  static String LINK_AVATAR_EMAIL = "<img style='border-bottom-left-radius:50' width='70' height='70' src=" + TOKEN_URL_AVATAR + ">";

  static String URI_FCM_SERVER = "fcm.googleapis.com";
  static String URI_FCM_PATH = "fcm/send";
  static String URI_EMAIL_SERVER = "https://api.mailgun.net/v3/scsc-veteran.fr/messages";

  static String API_KEY_MAIL = "key-b45c7a5f40097236738654c1fdbe747d";
  static String AUTH_BASIC ="Basic YXBpOmtleS1iNDVjN2E1ZjQwMDk3MjM2NzM4NjU0YzFmZGJlNzQ3ZA==";
  static String MAIL_SCSC = "scsc@scsc-veteran.fr";

  static String TOKEN_BODY_MESSAGE =
  "Nouveau message de " + TOKEN_LIBELLE_FROM ;
  static String TOKEN_BODY_PARTICIPATION_ENTRAINEMENT =
  "Participation de " + TOKEN_LIBELLE_FROM + " au prochain entrainement ";
  static String TOKEN_BODY_PARTICIPATION_MATCH = "Participation de " +
  TOKEN_LIBELLE_FROM + " au prochain match ";
  static String TOKEN_BODY_RETRAIT_PARTICIPATION_ENTRAINEMENT =
      "Retrait de participation de " + TOKEN_LIBELLE_FROM + " au prochain entrainement ";
  static String TOKEN_BODY_RETRAIT_PARTICIPATION_MATCH = "Retrait de participation de " +
      TOKEN_LIBELLE_FROM + " au prochain match ";

  static String TITLE_MESSAGE = "Message";
  static String TITLE_ENTRAINEMENT = "Entrainement " +  TOKEN_NB_PRESENT;
  static String TITLE_MATCH = "Match "  + TOKEN_NB_PRESENT;

  static const int TYPE_NOTIFICATION_MESSAGE = 1;
  static const int TYPE_NOTIFICATION_PARTICIPATION_ENTRAINEMENT = 2;
  static const int TYPE_NOTIFICATION_RETRAIT_PARTICIPATION_ENTRAINEMENT = 3;

  static final double DLG_WIDTH = 300.0;
  static const double DLG_HEIGHT = 200.0;
  static const double DLG_BIG_HEIGHT = 300.0;

  static const int ACTION_AVATAR_NONE = 0;
  static const int ACTION_AVATAR_EDIT = 1;
  static const int ACTION_AVATAR_ADMIN = 2;

  static const int TYPE_DLG_ERROR = 1;
  static const int TYPE_DLG_ALERT = 2;
  static const int TYPE_DLG_CONFIRME = 3;
  static const double SIZE_ICON_DLG = 50.0;

  static const int NB_MAX_MESSAGE = 30;

  static final PERSIST_EMAIL = "PERSIST_EMAIL";
  static final PERSIST_PASSWORD = "PERSIST_PASSWORD";
  static final PERSIST_LAST_ID_MESSAGE = "PERSIST_LAST_ID_MESSAGE";
  static final PERSIST_REMEMBER_CREDENTIALS = "PERSIST_REMEMBER_CREDENTIALS";

  static final ROLE_ADMIN = "admin";
  static final ROLE_JOUEUR = "joueur";

  static const int MAX_SIZE_AVATAR = 300;


}