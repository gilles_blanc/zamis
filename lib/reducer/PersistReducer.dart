import 'package:zamis/action/ActionRestorePersist.dart';
import 'package:zamis/action/ActionStatus.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/state/PersistState.dart';
import 'package:zamis/state/StatusState.dart';

class PersistReducer{

  static AppState status(AppState state, ActionRestorePersist action) {
    PersistState newPersistState = state.persistState.copyWith(
        flagPersistLogin : action.flagPersistLogin,passwordPersist:action.password,emailPersist:action.email);

    AppState newState = state.copyWith(persistState: newPersistState);
    return newState;
  }


}