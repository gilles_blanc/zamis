import 'package:zamis/action/ActionStatus.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/state/StatusState.dart';

class StatusReducer{

  static AppState status(AppState state, ActionStatus action) {
    StatusState newStatusState = state.statusState.copyWith(
        flagWait : action.flagWait,
        message: action.message,
        titre:action.titre,
        heightDlg: action.heightDlg,
        typeMessage: action.typeMessage);
    AppState newState = state.copyWith(statusState: newStatusState);
    return newState;
  }


}