import 'package:zamis/action/ActionLoginUser.dart';
import 'package:zamis/action/ActionLogoutUser.dart';
import 'package:zamis/action/ActionMajAvatar.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/state/TeamState.dart';
import 'package:zamis/state/UserState.dart';

class UserReducer{

  static AppState loginUser(AppState state, ActionLoginUser action) {
    UserState newUserState = state.currentUserState.copyWith(
        user: action.currenrUser);
    TeamState newTeamState = state.teamState.copyWith(listUser: action.listeUser,listAnniversaire: action.listeAnniversaire);
    return state.copyWith(currentUserState: newUserState,teamState: newTeamState);
  }

  static AppState logoutUser(AppState state, ActionLogoutUser action) {
    return state.copyWith(currentUserState: UserState.initial(),teamState: TeamState.initial());
  }


}