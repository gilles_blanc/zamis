import 'package:zamis/action/ActionPresenceActivite.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/state/PresenceActiviteState.dart';

class PresenceActiviteReducer{

  static AppState presenceActivite(AppState state, ActionPresenceActivite action) {
    PresenceActiviteState newPresenceActiviteState = state.presenceActiviteState.copyWith(
        dicPresenceActivite : action.dicPresenceActivite);
    return state.copyWith(presenceActiviteState: newPresenceActiviteState);
  }
}