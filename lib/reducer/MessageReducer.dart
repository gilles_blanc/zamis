import 'package:zamis/action/ActionListeMessage.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/state/MessageState.dart';

class MessageReducer{

  static AppState listMessage(AppState state, ActionListeMessage action) {
    MessageState newMessageState = state.messageState.copyWith(
        listeMessage: action.listeMessage);
    return state.copyWith(messageState: newMessageState);
  }



}