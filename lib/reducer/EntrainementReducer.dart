import 'package:zamis/action/ActionNextEntrainement.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/state/EntrainementState.dart';

class EntrainementReducer{

  static AppState nextEntrainement(AppState state, ActionNextEntrainement action) {
    EntrainementState newEntrainementState = state.entrainementState.copyWith(
        nextEntrainement: action.nextEntrainement);
    return state.copyWith(entrainementState: newEntrainementState);
  }
}