import 'package:redux/redux.dart';
import 'package:zamis/action/ActionListeMessage.dart';
import 'package:zamis/action/ActionLoginUser.dart';
import 'package:zamis/action/ActionLogoutUser.dart';
import 'package:zamis/action/ActionMajAvatar.dart';
import 'package:zamis/action/ActionNewFCM.dart';
import 'package:zamis/action/ActionNextEntrainement.dart';
import 'package:zamis/action/ActionParam.dart';
import 'package:zamis/action/ActionPresenceActivite.dart';
import 'package:zamis/action/ActionRestorePersist.dart';
import 'package:zamis/action/ActionStatus.dart';
import 'package:zamis/action/ActionVersion.dart';
import 'package:zamis/reducer/EntrainementReducer.dart';
import 'package:zamis/reducer/FCMReducer.dart';
import 'package:zamis/reducer/MessageReducer.dart';
import 'package:zamis/reducer/ParamReducer.dart';
import 'package:zamis/reducer/PresenceActiviteReducer.dart';
import 'package:zamis/state/AppState.dart';
import 'PersistReducer.dart';
import 'StatusReducer.dart';
import 'UserReducer.dart';
import 'VersionReducer.dart';

final appReducer = combineReducers<AppState>([
  TypedReducer<AppState, ActionStatus>(StatusReducer.status),
  TypedReducer<AppState, ActionRestorePersist>(PersistReducer.status),
  TypedReducer<AppState, ActionLoginUser>(UserReducer.loginUser),
  TypedReducer<AppState, ActionLogoutUser>(UserReducer.logoutUser),
  TypedReducer<AppState, ActionVersion>(VersionReducer.version),
  TypedReducer<AppState, ActionNextEntrainement>(EntrainementReducer.nextEntrainement),
  TypedReducer<AppState, ActionPresenceActivite>(PresenceActiviteReducer.presenceActivite),
  TypedReducer<AppState, ActionListeMessage>(MessageReducer.listMessage),
  TypedReducer<AppState, ActionParam>(ParamReducer.param),
  TypedReducer<AppState, ActionNewFCM>(FCMReducer.newNotif)
]);

