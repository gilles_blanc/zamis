import 'package:zamis/action/ActionRestorePersist.dart';
import 'package:zamis/action/ActionStatus.dart';
import 'package:zamis/action/ActionVersion.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/state/PersistState.dart';
import 'package:zamis/state/StatusState.dart';

class VersionReducer{

  static AppState version(AppState state, ActionVersion action) {
    AppState newState = state.copyWith(version: action.version,nrBuild: action.nrBuild);
    return newState;
  }


}