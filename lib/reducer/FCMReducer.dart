import 'package:zamis/action/ActionNewFCM.dart';
import 'package:zamis/state/AppState.dart';
import '../state/FCMState.dart';

class FCMReducer{

  static AppState newNotif(AppState state, ActionNewFCM action) {
    FCMState newFCMState = state.fcmState.copyWith(
        fcm: action.fcm);
    AppState newState = state.copyWith(fcmState: newFCMState);
    return newState;
  }


}