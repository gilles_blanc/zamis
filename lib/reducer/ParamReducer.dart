import 'package:zamis/action/ActionParam.dart';
import 'package:zamis/state/AppState.dart';


class ParamReducer{

  static AppState param(AppState state, ActionParam action) {
    AppState newState = state.copyWith(param: action.param);
    return newState;
  }


}