import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionLogin.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/action/ActionResetPassword.dart';
import 'package:zamis/action/ActionSavePersist.dart';
import 'package:zamis/action/ActionTogglePresenceActivite.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/model/EntActivite.dart';
import 'package:zamis/model/EntAnniversaire.dart';
import 'package:zamis/model/EntPresenceActivite.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';

class ActiviteViewModel extends BaseViewModel {
  late List<EntUser> listeUser;
  late Map<String,EntPresenceActivite> dicPresenceActivite;
  late EntPresenceActivite? Function(String idActivite) getActivite;
  late Function(EntActivite activite) goDetail;
  late Function(String idActivite) toggleActivite;

  ActiviteViewModel(Store<AppState> store) : super(store){
    this.listeUser=store.state.teamState.listUser;
    this.dicPresenceActivite=store.state.presenceActiviteState.dicPresenceActivite;

  }

  factory ActiviteViewModel.create(Store<AppState> store) {
    ActiviteViewModel vm=ActiviteViewModel(store);

    vm.getActivite=(String idActivite) {
      EntPresenceActivite? ret=null;
      if(vm.dicPresenceActivite.containsKey(idActivite)){
        ret=vm.dicPresenceActivite[idActivite];
      }
      return(ret);
    };

    vm.toggleActivite=(String idActivite) {
      EntPresenceActivite? pa=vm.getActivite(idActivite);
      if(pa == null){
        pa = EntPresenceActivite();
        pa.idActivite=idActivite;
        pa.idUser=vm.currentUser!.id;
        pa.codePresence=Constantes.CODE_PRESENCE_ACTIVITE_UNKNOWN;
      }

      store.dispatch(ActionTogglePresenceActivite(pa));

    };

    vm.goDetail=(EntActivite activite) {
      store.dispatch(ActionNavigate("/DetailActivite",activite));
    };
    return (vm);

  }
}