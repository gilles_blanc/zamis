import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionAddMessage.dart';
import 'package:zamis/action/ActionLogin.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/action/ActionResetPassword.dart';
import 'package:zamis/action/ActionSavePersist.dart';
import 'package:zamis/action/ActionTogglePresenceActivite.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/model/EntActivite.dart';
import 'package:zamis/model/EntAnniversaire.dart';
import 'package:zamis/model/EntMessage.dart';
import 'package:zamis/model/EntPresenceActivite.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';

class MessageViewModel extends BaseViewModel {
  late List<EntUser> listeUser;
  late List<EntMessage> listeMessage;
  late Function(String message) sendMessage;

  MessageViewModel(Store<AppState> store) : super(store){
    this.listeUser=store.state.teamState.listUser;
    this.listeMessage=store.state.messageState.listeMessage;

  }

  factory MessageViewModel.create(Store<AppState> store) {
    MessageViewModel vm=MessageViewModel(store);

    vm.sendMessage=(String message) {
      store.dispatch(ActionAddMessage(message, store.state.currentUserState.user!.id));
    };
    return (vm);

  }
}