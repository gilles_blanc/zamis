import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionAddMessage.dart';
import 'package:zamis/action/ActionCreateUser.dart';
import 'package:zamis/action/ActionLogin.dart';
import 'package:zamis/action/ActionMajUser.dart';
import 'package:zamis/action/ActionMajUserNote.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/action/ActionResetPassword.dart';
import 'package:zamis/action/ActionSavePersist.dart';
import 'package:zamis/action/ActionTogglePresenceActivite.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/model/EntActivite.dart';
import 'package:zamis/model/EntAnniversaire.dart';
import 'package:zamis/model/EntMessage.dart';
import 'package:zamis/model/EntPresenceActivite.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';

class CompositionViewModel extends BaseViewModel {
  late List<EntUser> listeUser;
  late Map<String,EntPresenceActivite> dicPresenceActivite;
  late EntPresenceActivite? Function(String idActivite) getActivite;

  CompositionViewModel(Store<AppState> store) : super(store){
    this.listeUser=store.state.teamState.listUser;
    this.dicPresenceActivite=store.state.presenceActiviteState.dicPresenceActivite;

  }

  factory CompositionViewModel.create(Store<AppState> store) {
    CompositionViewModel vm=CompositionViewModel(store);

    vm.getActivite=(String idActivite) {
      EntPresenceActivite? ret=null;
      if(vm.dicPresenceActivite.containsKey(idActivite)){
        ret=vm.dicPresenceActivite[idActivite];
      }
      return(ret);
    };

    return (vm);

  }
}