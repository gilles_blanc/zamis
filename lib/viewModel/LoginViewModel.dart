import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionLogin.dart';
import 'package:zamis/action/ActionResetPassword.dart';
import 'package:zamis/action/ActionSavePersist.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';

class LoginViewModel extends BaseViewModel {
  late bool flagPersistLogin;
  late String email;
  late String password;
  late String version;
  late Function(String email,String password) login;
  late Function(String password) resetPassword;
  late Function({bool? flagPersistLogin,String? email,String? password}) savePersist;

  LoginViewModel(Store<AppState> store) : super(store){
    flagPersistLogin=store.state.persistState.flagPersistLogin;
    email=store.state.persistState.emailPersist;
    password=store.state.persistState.passwordPersist;
    version=store.state.version;
  }

  factory LoginViewModel.create(Store<AppState> store) {
    LoginViewModel vm=LoginViewModel(store);

    vm.login=(String email,String password) {
      store.dispatch(ActionLogin(email,password));
    };

    vm.resetPassword=(String email) {
      store.dispatch(ActionResetPassword(email));
    };

    vm.savePersist=({bool? flagPersistLogin,String? email,String? password}){
      store.dispatch(ActionSavePersist(flagPersistLogin:flagPersistLogin,email:email,password:password));
    };
    return (vm);

  }
}