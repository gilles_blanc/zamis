import 'package:launch_review/launch_review.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionInitAuthent.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';

class NewVersionModel extends BaseViewModel {

  late Function() downloadNewVersion;
  late Function() cancelNewVersion;

  NewVersionModel(Store<AppState> store) : super(store){

  }

  factory NewVersionModel.create(Store<AppState> store) {
    NewVersionModel vm=NewVersionModel(store);

    vm.cancelNewVersion = () {
      store.dispatch(new ActionInitAuthent());
    };

    vm.downloadNewVersion = () {
      LaunchReview.launch(androidAppId: "fr.foot.zamis");
    };

    return (vm);

  }
}