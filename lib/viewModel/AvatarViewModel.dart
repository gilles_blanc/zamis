import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionLogin.dart';
import 'package:zamis/action/ActionMajAvatar.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/action/ActionResetPassword.dart';
import 'package:zamis/action/ActionSavePersist.dart';
import 'package:zamis/model/EntAnniversaire.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';

class AvatarViewModel extends BaseViewModel {
  late Function() edit;
  late Function() admin;
  late Function(EntUser user,Uint8List buffer) maj;

  AvatarViewModel(Store<AppState> store) : super(store){
  }

  factory AvatarViewModel.create(Store<AppState> store) {
    AvatarViewModel vm=AvatarViewModel(store);

    vm.edit=() {
      store.dispatch(ActionNavigate("/AvatarEdit"));
    };

    vm.admin=() {
      store.dispatch(ActionNavigate("/AdminUser"));
    };

    vm.maj=(EntUser user,Uint8List buffer) {
      store.dispatch(ActionMajAvatar(user,buffer));
    };

    return (vm);

  }
}