import 'package:redux/redux.dart';
import 'package:zamis/model/EntAnniversaire.dart';
import 'package:zamis/model/EntEntrainement.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';

class HomeViewModel extends BaseViewModel {
  late List<EntUser> listeUser;
  late List<EntAnniversaire> listeAnniversaire;
  late int nbNewMessage;
  late EntEntrainement? nextTraining;

  //late Function({bool? flagPersistLogin,String? email,String? password}) savePersist;

  HomeViewModel(Store<AppState> store) : super(store){
    this.listeUser=store.state.teamState.listUser;
    this.listeAnniversaire=store.state.teamState.listAnniversaire;
    this.nbNewMessage=1;
    this.nextTraining = store.state.entrainementState.nextEntrainement;

    //password=store.state.persistState.passwordPersist;
  }

  factory HomeViewModel.create(Store<AppState> store) {
    HomeViewModel vm=HomeViewModel(store);

    /*vm.savePersist=({bool? flagPersistLogin,String? email,String? password}){
      store.dispatch(ActionSavePersist(flagPersistLogin:flagPersistLogin,email:email,password:password));
    };*/
    return (vm);

  }
}