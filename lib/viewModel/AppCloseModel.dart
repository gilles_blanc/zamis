import 'package:flutter/services.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionInitAuthent.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';

class AppCloseModel extends BaseViewModel {

  late Function() exit;
  late Function() cancelClose;

  AppCloseModel(Store<AppState> store) : super(store){

  }

  factory AppCloseModel.create(Store<AppState> store) {
    AppCloseModel vm=AppCloseModel(store);

    vm.exit = () {
      SystemNavigator.pop;
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    };

    vm.cancelClose = () {
      store.dispatch(new ActionInitAuthent());
    };
    return (vm);

  }
}