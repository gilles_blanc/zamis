import 'package:redux/redux.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/action/ActionNewFCM.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/model/EntFCM.dart';
import 'package:zamis/model/EntParam.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';

class BaseViewModel{
  late EntUser? currentUser;
  late String message;
  late String titre;
  late int typeMessage;
  late bool flagWait;
  late double heightDlg;
  late Function(String target,[Object? data]) nav;
  late Function() pop;
  late Function(dynamic action) action;
  late EntParam param;
  late String version;
  late String nrBuild;
  late EntFCM? _fcm;
  late Function() getFCM;

  BaseViewModel(Store<AppState> store){
   message=store.state.statusState.message;
    titre=store.state.statusState.titre;
    typeMessage=store.state.statusState.typeMessage;
    flagWait=store.state.statusState.flagWait;
    heightDlg=store.state.statusState.heightDlg;
    currentUser=store.state.currentUserState.user;
    param=store.state.param;
    version=store.state.version;
    nrBuild=store.state.nrBuild;
    _fcm=store.state.fcmState.fcm;

   nav=(String target,[Object? data=null]){
      store.dispatch(new ActionNavigate(target,data));
    };

   pop=(){
     store.dispatch(new ActionNavigate());
   };

   action = (dynamic action){
     store.dispatch(action);
   };

   getFCM = () {
     EntFCM? ret=null;
     if(_fcm != null){
       ret=_fcm!.clone();
       store.dispatch(new ActionNewFCM(null));
     }
     return(ret);
   };
  }

  bool isAdmin(){
    bool ret=false;
    if(currentUser != null){
      if(currentUser!.role == Constantes.ROLE_ADMIN) ret=true;
    }
    return(ret);
  }

  bool isMe(String idUser){
    bool ret=false;
    if(currentUser != null){
      if(currentUser!.id == idUser) ret=true;
    }
    return(ret);
  }


}