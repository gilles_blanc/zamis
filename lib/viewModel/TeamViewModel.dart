import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:zamis/action/ActionAddMessage.dart';
import 'package:zamis/action/ActionCreateUser.dart';
import 'package:zamis/action/ActionLogin.dart';
import 'package:zamis/action/ActionMajUser.dart';
import 'package:zamis/action/ActionMajUserNote.dart';
import 'package:zamis/action/ActionNavigate.dart';
import 'package:zamis/action/ActionResetPassword.dart';
import 'package:zamis/action/ActionSavePersist.dart';
import 'package:zamis/action/ActionTogglePresenceActivite.dart';
import 'package:zamis/common/Constantes.dart';
import 'package:zamis/model/EntActivite.dart';
import 'package:zamis/model/EntAnniversaire.dart';
import 'package:zamis/model/EntMessage.dart';
import 'package:zamis/model/EntPresenceActivite.dart';
import 'package:zamis/model/EntUser.dart';
import 'package:zamis/state/AppState.dart';
import 'package:zamis/viewModel/BaseViewModel.dart';

class TeamViewModel extends BaseViewModel {
  late List<EntUser> listeUser;
  late Function(EntUser user) editUser;
  late Function(EntUser user,String password) createUser;
  late Function(String idUserNote,int note) majUserNote;

  TeamViewModel(Store<AppState> store) : super(store){
    this.listeUser=store.state.teamState.listUser;

  }

  factory TeamViewModel.create(Store<AppState> store) {
    TeamViewModel vm=TeamViewModel(store);

    vm.editUser=(EntUser user) {
      store.dispatch(ActionMajUser(user,true));
    };

    vm.createUser=(EntUser user,String password) {
      store.dispatch(ActionCreateUser(user,password));
    };

    vm.majUserNote=(String idUserNote,int note){
      store.dispatch(ActionMajUserNote(store.state.currentUserState.user!.id, idUserNote, note));
    };
    return (vm);

  }
}